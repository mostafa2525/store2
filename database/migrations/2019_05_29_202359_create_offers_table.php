<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('offers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('language_id')->unsigned();
            $table->string('name1');            
            $table->string('link1');            
            $table->string('name2');            
            $table->string('link2');            
            $table->string('name3');            
            $table->string('link3');            
            $table->string('name4');            
            $table->string('link4');            
            $table->string('name5');            
            $table->string('link5');            
            $table->string('name6');            
            $table->string('link6');            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('offers');
    }
}
