<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_contents', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('order_id')->unsigned();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->integer('product_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
            $table->string('quantity');            
            $table->tinyInteger('color')->nullable();           
            $table->tinyInteger('size')->nullable();           
            $table->enum('status',['pending','accepted','refused'])->default('pending');
            $table->text('refused_notes')->nullable(); // notes about refused order
            $table->double('price_at_this_time')->nullable(); // price for this product at time of order
            $table->double('charge_price_at_this_time')->nullable();  // price of charge for this product at time of order

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_contents');
    }
}
