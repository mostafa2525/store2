<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('admin_id')->unsigned()->nullable();
            $table->foreign('admin_id')->references('id')->on('admins');
            $table->text('data')->nullable();// all data about client in ( json )
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            $table->string('mobile')->nullable();
            $table->integer('order_id')->unsigned()->nullable();
            $table->string('code');
            $table->string('city');
            $table->integer('client_id')->unsigned()->nullable();
            $table->enum('status',['pending','canceled','shipping','accepted','refused'])->default('pending');
            $table->timestamp('status_change_date')->nullable();
            $table->text('notes')->nullable(); // global notes about order
            $table->text('refused_notes')->nullable(); // notes about refused order
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
