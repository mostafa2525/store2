<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobileAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mobile_ads', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('language_id')->unsigned();
            $table->string('img1')->nullable();
            $table->string('link1')->nullable();
            $table->string('img2')->nullable();
            $table->string('link2')->nullable();
            $table->string('img3')->nullable();
            $table->string('link3')->nullable();
            $table->string('img4')->nullable();
            $table->string('link4')->nullable();
            $table->string('img5')->nullable();
            $table->string('link5')->nullable();
            $table->string('img6')->nullable();
            $table->string('link6')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mobile_ads');
    }
}
