<?php

use Illuminate\Database\Seeder;
use App\Models\Offer;

class OfferSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Offer::create([
            'language_id' => 1,
            'name1' => 'offer one',
            'link1' => 'http://google.com',
            'name2' => 'offer two',
            'link2' => 'http://google.com',
            'name3' => 'offer three',
            'link3' => 'http://google.com',
            'name4' => 'offer four',
            'link4' => 'http://google.com',
            'name5' => 'offer five',
            'link5' => 'http://google.com',
            'name6' => 'offer six',
            'link6' => 'http://google.com',
        ]);


         Offer::create([
            'language_id' => 2,
            'name1' => 'offer one',
            'link1' => 'http://google.com',
            'name2' => 'offer two',
            'link2' => 'http://google.com',
            'name3' => 'offer three',
            'link3' => 'http://google.com',
            'name4' => 'offer four',
            'link4' => 'http://google.com',
            'name5' => 'offer five',
            'link5' => 'http://google.com',
            'name6' => 'offer six',
            'link6' => 'http://google.com',
        ]);
    }
}
