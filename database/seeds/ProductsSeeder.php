<?php

use App\Models\SubCategory;
use App\Models\Product;
use App\Models\Product\Pimage;
use App\Models\Product\Psize;
use App\Models\Product\Pcolor;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Helpers\Classes\Code;

/**
 * Created by PhpStorm.
 * User: Mahmoud
 * Date: 4/4/2019
 * Time: 10:58 AM
 */
class ProductsSeeder extends Seeder
{

    public function run()
    {
        $faker = Faker::create();
        $images = array("1.jpg","2.jpg","3.jpg");
        $sizes = array("1","2","3");
        $colors = array("1","2","3");

        for($i=1; $i<=25; $i++)
        {
            $data = new Product();
            $data['category_id']        = 1;
            $data['language_id']        = 1;
            $data['sub_category_id']    = rand(1,5);
            $data['name']               = $faker->name;
            $data['code']               = Code::getNewProduct();
            $data['img']                = rand(1,5).'.jpg';
            $data['price']              = rand(101,999);
            $data['show_in_homePage']   = 'yes';
            $data['show_in_footer']     = 'yes';
            $data['featured']           = 'yes';
            $data['small_desc']         = $faker->text;
            $data['desc']               = $faker->text;
            $data['slug']               = str_slug($faker->name).'1';
            $data['tags']               = str_slug($faker->name);
            $data['status']             = 'yes';
 
            $data->save();
        }


        for($i=1; $i<=25; $i++)
        {
            $data = new Product();
            $data['category_id']        = 2;
            $data['language_id']        = 1;
            $data['sub_category_id']    = rand(6,10);
            $data['name']               = $faker->name;
            $data['code']               = Code::getNewProduct();
            $data['img']                = rand(1,5).'.jpg';
            $data['price']              = rand(101,999);
            $data['show_in_homePage']   = 'yes';
            $data['show_in_footer']     = 'yes';
            $data['featured']           = 'yes';
            $data['small_desc']         = $faker->text;
            $data['desc']               = $faker->text;
            $data['slug']               = str_slug($faker->name).'2';
            $data['tags']               = str_slug($faker->name);

            $data['status']             = 'yes';

            $data->save();
        }


        for($i=1; $i<=25; $i++)
        {
            $data = new Product();
            $data['category_id']        = 3;
            $data['language_id']        = 1;
            $data['sub_category_id']    = rand(11,15);
            $data['name']               = $faker->name;
            $data['code']               = Code::getNewProduct();
            $data['img']                = rand(1,5).'.jpg';
            $data['price']              = rand(101,999);
            $data['show_in_homePage']   = 'yes';
            $data['show_in_footer']     = 'yes';
            $data['featured']           = 'yes';
            $data['small_desc']         = $faker->text;
            $data['desc']               = $faker->text;
            $data['slug']               = str_slug($faker->name).'3';
            $data['tags']               = str_slug($faker->name);

            $data['status']             = 'yes';
            $data->save();
        }






        for($i=1; $i<=25; $i++)
        {
            $data = new Product();
            $data['category_id']        = 4;
            $data['language_id']        = 1;
            $data['sub_category_id']    = rand(16,20);
            $data['name']               = $faker->name;
            $data['code']               = Code::getNewProduct();
            $data['img']                = rand(1,5).'.jpg';
            $data['price']              = rand(101,999);
            $data['show_in_homePage']   = 'yes';
            $data['show_in_footer']     = 'yes';
            $data['featured']           = 'yes';
            $data['small_desc']         = $faker->text;
            $data['desc']               = $faker->text;
            $data['slug']               = str_slug($faker->name).'4';
            $data['tags']               = str_slug($faker->name);

            $data['status']             = 'yes';
            $data->save();
        }



        for($i=1; $i<=25; $i++)
        {
            $data = new Product();
            $data['category_id']        = 5;
            $data['language_id']        = 1;
            $data['sub_category_id']    = rand(21,25);
            $data['name']               = $faker->name;
            $data['code']               = Code::getNewProduct();
            $data['img']                = rand(1,5).'.jpg';
            $data['price']              = rand(101,999);
            $data['show_in_homePage']   = 'yes';
            $data['show_in_footer']     = 'yes';
            $data['featured']           = 'yes';
            $data['small_desc']         = $faker->text;
            $data['desc']               = $faker->text;
            $data['slug']               = str_slug($faker->name).'5';
            $data['tags']               = str_slug($faker->name);

            $data['status']             = 'yes';
            $data->save();
        }
        























            $small_desc = "ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد ";

        $desc = "ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد " ;

        $name = "اسم المنتج بالعربية ";





        //  arabic   




        for($i=1; $i<=25; $i++)
        {
            $data = new Product();
            $data['category_id']        = 6;
            $data['language_id']        = 2;
            $data['sub_category_id']    = rand(26,30);
            $data['name']               = $name;
            $data['code']               = Code::getNewProduct();
            $data['img']                = rand(1,5).'.jpg';
            $data['price']              = rand(101,999);
            $data['show_in_homePage']   = 'yes';
            $data['show_in_footer']     = 'yes';
            $data['featured']           = 'yes';
            $data['small_desc']         = $small_desc;
            $data['desc']               = $desc;
            $data['slug']               =  str_slug($name).Code::getNewProduct();
            $data['tags']               = str_slug($faker->name);

            $data['status']             = 'yes';
            $data->save();
        }



         $name2 = " منتج القسم الثانى  ";



        for($i=1; $i<=25; $i++)
        {
            $data = new Product();
            $data['category_id']        = 7;
            $data['language_id']        = 2;
            $data['sub_category_id']    = rand(30,35);
            $data['name']               = $name2;
            $data['code']               = Code::getNewProduct();
            $data['tags']               = str_slug($faker->name);

            $data['img']                = rand(1,5).'.jpg';
            $data['price']              = rand(101,999);
            $data['show_in_homePage']   = 'yes';
            $data['show_in_footer']     = 'yes';
            $data['featured']           = 'yes';
            $data['small_desc']         = $small_desc;
            $data['desc']               = $desc;
            $data['slug']               =  str_slug($name2).Code::getNewProduct();
            $data['status']             = 'yes';
            $data->save();
        }


         $name3 = " منتج القسم  الثالث  ";

        for($i=1; $i<=25; $i++)
        {
            $data = new Product();
            $data['category_id']        = 8;
            $data['language_id']        = 2;
            $data['sub_category_id']    = rand(36,40);
            $data['name']               = $name3;
            $data['code']               = Code::getNewProduct();
            $data['img']                = rand(1,5).'.jpg';
            $data['price']              = rand(101,999);
            $data['show_in_homePage']   = 'yes';
            $data['show_in_footer']     = 'yes';
            $data['featured']           = 'yes';
            $data['small_desc']         = $small_desc;
            $data['desc']               = $desc;
            $data['slug']               =  str_slug($name3).Code::getNewProduct();
            $data['tags']               = str_slug($faker->name);

            $data['status']             = 'yes';
            $data->save();
        }





         $name4 = " منتج القسم   الرابع  ";

        for($i=1; $i<=25; $i++)
        {
            $data = new Product();
            $data['category_id']        = 9;
            $data['language_id']        = 2;
            $data['sub_category_id']    = rand(40,45);
            $data['name']               = $name4;
            $data['code']               = Code::getNewProduct();
            $data['img']                = rand(1,5).'.jpg';
            $data['price']              = rand(101,999);
            $data['show_in_homePage']   = 'yes';
            $data['show_in_footer']     = 'yes';
            $data['featured']           = 'yes';
            $data['small_desc']         = $small_desc;
            $data['desc']               = $desc;
            $data['slug']               =  str_slug($name4).Code::getNewProduct();
            $data['tags']               = str_slug($faker->name);

            $data['status']             = 'yes';
            $data->save();
        }


         $name5 = " منتج القسم   الخامس  ";

        for($i=1; $i<=25; $i++)
        {
            $data = new Product();
            $data['category_id']        = 10;
            $data['language_id']        = 2;
            $data['sub_category_id']    = rand(46,49);
            $data['name']               = $name5;
            $data['code']               = Code::getNewProduct();
            $data['img']                = rand(1,5).'.jpg';
            $data['price']              = rand(101,999);
            $data['show_in_homePage']   = 'yes';
            $data['show_in_footer']     = 'yes';
            $data['featured']           = 'yes';
            $data['small_desc']         = $small_desc;
            $data['desc']               = $desc;
            $data['slug']               =  str_slug($name5).Code::getNewProduct();
            $data['tags']               = str_slug($faker->name);
            
            $data['status']             = 'yes';
            $data->save();
        }
        





        for($i=1; $i<=250; $i++)
        {

            $img = new Pimage();
            $img->image = '1.jpg';
            $img->product_id = $i;
            $img->save(); 


            $img = new Pimage();
            $img->image = '2.jpg';
            $img->product_id = $i;
            $img->save(); 


            $img = new Pimage();
            $img->image = '3.jpg';
            $img->product_id = $i;
            $img->save(); 

        }




        for($i=1; $i<=125; $i++)
        {

            $size = new Psize();
            $size->size_id = '1';
            $size->product_id = $i;
            $size->save(); 

            $size = new Psize();
            $size->size_id = '2';
            $size->product_id = $i;
            $size->save(); 

            $size = new Psize();
            $size->size_id = '3';
            $size->product_id = $i;
            $size->save();  

        }




        for($i=125; $i<=250; $i++)
        {

            $size = new Psize();
            $size->size_id = '7';
            $size->product_id = $i;
            $size->save(); 

            $size = new Psize();
            $size->size_id = '8';
            $size->product_id = $i;
            $size->save(); 

            $size = new Psize();
            $size->size_id = '9';
            $size->product_id = $i;
            $size->save(); 

        }


        // colors  



        for($i=1; $i<=125; $i++)
        {

            $color = new Pcolor();
            $color->color_id = '1';
            $color->product_id = $i;
            $color->save(); 

            $color = new Pcolor();
            $color->color_id = '2';
            $color->product_id = $i;
            $color->save(); 

            $color = new Pcolor();
            $color->color_id = '3';
            $color->product_id = $i;
            $color->save();  

        }




        for($i=125; $i<=250; $i++)
        {

            $color = new Pcolor();
            $color->color_id = '7';
            $color->product_id = $i;
            $color->save(); 

            $color = new Pcolor();
            $color->color_id = '8';
            $color->product_id = $i;
            $color->save(); 

            $color = new Pcolor();
            $color->color_id = '9';
            $color->product_id = $i;
            $color->save(); 

        }









    }

}
