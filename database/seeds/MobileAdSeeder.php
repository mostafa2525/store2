<?php

use Illuminate\Database\Seeder;
use App\Models\MobileAd;

class MobileAdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        MobileAd::create([
            'language_id' => 1,
            'img1'        => 'adv1.jpg',
            'link1'       => 'http://google.com',
            'img2'        => 'adv2.jpg',
            'link2'       => 'http://google.com',
            'img3'        => 'adv3.jpg',
            'link3'       => 'http://google.com',
            'img4'        => 'adv4.jpg',
            'link4'       => 'http://google.com',
            'img5'        => 'adv5.jpg',
            'link5'       => 'http://google.com',
            'img6'        => 'adv6.jpg',
            'link6'       => 'http://google.com',
        ]);


        MobileAd::create([
            'language_id' => 2,
            'img1'        => 'adv1.jpg',
            'link1'       => 'http://google.com',
            'img2'        => 'adv2.jpg',
            'link2'       => 'http://google.com',
            'img3'        => 'adv3.jpg',
            'link3'       => 'http://google.com',
            'img4'        => 'adv4.jpg',
            'link4'       => 'http://google.com',
            'img5'        => 'adv5.jpg',
            'link5'       => 'http://google.com',
            'img6'        => 'adv6.jpg',
            'link6'       => 'http://google.com',
        ]); 



    }
}
