<?php

use App\Models\Category;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: Mahmoud
 * Date: 4/4/2019
 * Time: 10:58 AM
 */
class CategorySeeder extends Seeder
{

    public function run()
    {
        $faker = Faker::create();

        $data = new Category();
        $data['language_id'] = 1;
        $data['name'] = "Men";
        $data['img'] = '1'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;
        $data['slug'] = str_slug("Men");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes";
        $data->save();



        $data = new Category();
        $data['language_id'] = 1;
        $data['name'] = "Womens";
        $data['img'] = '2'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;
        $data['slug'] = str_slug("Womens");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes";
        $data->save();



        $data = new Category();
        $data['language_id'] = 1;
        $data['name'] = "Kids";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;
        $data['slug'] = str_slug("Kids");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes";
        $data->save();



        $data = new Category();
        $data['language_id'] = 1;
        $data['name'] = "Sports";
        $data['img'] = '4'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;
        $data['slug'] = str_slug("Sports");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes";
        $data->save();


        $data = new Category();
        $data['language_id'] = 1;
        $data['name'] = "Accessories";
        $data['img'] = '4'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;
        $data['slug'] = str_slug("Accessories");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes";
        $data->save();







        // arabic  


        $small_desc = "ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد ";

        $desc = "ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد " ;


        $data = new Category();
        $data['language_id'] = 2;
        $data['name'] = " رجالى ";
        $data['img'] = '1'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc;
        $data['slug'] = str_slug("رجالى");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes";
        $data->save();



        $data = new Category();
        $data['language_id'] = 2;
        $data['name'] = "حريمى ";
        $data['img'] = '2'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc;
        $data['slug'] = str_slug("حريمى");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes";
        $data->save();



        $data = new Category();
        $data['language_id'] = 2;
        $data['name'] = "اطفال ";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc;
        $data['slug'] = str_slug("اطفال");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes";
        $data->save();



        $data = new Category();
        $data['language_id'] = 2;
        $data['name'] = "رياضى ";
        $data['img'] = '4'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc;
        $data['slug'] = str_slug("رياضى");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes";
        $data->save();


        $data = new Category();
        $data['language_id'] =2;
        $data['name'] = "اكسسوارات";
        $data['img'] = '4'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc;
        $data['slug'] = str_slug("اكسسوارات");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes";
        $data->save();






    

    }

}
