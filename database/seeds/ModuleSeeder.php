<?php

use Illuminate\Database\Seeder;
use App\Models\Module;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
                
        $data = new Module();
        $data['name'] = 'admins';
        $data['type'] = 'crud';
        $data->save();
        
        $data = new Module();
        $data['name'] = 'adminGroups';
        $data['type'] = 'crud';
        $data->save();
                
        $data = new Module();
        $data['name'] = 'categories';
        $data['type'] = 'crud';
        $data->save();

        $data = new Module();
        $data['name'] = 'brands';
        $data['type'] = 'crud';
        $data->save();

        $data = new Module();
        $data['name'] = 'subCategories';
        $data['type'] = 'crud';
        $data->save();

        $data = new Module();
        $data['name'] = 'slider';
        $data['type'] = 'crud';
        $data->save();

        $data = new Module();
        $data['name'] = 'governates';
        $data['type'] = 'crud';
        $data->save();

        $data = new Module();
        $data['name'] = 'staticPages';
        $data['type'] = 'crud';
        $data->save();

        $data = new Module();
        $data['name'] = 'sizes';
        $data['type'] = 'crud';
        $data->save();

        $data = new Module();
        $data['name'] = 'colors';
        $data['type'] = 'crud';
        $data->save();

        $data = new Module();
        $data['name'] = 'products';
        $data['type'] = 'crud';
        $data->save();
        
        $data = new Module();
        $data['name'] = 'blog';
        $data['type'] = 'crud';
        $data->save();

        $data = new Module();
        $data['name'] = 'newsletter';
        $data['type'] = 'crud';
        $data->save();

        $data = new Module();
        $data['name'] = 'messages';
        $data['type'] = 'crud';
        $data->save();

        $data = new Module();
        $data['name'] = 'clients';
        $data['type'] = 'crud';
        $data->save();

        $data = new Module();
        $data['name'] = 'settings';
        $data['type'] = 'other';
        $data->save();

        $data = new Module();
        $data['name'] = 'orders';
        $data['type'] = 'other';
        $data->save();
    }
}
