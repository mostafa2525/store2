<?php

use Illuminate\Database\Seeder;
use App\Models\Product\Order;
use Faker\Factory as Faker;
use App\Helpers\Classes\Code;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i=1; $i<=200; $i++) {
            Order::create([
                'data' => 'this is data',
                'name' => $faker->name,
                'email' => $faker->unique()->email,
                'code' => Code::getNewOrder(),
                'city' => 'Cairo',
                'status' => 'pending'
            ]);
        }
        
        // for ($i=1; $i<=150; $i++) {
        //     Order::create([
        //         'admin_id' => 1,
        //         'data' => 'this is data',
        //         'name' => $faker->name,
        //         'email' => $faker->unique()->email,
        //         'code' => rand(1,10000),
        //         'status' => 'shipping'
        //     ]);
        // }

        // for ($i=1; $i<=150; $i++) {
        //     Order::create([
        //         'admin_id' => 1,
        //         'data' => 'this is data',
        //         'name' => $faker->name,
        //         'email' => $faker->unique()->email,
        //         'code' => rand(1,10000),
        //         'status' => 'canceled'
        //     ]);
        // }

        // for ($i=1; $i<=150; $i++) {
        //     Order::create([
        //         'admin_id' => 1,
        //         'data' => 'this is data',
        //         'name' => $faker->name,
        //         'email' => $faker->unique()->email,
        //         'code' => rand(1,10000),
        //         'status' => 'accepted'
        //     ]);
        // }

        // for ($i=1; $i<=150; $i++) {
        //     Order::create([
        //         'admin_id' => 1,
        //         'data' => 'this is data',
        //         'name' => $faker->name,
        //         'email' => $faker->unique()->email,
        //         'code' => rand(1,10000),
        //         'status' => 'refused'
        //     ]);
        // }

    }
}
