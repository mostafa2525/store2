<?php

use App\Models\SubCategory;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

/**
 * Created by PhpStorm.
 * User: Mahmoud
 * Date: 4/4/2019
 * Time: 10:58 AM
 */
class SubCategorySeeder extends Seeder
{

    public function run()
    {
        $faker = Faker::create();

        $data = new SubCategory();
        $data['category_id'] = 1;
        $data['name'] = "T-Shirt";
        $data['img'] = '1'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("T-Shirt");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 1;
        $data['name'] = "Shoes";
        $data['img'] = '2'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Shoes");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 1;
        $data['name'] = "Polo-Shirts";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Polo-Shirts");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 1;
        $data['name'] = "Paints";
        $data['img'] = '4'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Paints");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 1;
        $data['name'] = "Jeans";
        $data['img'] = '5'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Jeans");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();


        // ///////////////////////////
        //////////////////////////
        // /////////////////////////

        $data = new SubCategory();
        $data['category_id'] = 2;
        $data['name'] = "Bags";
        $data['img'] = '1'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Bags");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 2;
        $data['name'] = "Shoes";
        $data['img'] = '2'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Shoes-d");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 2;
        $data['name'] = "Skirt";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Skirt");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 2;
        $data['name'] = "Coats";
        $data['img'] = '4'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Coats");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 2;
        $data['name'] = "Bags";
        $data['img'] = '5'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Bags-d");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();


        // // ///////////////////////////
        // // //////////////////////////
        // // /////////////////////////

        $data = new SubCategory();
        $data['category_id'] = 3;
        $data['name'] = "Bags";
        $data['img'] = '1'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Bags-e");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 3;
        $data['name'] = "Shoes";
        $data['img'] = '2'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Shoes-dd");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 3;
        $data['name'] = "Shirts";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Shirts-a");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 3;
        $data['name'] = "Paints";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1; 
        $data['slug'] = str_slug("Paints-w");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 3;
        $data['name'] = "Jeans";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Jeans-a");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();



        // // ///////////////////////////
        // // //////////////////////////
        // // /////////////////////////

        $data = new SubCategory();
        $data['category_id'] = 4;
        $data['name'] = "Clothing";
        $data['img'] = '1'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Clothing");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 4;
        $data['name'] = "Shoes";
        $data['img'] = '2'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Shoes-v");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 4;
        $data['name'] = "Bags";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Bags-s");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 4;
        $data['name'] = "Vintage";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Vintage");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 4;
        $data['name'] = "Activeware";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Activeware");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();





        // // ///////////////////////////
        // // //////////////////////////
        // // /////////////////////////

        $data = new SubCategory();
        $data['category_id'] = 5;
        $data['name'] = "Hats";
        $data['img'] = '1'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Hats");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 5;
        $data['name'] = "Wallet";
        $data['img'] = '2'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Wallet");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 5;
        $data['name'] = "Bags";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Bags-yt");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 5;
        $data['name'] = "Belts";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Belts");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 5;
        $data['name'] = "Braces";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $faker->text;
        $data['desc'] = $faker->text;$data['language_id'] = 1;
        $data['slug'] = str_slug("Braces");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();


































        $small_desc = "ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد ";

        $desc = "ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد 
        ويُستخدم في صناعات المطابع ودور النشر. كان لوريم إيبسوم ولايزال المعيار للنص الشكلي منذ القرن الخامس عشر عندما قامت مطبعة مجهولة برص مجموعة من الأحرف بشكل عشوائي أخذتها من نص، لتكوّن كتيّب بمثابة دليل أو مرجع شكلي لهذه الأحرف. خمسة قرون من الزمن لم تقضي على هذا النص، بل انه حتى صار مستخدماً وبشكله الأصلي في الطباعة والتنضيد " ;









        //  arabic  



        $data = new SubCategory();
        $data['category_id'] = 6;
        $data['name'] = "تيشيرت";
        $data['img'] = '1'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("تيشيرت");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 6;
        $data['name'] = "احذية";
        $data['img'] = '2'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("احذية");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 6;
        $data['name'] = "تيشيرت  بولو";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("تيشيرت  بولو");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 6;
        $data['name'] = "بنطلون";
        $data['img'] = '4'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("بنطلون");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 6;
        $data['name'] = "جينز";
        $data['img'] = '5'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("جينز");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();


        // ///////////////////////////
        // //////////////////////////
        // /////////////////////////

        $data = new SubCategory();
        $data['category_id'] = 7;
        $data['name'] = "شنط";
        $data['img'] = '1'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("شنط");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 7;
        $data['name'] ="  احذية جديدة ";
        $data['img'] = '2'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("  احذية جديدة ");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 7;
        $data['name'] = "جيبة";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("جيبة");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 7;
        $data['name'] = "جاكت ";
        $data['img'] = '4'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("جاكت ");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 7;
        $data['name'] = "حقئب جديدة";
        $data['img'] = '5'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("حقئب جديدة");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();


        // ///////////////////////////
        // //////////////////////////
        // /////////////////////////

        $data = new SubCategory();
        $data['category_id'] = 8;
        $data['name'] = "حقائب فرنسية";
        $data['img'] = '1'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("حقائب فرنسية");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 8;
        $data['name'] = "احذية  مستوردة";
        $data['img'] = '2'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("احذية  مستوردة");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 8;
        $data['name'] = " جيبة  محجبات";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug(" جيبة  محجبات");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 8;
        $data['name'] = "شروال";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2; 
        $data['slug'] = str_slug("شروال");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 8;
        $data['name'] = "جينز سترة";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("جينز سترة");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();



        // ///////////////////////////
        // //////////////////////////
        // /////////////////////////

        $data = new SubCategory();
        $data['category_id'] = 9;
        $data['name'] = "ملابس جديدة ";
        $data['img'] = '1'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("ملابس جديدة ");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 9;
        $data['name'] = "احذية ا جلد";
        $data['img'] = '2'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("احذية ا جلد");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 9;
        $data['name'] = "حائب حريمى  ";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("حائب حريمى  ");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; 
        $data->save();

        $data = new SubCategory();
        $data['category_id'] = 9;
        $data['name'] = "فينتاج  ";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("فينتاج  ");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 9;
        $data['name'] = "اكتف وير";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("اكتف وير");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();





        // ///////////////////////////
        // //////////////////////////
        // /////////////////////////

        $data = new SubCategory();
        $data['category_id'] = 10;
        $data['name'] = "قبعات ";
        $data['img'] = '1'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("قبعات ");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 10;
        $data['name'] = "محفظات ";
        $data['img'] = '2'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("محفظات ");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 10;
        $data['name'] = "حقائب ظهر ";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("حقائب ظهر ");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 10;
        $data['name'] = "بيلتس";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("بيلتس");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();

        $data = new SubCategory();
        $data['category_id'] = 10;
        $data['name'] = "حظاظات ";
        $data['img'] = '3'.'.jpg';
        $data['small_desc'] = $small_desc;
        $data['desc'] = $desc; 
        $data['language_id'] = 2;
        $data['slug'] = str_slug("حظاظات ");
        $data['status'] = "yes";
        $data['show_in_homePage'] = "yes"; $data->save();










    }

}
