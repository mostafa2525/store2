<?php

use App\Models\Client\Client;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for($i=0; $i<5; $i++)
        {
            Client::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'mobile1' => '01000002344',
                'status' => 'active',
                'password' => bcrypt('123456')
            ]);
        }
    }
}
