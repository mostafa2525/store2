<?php 

namespace App\Helpers\Classes;

class Code
{

    public static function getNewOrder()
    {
        $latestOrder = \App\Models\Product\Order::select('code')->orderBy('id', 'DESC')->first();
        $latestCode = $latestOrder['code'];
        $settings = \App\Models\Setting::select('order_code_string')->first();
        $codeStr = $settings['order_code_string'];

        if($latestOrder == null)
        {
            $codeNum = '00001';
        } else 
        {
            $splitCode = explode($codeStr, $latestCode);
            if(sizeof($splitCode) == 2 && preg_match('/^[0-9]*$/', $splitCode[1]))
            {
                $codeNum = $splitCode[1] += 1;
                $codeNum = str_pad((string)$codeNum, 5, "0", STR_PAD_LEFT); 
            } else 
            {
                $codeNum = '00001';
            }
        }

        return $codeStr . $codeNum;
    }


    public static function getNewProduct()
    {
        $latestProduct = \App\Models\Product::select('code')->orderBy('id', 'DESC')->first();
        $latestCode = $latestProduct['code'];
        $settings = \App\Models\Setting::select('product_code_string')->first();
        $codeStr = $settings['product_code_string'];

        if($latestProduct == null)
        {
            $codeNum = '00001';
        } else 
        {
            $splitCode = explode($codeStr, $latestCode);
            if(sizeof($splitCode) == 2 && preg_match('/^[0-9]*$/', $splitCode[1]))
            {
                $codeNum = $splitCode[1] += 1;
                $codeNum = str_pad((string)$codeNum, 5, "0", STR_PAD_LEFT); 
            } else 
            {
                $codeNum = '00001';
            }
        }

        return $codeStr . $codeNum;
    }
    
}





























