<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    protected $fillable = ['language_id', 'name1', 'link1', 'name2', 'link2', 'name3', 'link3',
        'name4', 'link4', 'name5', 'link5', 'name6', 'link6'
    ];
}
