<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MobileAd extends Model
{
    protected $fillable = ['language_id', 'img1', 'link1', 'img2', 'link2', 'img3', 'link3',
        'img4', 'link4', 'img5', 'link5', 'img6', 'link6'
    ];

}
