<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminGroup extends Model
{
    protected $fillable = [
        'name', 'desc', 'permissions'
    ];

    public function admins()
    {
        return $this->hasMany('App\Models\Admin','group_id','id');
    }

    public function permissions()
    {
        return $this->hasMany('App\Models\AdminPermission','group_id','id');
    }
}
