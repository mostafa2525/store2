<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product\Color;
use App\Models\Product\Size;

class Product extends Model
{
    

    protected $fillable = ['name','category_id','language_id','img','sub_category_id','qty','slug','show_in_homePage','code','price','offer','featured','desc','small_desc','tags'];


    public function category()
    {
        return $this->hasOne('App\Models\Category','id','category_id');
   
    }

    public function sub()
    {
    	return $this->hasOne('App\Models\SubCategory','id','sub_category_id');
    }


    // print color of product 
    public function getColor($id)
    {
    	return Color::where('id',$id)->first();
    }


    // print color of product 
    public function getSize($id)
    {
        return Size::where('id',$id)->first();
    }


    public function allImages()
    {
        return $this->hasMany('App\Models\Product\Pimage','product_id','id');
    }



    public function allSizes()
    {
        return $this->hasMany('App\Models\Product\Psize','product_id','id');
    }



    public function allColors()
    {
        return $this->hasMany('App\Models\Product\Pcolor','product_id','id');
    }




}
