<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product\Psize;


class Size extends Model
{
    protected $fillable = ['language_id', 'title', 'desc'];

    public function getSize($sizeId,$productId)
    {
    	return Psize::where('size_id',$sizeId)->where('product_id',$productId)->first();
    }
}
