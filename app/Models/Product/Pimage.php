<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Pimage extends Model
{
    protected $fillable = ['product_id', 'image'];
    
}
