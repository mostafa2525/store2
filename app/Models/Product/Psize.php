<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Psize extends Model
{
    protected $fillable = ['product_id', 'size_id'];

    public function sizeData()
    {
    	return $this->hasOne('App\Models\Product\Color','id','size_id');
    }
    
}
