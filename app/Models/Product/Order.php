<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
     protected $fillable = [
        'name', 'email', 'mobile','address', 'data', 'notes', 'admin_id', 'client_id', 'code'
    ];


    public function content()
    {
        return $this->hasMany('App\Models\Product\OrderContent','order_id','id');
    }

    public function admin()
    {
        return $this->hasOne('App\Models\Admin','id','admin_id');
    }


    public function client()
    {
        return $this->hasOne('App\Models\Client\Client','id','client_id');
    }


    public function gov()
    {
        return $this->hasOne('App\Models\Area\Gov','id','gov_id');
    }
}
