<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use App\Models\Product\Pcolor;

class Color extends Model
{
    protected $fillable = ['language_id', 'title', 'color'];


    public function getColor($colorId,$productId)
    {
    	return Pcolor::where('color_id',$colorId)->where('product_id',$productId)->first();
    }

}
