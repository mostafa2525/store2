<?php

namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;

class Pcolor extends Model
{
    protected $fillable = ['product_id', 'color_id'];

    public function colorData()
    {
    	return $this->hasOne('App\Models\Product\Color','id','color_id');
    }
    
}
