<?php

namespace App\Http\Controllers\Front\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Client\Client;
use Jenssegers\Agent\Agent;


class AuthController extends Controller
{
    

     public function login()
     {
        $agent = new Agent();

        if($agent->isMobile())
        {
           return view('mobile.client.auth.login');

        }
        else
        {
           return view('front2.client.auth.login');
           
        }
     	 
     }


      // login authencation 
  	public function doLogin(Request $request)
    {

    	$request->validate([
    		'email'=>'required|email|max:30',
    		'password'=>'required|max:30',
    	]);
    	$remember = request()->has('remember') ? true: false;
    	if(\Auth::guard('client')->attempt(['email'=>request('email'),'password'=>request('password'),'status'=>'active']))
    	{
            if(\Cart::getTotalQuantity())
            {
                return redirect(route('front.get.order.checkout'));
            }
            else
            {

    		  return redirect(route('front.get.home.index'));
            }
    	}
    	else
    	{
    		return back()->withErrors(trans('site.errors.dataNotCorrect'));
    	}
    }// end do login function 


















    public function register()
    {

        $agent = new Agent();

        if($agent->isMobile())
        {
           return view('mobile.client.auth.register');
        }
        else
        {
           return view('front2.client.auth.login');

        }


     	
    }





    public function doRegister(Request $request)
    {
    	$request->validate([

    		'name'=>'required|string|max:100',
    		'email'=>'required|email|unique:clients,email|max:100',
    		'password'=>'required|string|max:50|min:6',
    		'confirm-password'=>'required|string|same:password|max:50',

    	]);


    	$data = new Client();
    	$data->name = $request->name;
        $data->email = $request->email;
    	$data->mobile1 = $request->mobile1;
    	$data->status = "active";
    	$data->password = bcrypt($request->password);
    	$data->save();




        if(\Auth::guard('client')->attempt(['email'=>request('email'),'password'=>request('password'),'status'=>'active']))
        {
            if(\Cart::getTotalQuantity())
            {
                return redirect(route('front.get.order.checkout'));
            }
            else
            {

              return redirect(route('front.get.home.index'));
            }
        }
        else
        {
            return back()->withErrors(trans('site.errors.dataNotCorrect'));
        }



    }


     // client logout 

    public function logout()
    {
        clientAuth()->logout();
        return redirect(route('front.get.home.index'));
    } // end logout function 




}
