<?php

namespace App\Http\Controllers\Front\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Client\Client;
use App\Mail\ForgotMail;
use DB;
use Jenssegers\Agent\Agent;


class ForgotController extends Controller
{
    

     public function forgot()
     {

        $agent = new Agent();

        if($agent->isMobile())
        {
           return view('mobile.client.auth.forgot');
        }
        else
        {
           return view('front2.client.auth.forgot');

        }


     }




    public function forgotPassword(Request $request)
    {
    	 $check  = $request->validate([
    		'email'=>'required|email|exists:clients,email',

    	]);

        // $check = Client::where('email',$request->email)->first();

        if($check)
        {

            $token = str_random(32);
            DB::table('password_resets')->where('email','=',$request->email)->delete();
            DB::table('password_resets')->insert(['email' =>$request->email, 'token' => $token]);
            \Mail::to($request->email)->send(new ForgotMail($token));
            $message['success'] = trans('frontSite.checkYourEmail');
            return response()->json($message);
        }
        else
        {
            $message['success'] = trans('frontSite.emailNotFound');
            return response()->json($message);
        }

    }






    public function changePassword($token)
    {

        $check = DB::table('password_resets')->where('token','=',$token)->first();
        if($check)
        {
            $data['row'] = $check;

            $agent = new Agent();

            if($agent->isMobile())
            {
               return view('mobile.client.auth.newPassword')->with($data);
            }
            else
            {
               return view('front2.client.auth.newPassword')->with($data);
               
            }


            
        }
    }




    public function newPassword(Request $request)
    {

        if($request->ajax())
        {
            $check = DB::table('password_resets')->where('token','=',$request->base_token)->first();
            if($check)
            {
                $request->validate([
                    'password'=>'required|string|max:20|min:6',
                    'confirm-password'=>'required|string|max:20|min:6|same:password',

                ]);

                $new = Client::where('email',$check->email)->first();
                $new->password = bcrypt($request->password);
                $new->save();
                $message['success'] = true;
                return response()->json($message);
            }
            else
            {
                $message['error'] = trans('frontSite.dataNotCorrect');
                return response()->json($message);
            }
        }
        

    }







}
