<?php

namespace App\Http\Controllers\Front\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Client\Client;
use App\Models\Product;
use App\Models\Product\Order;
use App\Models\Product\OrderContent;
use DB;
use Jenssegers\Agent\Agent;



class ProfileController extends Controller
{
    

     public function index()
     {

        $data['latest'] = Product::select('id','name','price','img','offer','category_id','slug','sub_category_id')
        ->where('status','yes')->where('show_in_homePage','yes')
        ->where('language_id',lang_front())->orderBy(DB::raw('RAND()'))
        ->take(10)->get();

        $agent = new Agent();

        if($agent->isMobile())
        {
            return view('mobile.client.profile')->with($data);
        }
        else
        {
            return view('front2.client.profile')->with($data);
            // return view('mobile.client.profile')->with($data);

            
        }



     	
     }



    public function editProfile(Request $request)
    {
    	$request->validate([

    		'name'=>'required|string|max:100',
    		'address'=>'required|string|max:300',
    		'mobile1'=>'required|string|max:100',
    		'mobile2'=>'nullable|string|max:100',
    		'more_info'=>'nullable|string|max:500',

    	]);


    	$data = Client::find(clientAuth()->user()->id);
    	$data->name = $request->name;
    	$data->address = $request->address;
    	$data->mobile1 = $request->mobile1;
    	$data->mobile2 = $request->mobile2;
    	$data->more_info =nl2br($request->more_info);
    	$data->save();
    	session()->flash('message',trans('frontSite.successEdit'));
    	return back();
    }











       // send message   
    public function editPassword(Request $request)
    {

        if($request->ajax())
        {

            $check = \Hash::check($request->oldPassword, clientAuth()->user()->password);
            if($check)
            {
                $request->validate([

                    'password' => 'required|string|max:20',
                    'confirm-password' => 'required|string|max:20|same:password',

                ]);

                $client = Client::find(clientAuth()->user()->id);
                $client->password = bcrypt(request('password'));
                $client->save();

                $message['success'] = trans('frontSite.updatedSuccess');
                return response()->json($message);
            }
            else
            {
                $message['errors'] =  bcrypt($request->oldPassword);// trans('frontSite.passwordNotCorrect');
                return response()->json($message);
            }
            

            
        }
    }







    public function showDatails($id)
    {

        $check = Order::where('client_id',clientAuth()->user()->id)->where('id',$id)->first();

        if($check)
        {
            $data['rowOrder'] = $check;
            return view('front2.client.showOrder')->with($data);
        }
    }



    public function showDatailsM($id)
    {
        $ch = OrderContent::where('id',$id)->first();
        if($ch)
        {
            $check = Order::where('client_id',clientAuth()->user()->id)->where('id',$ch->order_id)->first();
            if($check)
            {
                $data['rowOrder'] = $ch;
                $data['rowContent'] = $check;
                return view('mobile.client.showOrder')->with($data);
            }
        }
        
    }







}
