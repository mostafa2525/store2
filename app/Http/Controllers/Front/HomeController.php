<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Slider;
use App\Models\Product;
use App\Models\Category;
use App\Models\Blog;
use DB;
use GeoIP;
use App\Models\Visitor;
use App\Models\MobileAd;

use Jenssegers\Agent\Agent;


class HomeController extends Controller
{
    
    // base function for admin 
    public function index()
    {


        // GeoIP::getClientIP()country city postal_code
        // dd($location = GeoIP::getLocation()->ip);
        $data['slider'] = Slider::select('name','description','img','link')
        ->where('status','yes')->orderBy('sort')
        ->where('language_id',lang_front())->take(3)->get();

        $data['latest'] = Product::select('id','name','price','img','offer','category_id','slug','sub_category_id')
        ->where('status','yes')->where('show_in_homePage','yes')
        ->where('language_id',lang_front())->orderBy(DB::raw('RAND()'))
        ->take(10)->get();

        $data['posts'] = Blog::select('name','small_description','img','slug','created_at')
        ->where('status','yes')->orderBy('sort')
        ->where('language_id',lang_front())->take(2)->get();

        
    
        
         $agent = new Agent();


        if($agent->isMobile())
        {
            $data['adv'] = MobileAd::where('language_id',lang_front())->first();
            return view('mobile.index')->with($data);
        }
        else
        {

          return view('front2.index')->with($data);
          
        }

        
        
    }



    // base function for admin 
    public function about()
    {
        

        $agent = new Agent();

        if($agent->isMobile())
        {
            return view('mobile.static.about');
        }
        else
        {

          return view('front2.static.about');

        }
        
    }



    // base function for admin 
    public function help()
    {
        

        $agent = new Agent();

        if($agent->isMobile())
        {
            return view('mobile.static.help');
        }
        else
        {

          return view('front2.static.about');
          
        }
        
    }






    public function geoip(Request $request)
    {
        if($request->ajax())
        {
            //  count visitors 
            $client_ip = GeoIP::getClientIP();
            $date = date('Y-m-d H:i:s', strtotime('-2 hour'));
            $check_client = Visitor::where('ip',$client_ip)
            ->where('created_at','>=',$date)->first();

            // dd($date);

            // dd($check_client);
            if(!$check_client)
            {
                $newVisitor = new Visitor();
                $newVisitor->ip = $client_ip;
                $newVisitor->country = GeoIP::getLocation()->country;
                $newVisitor->postal_code = GeoIP::getLocation()->postal_code;
                $newVisitor->lat = GeoIP::getLocation()->lat;
                $newVisitor->lon = GeoIP::getLocation()->lon;
                $newVisitor->save();
            }

        }
    }

}
