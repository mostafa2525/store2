<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Product;
use App\Models\Setting;
use App\Models\Product\Size;
use Jenssegers\Agent\Agent;



class CartController extends Controller
{
    

    // add to cart 
    public function add(Request $request)
    {
       if($request->ajax())
       {

            $check = Product::where('status','yes')->where('language_id',lang_front())
            ->where('id',$request->prodId)->first();

            // dd($request->all());

            if($check)
            {
                $checkCart = \Cart::get($request->prodId);
                if($checkCart)
                {
                
                    $message['success'] = trans('frontSite.itemAddedToCart');
                    return response()->json($message);
                }
                else
                {
                    if($check->offer){ $price =  $check->price - ($check->price * ($check->offer/100)) ;  }
                     else {$price = $check->price;} 

                    \Cart::add(array(
                        'id' => $request->prodId,
                        'name' => $check->name,
                        'price' => $price,
                        'quantity' => 1,
                        'attributes' => array('img' => $check->img,
                        'catSlug' => $check->category->slug,
                        'subCatslug' => $check->sub->slug,
                        'slug' => $check->slug,
                        'color' => $request->color,
                        'size' => $request->size
                    ),
                        
                        

                    ));

                    $data['row'] = $check;
                    // dd(\Cart::getContent());
                    return view('front2.product.ajax.cart')->with($data);
                }
                
               
            }

            
       }
    }




    // add to cart 
    public function mobileAdd(Request $request)
    {
       if($request->ajax())
       {

            $check = Product::where('status','yes')->where('language_id',lang_front())
            ->where('id',$request->prodId)->first();

            // dd($request->all());

            if($check)
            {
                $checkCart = \Cart::get($request->prodId);
                if($checkCart)
                {
                
                    $message['success'] = trans('frontSite.itemAddedToCart');
                    return response()->json($message);
                }
                else
                {
                    if($check->offer){ $price =  $check->price - ($check->price * ($check->offer/100)) ;  }
                     else {$price = $check->price;} 

                    \Cart::add(array(
                        'id' => $request->prodId,
                        'name' => $check->name,
                        'price' => $price,
                        'quantity' => 1,
                        'attributes' => array('img' => $check->img,
                        'catSlug' => $check->category->slug,
                        'subCatslug' => $check->sub->slug,
                        'slug' => $check->slug,
                        'color' => $request->color,
                        'size' => $request->size,
                        'offer' => $check->offer,
                    ),
                        
                        

                    ));

                    $data['row'] = $check;
                    // dd(\Cart::getContent());
                    return view('mobile.product.ajax.cart')->with($data);
                }
                
               
            }

            
       }
    }












    



    public function remove(Request $request)
    {
        if($request->ajax())
        {

            $check = Product::where('status','yes')->where('language_id',lang_front())
            ->where('id',$request->itemId)->first();

            if($check)
            {
                $checkCart = \Cart::get($request->itemId);
                if($checkCart)
                {
                    \Cart::remove($request->itemId);
                    $message['success'] = trans('frontSite.itemCartRemoved');
                    return response()->json($message);
                }
            }
        }
    }







    public function show()
    {
        $agent = new Agent();

        if($agent->isMobile())
        {
            return view('mobile.product.cart');
        }
        else
        {
            return view('front2.product.cart');
            // return view('mobile.product.cart');
            
        }
    }






    //  update datA IN CART
    public function update(Request $request)
    {
        // $request->validate([])
        if($request->qunt)
        {
            $i=0;
            foreach($request->qunt as $q)
            {

                \Cart::update($request->prodCart[$i], array(
                  'quantity' => array(
                      'relative' => false,
                      'value' => $q
                  ),
                ));
                $i++;
            }
                return back();
            
        }
    }




     //  update datA IN CART
    public function edit(Request $request)
    {

        // dd($request->all());
        if($request->ajax())
        {
            if($request->qty)
            {
               
                \Cart::update($request->cartId, array(
                  'quantity' => array(
                      'relative' => false,
                      'value' => (int) $request->qty
                  ),
                ));

                $message['qty'] = (int) $request->qty;
                $message['toatal'] = \Cart::getSubTotal();
                $message['toatalWithCharge'] = \Cart::getSubTotal() + Setting::where('language_id',lang_front())->first()->price_charge;
                return response()->json($message);
            }
        }
    }





    public function clear()
    {
        \Cart::clear();
        return back();
    }




    public function delete($id)
    {
        $checkCart = \Cart::get($id);
        if($checkCart)
        {
            \Cart::remove($id);
        }

        return back();
       
    }





}
