<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Product;
use App\Models\Product\Size;
use App\Models\Product\Pimage;
use App\Models\Product\Psize;
use App\Models\Product\Pcolor;
use Jenssegers\Agent\Agent;



class ProductController extends Controller
{
    
    // base function for admin 
    public function all()
    {
        $data['categories'] = Category::where('language_id',lang_front())
        ->where('status','yes')->latest()->get();
        return view('front2.services.index')->with($data);
    }


    // view data of specific service 
    public function category($slugcat)
    {
        $check = Category::where('language_id',lang_front())->where('slug',$slugcat)
        ->where('status','yes')->first();
        if($check)
        {
            $data['row'] = $check;

            $data['allProducts'] = Product::where('category_id',$check->id)
            ->where('status','yes')->latest()->paginate(20);
            // number of products according to this category
            $data['allProdCat'] = Product::where('category_id',$check->id)
            ->where('status','yes')->count();

            // get sizes
            $data['sizes'] = Size::where('language_id',lang_front())->get();

            //  get three products that has offers random

            $data['offers'] = Product::where('offer','!=','')->where('language_id',lang_front())
            ->where('status','yes')->take(3)->orderByRaw("RAND()")->get();

            //  get three products that has offers random

            $data['fetured'] = Product::where('featured','yes')->where('language_id',lang_front())
            ->where('status','yes')->take(3)->orderByRaw("RAND()")->get();

            $agent = new Agent();


            if($agent->isMobile())
            {
                return view('mobile.product.cat')->with($data);
            }
            else
            {
                return view('front2.product.cat')->with($data);

            }

            
        }
    }





    // view data of specific service 
    public function subCategory($slugcat,$slugSubCat)
    {
        //  check from category 
        $check = Category::where('language_id',lang_front())->where('slug',$slugcat)
        ->where('status','yes')->first();

        if($check)
        {
            //  check from sub category 
            $checkSub = SubCategory::where('language_id',lang_front())->where('slug',$slugSubCat)
            ->where('category_id',$check->id)
            ->where('status','yes')->first();

            if($checkSub)
            {
                    $data['row'] = $check;
                    $data['rowSub'] = $checkSub;
                    $data['allProducts'] = Product::where('sub_category_id',$checkSub->id)
                    ->where('status','yes')->latest()->paginate(20);

                    // get sizes
                    $data['sizes'] = Size::where('language_id',lang_front())->get();

                    //  get three products that has offers random

                    $data['offers'] = Product::where('offer','!=','')->where('language_id',lang_front())
                    ->where('status','yes')->take(3)->orderByRaw("RAND()")->get();

                    //  get three products that has offers random

                    $data['fetured'] = Product::where('featured','yes')->where('language_id',lang_front())
                    ->where('status','yes')->take(3)->orderByRaw("RAND()")->get();

                    return view('front2.product.sub')->with($data);

             }
        }
    }







    // view data of specific product 
    public function show($slugcat,$slugSubCat,$slugProduct)
    {
        //  check from category 
        $check = Category::where('language_id',lang_front())->where('slug',$slugcat)
        ->where('status','yes')->first();

        if($check)
        {


            //  check from sub category 
            $checkSub = SubCategory::where('language_id',lang_front())->where('slug',$slugSubCat)
            ->where('category_id',$check->id)
            ->where('status','yes')->first();

            if($checkSub)
            {

                // dd($checkSub->id);

                //  check from product 
                $checkProduct = Product::where('language_id',lang_front())->where('slug',$slugProduct)
                ->where('category_id',$check->id)
                ->where('sub_category_id',$checkSub->id)
                ->where('status','yes')->first();

                if($checkProduct)
                {
                    $data['row'] = $checkProduct;
                    $data['rowSub'] = $checkSub;
                    $data['rowCat'] = $check;
                    //  get three products that has offers random
                    $data['fetured'] = Product::where('featured','yes')->where('language_id',lang_front())
                    ->where('status','yes')->take(4)->orderByRaw("RAND()")->get();

                    //  get three products that has offers random
                    $data['related'] = Product::where('language_id',lang_front())
                    ->where('sub_category_id',$checkSub->id)
                    ->where('status','yes')->take(4)->orderByRaw("RAND()")->get();

                    $data['images'] = Pimage::where('product_id',$checkProduct->id)->get();


                    $agent = new Agent();


                    if($agent->isMobile())
                    {
                        return view('mobile.product.show')->with($data);
                    }
                    else
                    {
                        return view('front2.product.show')->with($data);
                    }

                }


            }
        }
    }














    // view data of search
    public function search(Request $request)
    {

        // $products  = Product::when($request->search, function($q) use ($request) {
        //     return $q->where('name','like','%'.$request->search.'%');
        // })->when($request->search, function($q) use ($request) {
        //     return $q->orWhere('desc','like','%'.$request->search.'%');
        // })->when($request->search, function($q) use ($request) {
        //     return $q->orWhere('small_desc','like','%'.$request->search.'%');
        // })->latest()->paginate(20);

        $data['allProducts']  = Product::where('tags','like','%'.$request->search.'%')
        ->where('language_id',lang_front())->paginate(20);


        // get sizes
        $data['sizes'] = Size::where('language_id',lang_front())->get();

        //  get three products that has offers random

        $data['offers'] = Product::where('offer','!=','')->where('language_id',lang_front())
        ->where('status','yes')->take(3)->orderByRaw("RAND()")->get();

        //  get three products that has offers random

        $data['fetured'] = Product::where('featured','yes')->where('language_id',lang_front())
        ->where('status','yes')->take(3)->orderByRaw("RAND()")->get();

        // $data['allProducts'] = $products;


        $agent = new Agent();

        if($agent->isMobile())
        {
           return view('mobile.product.search')->with($data);
        }
        else
        {
           return view('front2.product.search')->with($data);

        }

        
      
    }















    public function filter(Request $request)
    {
        if($request->ajax())
        {
            $min = (int) $request->min;
            $max = (int) $request->max;

            $data['allProducts'] = Product::where('featured','yes')
            ->where('language_id',lang_front())
            ->where('status','yes')
            ->where('price','>=',$min)
            ->where('price','<=',$max)
            ->take(20)->orderByRaw("RAND()")->get();
            
            return view('front2.product.ajax.filter')->with($data);
        }
    }




    public function sortProduct(Request $request)
    {
        if($request->ajax())
        {
            $sortId = (int) $request->sortId;

            switch ($sortId) {
                case 1:
                    $data['allProducts'] = Product::where('featured','yes')
                    ->where('language_id',lang_front())
                    ->where('status','yes')
                    ->orderBy('price','DESC')
                    ->take(50)->orderByRaw("RAND()")->get();
                    break;

            case 2:
                    $data['allProducts'] = Product::where('featured','yes')
                    ->where('language_id',lang_front())
                    ->where('status','yes')
                    ->orderBy('price','ASC')
                    ->take(50)->orderByRaw("RAND()")->get();
                    break;

            case 3:
                    $data['allProducts'] = Product::where('featured','yes')
                    ->where('language_id',lang_front())
                    ->where('status','yes')
                    ->orderBy('num_seller','DESC')
                    ->take(50)->orderByRaw("RAND()")->get();
                    break;

                    
                
                default:
                    # code...
                    break;
            }

            
            
            return view('mobile.product.ajax.filter')->with($data);
        }
    }





}
