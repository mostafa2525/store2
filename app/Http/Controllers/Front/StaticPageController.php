<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\StaticPage;


class StaticPageController extends Controller
{
    




    // view data of specific service 
    public function show($slugPage)
    {
        $check = StaticPage::where('language_id',lang_front())
        ->where('status','yes')->where('slug',$slugPage)->first();
        if($check)
        {
            $data['posts'] = StaticPage::select('name','slug','img','created_at','description')->where('status','yes')
            ->where('language_id',lang_front())->latest()->take(8)->get();
            $data['row'] = $check;
            return view('front2.static.show')->with($data);
        }
    }

}
