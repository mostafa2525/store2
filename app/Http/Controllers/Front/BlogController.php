<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\Blog;


class BlogController extends Controller
{
    
    // base function for admin 
    public function index()
    {
        $data['posts'] = Blog::select('name','slug','small_description','img','created_at')
        ->where('status','yes')
        ->where('language_id',lang_front())->paginate(6);

        $data['latest'] = Blog::select('name','slug')
        ->where('language_id',lang_front())->latest()->take(6)->get();

        return view('front2.blog.index')->with($data);
    }




    // view data of specific service 
    public function show($slugBlog)
    {
        $check = Blog::where('language_id',lang_front())
        ->where('status','yes')->where('slug',$slugBlog)->first();
        if($check)
        {
            $data['posts'] = Blog::select('name','slug','img','created_at','small_description')->where('status','yes')
            ->where('language_id',lang_front())->latest()->take(8)->get();
            $data['row'] = $check;
            return view('front2.blog.show')->with($data);
        }
    }

}
