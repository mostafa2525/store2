<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Product;
use App\Models\Product\Size;
use App\Models\Area\Gov;
use App\Models\Product\Order;
use App\Models\Product\OrderContent;
use App\Helpers\Classes\Code;

use App\Models\Setting;
use Jenssegers\Agent\Agent;
use App\Mail\OrderMail;





class OrderController extends Controller
{
    

   public function checkout()
   {
      if(clientAuth()->user())
      {

        if(\Cart::getTotalQuantity())
        {


           $data['govs'] = Gov::select('id','name')->where('language_id',lang_front())->get();

           $agent = new Agent();

          if($agent->isMobile())
          {
             return view('mobile.order.checkout')->with($data);
          }
          else
          {
             return view('front2.order.checkout')->with($data);
             // return view('mobile.order.checkout')->with($data);

             
          }
        }
        else
        {
          return redirect(route('front.get.home.index'));
        }


         
      }
      else
      {
         return redirect(route('front.client.get.auth.login'));
      }
   }

   public function sendOrder(Request $request)
   {

      // dd($request->all());
      $request->validate([

            'gov_id'=>'required|numeric|exists:govs,id',
            'name'=>'required|string|max:100',
            'address'=>'required|string|max:300',
            'mobile'=>'required|numeric|digits_between:10,12',
            'city'=>'required|string|max:50',

      ]);


        $data = new Order();
        $data['name'] = $request->name;
        $data['address'] = $request->address;
        $data['email'] = clientAuth()->user()->email;
        $data['mobile'] = $request->mobile;
        $data['gov_id'] = $request->gov_id;
        $data['city'] = $request->city;
        $data['client_id'] = clientAuth()->user()->id;
        $data['code'] = Code::getNewOrder();

        $data->save();

        $setting = Setting::where('language_id',lang_front())->first();

        foreach(\Cart::getContent() as $cart)
        {
            $content = new OrderContent();
            $content->order_id                        = $data->id;
            $content->product_id                      = $cart->id;
            $content->quantity                        = $cart->quantity;
            $content->color                           = $cart->attributes->color;
            $content->size                            = $cart->attributes->size;
            $content->price_at_this_time              = $cart->price;
            $content->charge_price_at_this_time       = $setting->price_charge;
            $content->save();
        }

        \Cart::clear();

        $allData = $data;

        \Mail::to(clientAuth()->user()->email)->send(new OrderMail($allData));
        if (\Mail::failures()) 
        {
            $message['success'] = trans('frontSite.orderSuccess');
            return response()->json($message);
        }
        $message['success'] = trans('frontSite.orderSuccess');
        return response()->json($message);
        

   }




}
