<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\MessageReplyRequest;
use App\Http\Controllers\Controller;
use App\Models\Message;


class MessageController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:messages-show', ['only' => 'index']);
        $this->middleware('permission:messages-delete', ['only' => 'delete', 'deleteMulti']);
    }

    public function index()
    {

    	$data['messages'] = Message::all();
    	return view('admin.message.index')->with($data);
    }




    // deleteing row from db  ( soft delete )
    public function delete($id)
    {
    	Message::findOrFail($id)->delete();
    	session()->flash('message',trans('site.deleted_success'));
    	return redirect(route('admin.get.message.index'));

    }

    // delete mutli row from table 
    public function deleteMulti(Request $request)
    {
    	foreach ($request->deleteMulti as  $value) 
    	{
    		Message::findOrFail($value)->delete();
	    	
    	}

    	session()->flash('message',trans('site.deleted_success'));
	    return redirect(route('admin.get.message.index'));
    }


    public function reply($id)
    {
        $data['id'] = $id;
    	return view('admin.message.reply')->with($data);
    }

    public function sendReply(MessageReplyRequest $request)
    {
        $data = $request->validated();
        $title = $data['title'];
        $body = $data['body'];
        $email = Message::select('email')->where('id', $data['id'])->first()['email'];

        \Mail::to($email)->send(new \App\Mail\MessageReplyMail($title, $body));

    	session()->flash('message',trans('site.sent_success'));
	    return redirect(route('admin.get.message.index'));
    }

}
