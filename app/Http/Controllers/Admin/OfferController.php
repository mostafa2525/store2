<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\OfferRequest;
use App\Http\Controllers\Controller;

use App\Models\Offer;

class OfferController extends Controller
{
    public function index()
    {
        $data['row'] = Offer::where('language_id',language())->first();
    	return view('admin.offer.index')->with($data);
    }

    public function update(OfferRequest $request)
    {
    	$data = $request->validated();
    	// updating data in db
    	Offer::where('language_id', language())->update($data);
    	session()->flash('message',trans('site.updated_success'));
    	return redirect(route('admin.get.offer.index'));
    }
    
}
