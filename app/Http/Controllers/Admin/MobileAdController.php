<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\MobileAdRequest;
use App\Http\Controllers\Controller;
use App\Models\MobileAd;

use Image;
use Storage;
use App\Helpers\Classes\UploadClass;


class MobileAdController extends Controller
{
    public function index()
    {
        $data['row'] = MobileAd::where('language_id',language())->first();
    	return view('admin.mobileAd.index')->with($data);
    }

    // storing data in db 
    public function update(MobileAdRequest $request)
    {
        $data = $request->validated();
        $old = MobileAd::findOrFail($request->id);
        foreach (range(1, 6) as $i) {
            $link = 'link'. $i;
            $img = 'img'. $i;

            if($request->hasFile($img))
            {
                // delete old image from server
                if($old['img'. $i])
                {
                    Storage::disk('public_uploads')->delete(MOBILEAD_PATH.$old[$img]);
                    Storage::disk('public_uploads')->delete(MOBILEAD_PATH.'small/'.$old[$img]);
                    Storage::disk('public_uploads')->delete(MOBILEAD_PATH.'medium/'.$old[$img]);
                }
                // UPLOADS_PATH.MOBILEAD_PATH 
                // -> static path ( you can change it from helpers/files/basehelper)
                $image = UploadClass::uploadFile($request,$img,UPLOADS_PATH.MOBILEAD_PATH,$all=true);
                $data[$img] = $image;
            }
        }

        
        // updating data in db
        MobileAd::where('id', $request->id)->update($data);
        session()->flash('message',trans('site.updated_success'));
        return redirect(route('admin.get.mobilead.index'));
    }
}
