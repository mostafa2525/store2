<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\AdminGroupRequest;
use App\Http\Controllers\Controller;
use App\Models\AdminGroup;
use App\Models\Module;
use App\Models\Admin;
use function GuzzleHttp\json_encode;

class AdminGroupController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:adminGroups-show', ['only' => 'index']);
        $this->middleware('permission:adminGroups-add', ['only' => 'add', 'store']);
        $this->middleware('permission:adminGroups-edit', ['only' => 'edit', 'update']);
        $this->middleware('permission:adminGroups-delete', ['only' => 'delete', 'deleteMulti']);
    }


    public function index(){
        $data['groups'] = AdminGroup::all();
        return view('admin.adminGroup.index')->with($data);
    }

    // return view of adding data 
    public function add()
    {
        $data['crud_modules'] = Module::select('id', 'name')->where('type', 'crud')->get();
        $data['order_types'] = ['pending', 'shipping', 'canceled', 'accepted', 'refused'];
    	return view('admin.adminGroup.add')->with($data);
    }


    // storing data in db 
    public function store(AdminGroupRequest $request)
    {
        $data = $request->validated();
        $permissions = [];

        foreach ($data as $key => $value) {
            if(!in_array($key, ['name', 'desc'])){
                if(request($key) == null){
                    $permissions[$key] = 'no';
                } else {
                    $permissions[$key] = $data[$key];
                }
            }
        }

        $data['permissions'] = json_encode($permissions);
    	AdminGroup::create($data); // inserting data 
    	session()->flash('message',trans('site.added_success'));
    	return redirect(route('admin.get.adminGroup.index'));
    }

    // return view of editing data 
    public function edit($id)
    {
        $data['row'] = AdminGroup::findOrFail($id);
        $data['crud_modules'] = Module::select('id', 'name')->where('type', 'crud')->get();
        $data['order_types'] = ['pending', 'shipping', 'canceled', 'accepted', 'refused'];
    	return view('admin.adminGroup.edit')->with($data);
    }



    // storing data in db 
    public function update(AdminGroupRequest $request)
    {
    	$data = $request->validated();
        $permissions = [];

        foreach ($data as $key => $value) {
            if(!in_array($key, ['name', 'desc'])){
                if(request($key) == null){
                    $permissions[$key] = 'no';
                } else {
                    $permissions[$key] = $data[$key];
                }
                unset($data[$key]);
            }
        }

        $data['permissions'] = json_encode($permissions);

    	AdminGroup::where('id', $request->id)->update($data);
    	session()->flash('message',trans('site.updated_success'));
    	return redirect(route('admin.get.adminGroup.index'));
    }

    // deleteing row from db  ( soft delete )
    public function delete($id)
    {
        AdminGroup::findOrFail($id)->delete();
    	session()->flash('message',trans('site.deleted_success'));
    	return back();

    }

    // delete mutli row from table 
    public function deleteMulti(Request $request)
    {
    	foreach ($request->deleteMulti as  $value) 
    	{
    		AdminGroup::findOrFail($value)->delete();
    	}

    	session()->flash('message',trans('site.deleted_success'));
	    return back();
    }

}
