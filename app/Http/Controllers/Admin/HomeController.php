<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use App\Models\Product;
use App\Models\Client\Client;
use App\Models\Admin;
use App\Models\Visitor;
use App\Models\Product\Order;

class HomeController extends Controller
{
    
    // base function for admin 
    public function index()
    {
			$admin_id = auth()->guard('admin')->user()->id;

    	$data['totalOrders'] 		= Order::where('admin_id', $admin_id)->orWhere('admin_id', null)->count();
    	$data['totalProducts'] 		= Product::count();


    	$data['pendingOrders'] 		= Order::where('status','pending')->where('admin_id', $admin_id)->orWhere('admin_id', null)->count();
    	$data['shippingOrders'] 	= Order::where('status','shipping')->where('admin_id', $admin_id)->count();
    	$data['acceptedOrders'] 	= Order::where('status','accepted')->where('admin_id', $admin_id)->count();
    	$data['refusedOrders'] 		= Order::where('status','refused')->where('admin_id', $admin_id)->count();
			$data['canceledOrders']     = Order::where('status','canceled')->where('admin_id', $admin_id)->count();
			$data['totalClients']       = Client::count();
			$data['totalAdmins']        = Admin::count();
    	$data['totalVisitors'] 	    = Visitor::count();


    	$firstday = date('Y-m-d', strtotime("friday -1 week"));
    	$today = date('Y-m-d',strtotime(now()));

    	// echo $today;

    	// echo "First day of this week: ", $firstday, "\n";  

    	// echo $firstday;

    	$data['weekOrders'] 		= Order::where('created_at','>',$firstday)->where('admin_id', $admin_id)->orWhere('admin_id', null)->count();
    	$data['todayOrders'] 		= Order::where('created_at','>',$today)->where('admin_id', $admin_id)->orWhere('admin_id', null)->count();

    	return view('admin.index')->with($data);


    }
}
