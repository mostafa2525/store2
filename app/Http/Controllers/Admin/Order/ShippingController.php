<?php

namespace App\Http\Controllers\Admin\Order;

use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;
use App\Http\Controllers\Controller;
use App\Models\Product\Order;
use App\Models\Product\OrderContent;

class ShippingController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:orders-show-shipping', ['only' => 'index', 'show']);
        $this->middleware('permission:orders-add-accepted', ['only' => 'toAccepted']);
        $this->middleware('permission:orders-add-refused', ['only' => 'toRefused']);
        $this->middleware('permission:orders-delete-shipping', ['only' => 'delete', 'deleteMulti']);
    }

    public function index()
    {
        $admin_id = auth()->guard('admin')->id();
        $is_important = auth()->guard('admin')->user()->is_important;
        if($is_important == 'yes')
        {
            $data['orders'] = Order::select('id','name','code','email','mobile')
            ->where('status', 'shipping')
            ->orderBy('status_change_date', 'DESC')
            ->paginate(30);
        } else 
        {
            $data['orders'] = Order::select('id','name','code','email','mobile')
            ->where('status', 'shipping')
            ->where('admin_id', $admin_id)
            ->orderBy('status_change_date', 'DESC')
            ->paginate(30);
        }
           
    	return view('admin.order.shipping.index')->with($data);
    }

    public function show($id)
    {
        $data['order'] = Order::where('id', $id)->first();
        $data['orderContents'] = OrderContent::select('id', 'product_id','quantity','status','size','color')
        ->where('order_id', $id)
    	->get();
    	return view('admin.order.shipping.show')->with($data);
    }

    

    public function toAccepted(Request $request)
    {
        if($request->ajax())
        {
            $order = Order::where('id', $request->id)->first();
            $order->status = 'accepted';
            $order->status_change_date = now();
            $order->save();

            $contents = OrderContent::where('order_id', $request->id)->get();
            foreach ($contents as $content) {
                $content->status = 'accepted';
                $content->save();
            }

            $message['success'] = trans('site.accepted_success');
            return response()->json($message);
        }
    }


    public function toRefused(Request $request)
    {
        if($request->ajax())
        {
            $order = Order::where('id', $request->id)->first();
            $order->status = 'refused';
            $order->status_change_date = now();
            $order->refused_notes = $request->refused_notes;
            $order->save();

            $contents = OrderContent::where('order_id', $request->id)->get();
            foreach ($contents as $content) {
                $content->status = 'refused';
                $content->save();
            }
            
            $message['success'] = trans('site.refused_success');
            return response()->json($message);
        }
    }
    // deleteing row from db  ( soft delete )
    public function delete($id)
    {
        Order::findOrFail($id)->delete();
    	session()->flash('message',trans('site.deleted_success'));
    	return back();
    }

    // delete mutli row from table 
    public function deleteMulti(Request $request)
    {
    	foreach ($request->deleteMulti as  $value) 
    	{
    		Order::findOrFail($value)->delete();
    	}

    	session()->flash('message',trans('site.deleted_success'));
	    return back();
    }

}
