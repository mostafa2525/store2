<?php

namespace App\Http\Controllers\Admin\Order;

use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;
use App\Http\Requests\OrderContentRequest;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Product\Order;
use App\Models\Product\OrderContent;
use App\Helpers\Classes\Code;

class OrderController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:orders-add-pending', ['only' => 'add', 'store']);
        $this->middleware('permission:orders-add-canceled', ['only' => 'toCanceled']);
        $this->middleware('permission:orderContents-accept-refuse', ['only' => 'contentStatus']);
    }

    public function add(){
        $data['products'] = Product::select('id', 'name')->where('language_id',language())->get();

        return view('admin.order.add')->with($data);
    }




    public function search(Request $request)
    {
        $data['order'] = Order::where('code',$request->code)->first();
        return view('admin.order.search')->with($data);
    }


    public function store(OrderRequest $request){

        if($request->ajax()){
            $data = $request->validated();
            
            $data['admin_id'] = auth()->guard('admin')->id();
            $data['code'] = Code::getNewOrder();
            $data['data'] = 'data';
            $data['status'] = 'pending';

            Order::create($data);
            $orderData['order_id'] = Order::orderBy('id', 'DESC')->first()['id'];

            $product_ids = $data['product_id'];
            $quantities = $data['quantity'];

            foreach ($product_ids as $key => $product_id) {
                $orderData['product_id'] = $product_id;
                $orderData['quantity'] = $quantities[$key];
                OrderContent::create($orderData);
            }

    	    $message['success'] = trans('site.orderSuccess');
            return response()->json($message);
        }
    }

    public function contentStatus(Request $request)
    {
        if($request->ajax())
        {
            $content = OrderContent::where('id', $request->id)->first();

            if($request->status == 'refused'){
                $content->status = 'refused';
                $message['success'] = trans('site.accepted_success');
            }
            
            else if($request->status == 'accepted') {
                $content->status = 'accepted';
                $message['success'] = trans('site.refused_success');
            }

            $status = $content->status;
            $content->save();

            return response()->json($status);
        }
    }

    public function toCanceled(Request $request)
    {
        if($request->ajax())
        {
            $order = Order::where('id', $request->id)->first();
            $order->status = 'canceled';
            $order->admin_id = adminAuth()->user()->id;
            $order->status_change_date = now();
            $order->save();

            $contents = OrderContent::where('order_id', $request->id)->get();
            foreach ($contents as $content) {
                $content->status = 'refused';
                $content->save();
            }
            
            $message['success'] = trans('site.canceled_success');
            return response()->json($message);
        }
    }














    public function filter()
    {
        return view('admin.order.filter');

    }




    public function filterOrder(Request $request)
    {
        $orders  = Order::when($request->from, function($q) use ($request) {
            return $q->where('created_at','>=',$request->from);
        })->when($request->to, function($q) use ($request) {
            return $q->where('created_at','<=',$request->to);
        })->when($request->status, function($q) use ($request) {
            if($request->status != "all")
            {
                return $q->where('status','=',$request->status);
            }
        })->latest()->paginate(30);

        $data['orders'] = $orders;
        return view('admin.order.filter')->with($data);

    }




















}
