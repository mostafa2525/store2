<?php

namespace App\Http\Controllers\Admin\Order;

use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;
use App\Http\Controllers\Controller;
use App\Models\Product\Order;
use App\Models\Product\OrderContent;

class AcceptedController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:orders-show-accepted', ['only' => 'index', 'show']);
        $this->middleware('permission:orders-delete-accepted', ['only' => 'delete', 'deleteMulti']);
    }

    public function index()
    {
        $admin_id = auth()->guard('admin')->id();
        $is_important = auth()->guard('admin')->user()->is_important;
        if($is_important == 'yes')
        {
            $data['orders'] = Order::select('id','name','code','email','mobile')
            ->where('status', 'accepted')
            ->orderBy('status_change_date', 'DESC')
            ->paginate(30);
        } else 
        {
            $data['orders'] = Order::select('id','name','code','email','mobile')
            ->where('status', 'accepted')
            ->where('admin_id', $admin_id)
            ->orderBy('status_change_date', 'DESC')
            ->paginate(30);
        }
        
    	return view('admin.order.accepted.index')->with($data);
    }

    public function show($id)
    {
        $data['order'] = Order::where('id', $id)->first();
        $data['orderContents'] = OrderContent::select('id', 'product_id','quantity','status','size','color')
        ->where('order_id', $id)
        ->get();
        
    	return view('admin.order.accepted.show')->with($data);
    }

    // deleteing row from db  ( soft delete )
    public function delete($id)
    {
        Order::findOrFail($id)->delete();
    	session()->flash('message',trans('site.deleted_success'));
    	return back();
    }

    // delete mutli row from table 
    public function deleteMulti(Request $request)
    {
    	foreach ($request->deleteMulti as  $value) 
    	{
    		Order::findOrFail($value)->delete();
    	}

    	session()->flash('message',trans('site.deleted_success'));
	    return back();
    }

}
