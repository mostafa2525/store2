<?php

namespace App\Http\Controllers\Admin\Order;

use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;
use App\Http\Controllers\Controller;
use App\Models\Product\Order;
use App\Models\Product\OrderContent;

class PendingController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:orders-show-pending', ['only' => 'index', 'show']);
        $this->middleware('permission:orders-add-shipping', ['only' => 'toShipping']);
        $this->middleware('permission:orders-delete-pending', ['only' => 'delete', 'deleteMulti']);

    }

    public function index()
    {
        $admin_id = auth()->guard('admin')->id();
        $is_important = auth()->guard('admin')->user()->is_important;
        if($is_important == 'yes')
        {
            $data['orders'] = Order::select('id','admin_id','name','code','email','mobile')
            ->where('status', 'pending')
            ->orderBy('id', 'DESC')
            ->paginate(30);
        } else 
        {
            $data['orders'] = Order::select('id','admin_id','name','code','email','mobile')
            ->where('admin_id', null)
            ->orWhere('admin_id', $admin_id)
            ->where('status', 'pending')
            ->orderBy('id', 'DESC')
            ->paginate(30);
        }
        
    	return view('admin.order.pending.index')->with($data);
    }

    public function show($id)
    {
        $data['order'] = Order::where('id', $id)->first();
        $data['orderContents'] = OrderContent::select('id', 'product_id','quantity','status','size','color')
        ->where('order_id', $id)
    	->get();
    	return view('admin.order.pending.show')->with($data);
    }

    public function toShipping(Request $request)
    {
        if($request->ajax())
        {
            $order = Order::where('id', $request->id)->first();
            $order->status = 'shipping';
            $order->status_change_date = now();
            $order->admin_id = auth()->guard('admin')->id();
            $order->save();
            $message['success'] = trans('site.shipped_success');
            return response()->json($message);
        }
    }

    // deleteing row from db  ( soft delete )
    public function delete($id)
    {
        Order::findOrFail($id)->delete();
    	session()->flash('message',trans('site.deleted_success'));
    	return back();
    }

    // delete mutli row from table 
    public function deleteMulti(Request $request)
    {
    	foreach ($request->deleteMulti as  $value) 
    	{
    		Order::findOrFail($value)->delete();
    	}

    	session()->flash('message',trans('site.deleted_success'));
	    return back();
    }

}
