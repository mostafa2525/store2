<?php

namespace App\Http\Controllers\Admin\Order;

use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;
use App\Http\Controllers\Controller;
use App\Models\Product\Order;
use App\Models\Product\OrderContent;

class RefusedController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:orders-show-refused', ['only' => 'index', 'show']);
        $this->middleware('permission:orders-delete-refused', ['only' => 'delete', 'deleteMulti']);
    }
    
    public function index()
    {
        $admin_id = auth()->guard('admin')->id();
        $is_important = auth()->guard('admin')->user()->is_important;
        if($is_important == 'yes')
        {
            $data['orders'] = Order::select('id','name','code', 'refused_notes','email','mobile')
            ->where('status', 'refused')
            ->orderBy('status_change_date', 'DESC')
            ->paginate(30);
        } else 
        {
            $data['orders'] = Order::select('id','name','code', 'refused_notes','email','mobile')
            ->where('status', 'refused')
            ->where('admin_id', $admin_id)
            ->orderBy('status_change_date', 'DESC')
            ->paginate(30);
        }

        
    	return view('admin.order.refused.index')->with($data);
    }

    public function show($id)
    {
        $data['order'] = Order::where('id', $id)->first();
        $data['orderContents'] = OrderContent::select('id', 'product_id','quantity','status','size','color')
        ->where('order_id', $id)
    	->get();
    	return view('admin.order.refused.show')->with($data);
    }

    // deleteing row from db  ( soft delete )
    public function delete($id)
    {
        Order::findOrFail($id)->delete();
        session()->flash('message',trans('site.deleted_success'));
        return back();
    }

    // delete mutli row from table 
    public function deleteMulti(Request $request)
    {
        foreach ($request->deleteMulti as  $value) 
        {
            Order::findOrFail($value)->delete();
        }

        session()->flash('message',trans('site.deleted_success'));
        return back();
    }
}
