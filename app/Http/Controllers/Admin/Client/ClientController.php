<?php

namespace App\Http\Controllers\Admin\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Client\Client;
use App\Models\Product\Order;


class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:clients-show', ['only' => 'index', 'show']);
        $this->middleware('permission:clients-delete', ['only' =>'delete', 'deleteMulti']);
        $this->middleware('permission:clients-edit', ['only' =>'block', 'trust']);
    }

    public function index()
    {
    	$data['clients'] = Client::all();
    	return view('admin.clients.index')->with($data);
    }


    public function show($id)
    {
        $data['row'] = Client::where('id', $id)->first();
    	return view('admin.clients.show')->with($data);
    }




    // deleteing row from db  ( soft delete )
    public function delete($id)
    {
        Client::findOrFail($id)->delete();
    	session()->flash('message',trans('site.deleted_success'));
    	return back();
    }



    // Trust This Client
    public function trust(Request $request)
    {
        if($request->ajax())
        {
            $data = Client::findOrFail($request->id);
            $data->status = "trust";
            $data->save();
            $message['success'] = trans('site.blocked_success');
            return response()->json($message);
        }
    }


    // Block This Client
    public function block(Request $request)
    {

        if($request->ajax())
        {

            $data = Client::findOrFail($request->id);
            $data->status = "block";
            $data->save();
            $message['success'] = trans('site.blocked_success');
            return response()->json($message);

        }

    }





    // public function orders($id)
    // {
    //     Client::findOrFail($id);
    //     $data['orders'] = Order::select('id','name','code','email','mobile')
    //     ->where('client_id', $id)
    //     ->orderBy('id', 'DESC')
    //     ->paginate(30);
        
    //     return view('admin.clients.orders')->with($data);
    // }

    // delete mutli row from table 
    public function deleteMulti(Request $request)
    {
    	foreach ($request->deleteMulti as  $value) 
    	{
    		Client::findOrFail($value)->delete();
	    	
    	}

    	session()->flash('message',trans('site.deleted_success'));
	    return redirect(route('admin.get.message.index'));
    }


}
