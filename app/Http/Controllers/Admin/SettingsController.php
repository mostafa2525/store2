<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\SettingsRequest;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Models\SiteContent;


use Image;
use Storage;
use App\Helpers\Classes\UploadClass;
use App\Helpers\Classes\Code;

class SettingsController extends Controller
{

    public function __construct()
    {
        $this->middleware('permission:baseData-edit', ['only' => 'base', 'updateBase']);
        $this->middleware('permission:seoData-edit', ['only' => 'seo', 'updateSeo']);
        $this->middleware('permission:siteContent-edit', ['only' => 'siteContent', 'homeSiteContent', 'uploadSiteContentImage']);
        $this->middleware('permission:aboutUs-edit', ['only' => 'aboutus', 'updateAboutus']);
        $this->middleware('permission:collections-edit', ['only' => 'collections', 'updateCollections']);
    }


    // return view of adding data 
    
    public function base()
    {
        $data['base'] = Setting::where('language_id',language())->first();
        if(!$data['base'])
        {
            // create instance for this setting language first time 
            $base = new Setting();
            $base->language_id = language();
            $base->save();

            $data['base'] = $base; 
        }
        return view('admin.setting.base')->with($data);

    }





    // storing data in db 
    public function updateBase(SettingsRequest $request)
    {
    	$data = $request->validated();
        // upload image of this module
        if($request->hasFile('logo'))
        {
            $old = Setting::findOrFail($request->id);
            // delete old image from server
            if($old->logo)
            {
                Storage::disk('public_uploads')->delete(SETTINGS_PATH.$old->logo);
            }
            // UPLOADS_PATH.CATEGORY_PATH 
            // -> static path ( you can change it from helpers/files/basehelper)
            $img = UploadClass::uploadRealImage($request,'logo',UPLOADS_PATH.SETTINGS_PATH);
            $data['logo'] = $img;
        }

        $data['pinterest'] = $request->pinterest;
        $data['address'] =  nl2br($request->address);
      // updating data in db

    	Setting::where('id', $request->id)->update($data);
    	session()->flash('message',trans('site.updated_success'));
    	return back();
    }






     // return view of seo data 
    public function seo()
    {
        $data['seo'] = Setting::where('language_id',language())->first();
        if(!$data['seo'])
        {
            // create instance for this setting language first time 
            $seo = new Setting();
            $seo->language_id = language();
            $seo->save();
            $data['seo'] = $seo; 
        }
        $data['seoData'] = json_decode($data['seo']['seo']);
        return view('admin.setting.seo')->with($data);

    }



     // storing data in db 
    public function updateSeo(SettingsRequest $request)
    {


        $data = $request->except('id','_token','setting_type');
        // updating data in db
        $seoData = Setting::find($request->id);
        $seoData->seo = json_encode($data);
        $seoData->save();
        session()->flash('message',trans('site.updated_success'));
        return back();
    }













   //  site content view 
    
   public function siteContent()
   {
       $site_content = SiteContent::where('language_id',language())->first();
       if(!$site_content)
       {
           // create instance for this setting language first time 
           $dataSite = new SiteContent();
           $dataSite->language_id = language();
           $dataSite->save();
           $data['site_content'] = $dataSite; 
       }
       $data['site_content'] = json_decode($site_content->site_content);
       return view('admin.setting.siteContent')->with($data);
   }


   //  site content home 
   
   public function homeSiteContent(Request $request)
   {
       $data = $request->except('_token');
         // updating data in db
       $siteContent = SiteContent::where('language_id',language())->first();
       $site_content_json = json_decode($siteContent->site_content);

       $path =  UPLOADS_PATH.SETTINGS_PATH;
       $name = 'cat_cover';
       $data['cat_cover'] = $this->uploadSiteContentImage($request,$path,$name,1366,273,$site_content_json);

       $data['cat_img'] = $this->uploadSiteContentImage($request,$path,'cat_img',850,250,$site_content_json);
       $data['subCat_cover'] = $this->uploadSiteContentImage($request,$path,'subCat_cover',1366,273,$site_content_json);
       $data['subCat_img'] = $this->uploadSiteContentImage($request,$path,'subCat_img',850,250,$site_content_json);
       $data['about_cover'] = $this->uploadSiteContentImage($request,$path,'about_cover',1366,273,$site_content_json);
       $data['login_cover'] = $this->uploadSiteContentImage($request,$path,'login_cover',1366,273,$site_content_json);
       $data['register_cover'] = $this->uploadSiteContentImage($request,$path,'register_cover',1366,273,$site_content_json);
       $data['cart_cover'] = $this->uploadSiteContentImage($request,$path,'cart_cover',1366,273,$site_content_json);
       $data['checkout_cover'] = $this->uploadSiteContentImage($request,$path,'checkout_cover',1366,273,$site_content_json);
       $data['profile_cover'] = $this->uploadSiteContentImage($request,$path,'profile_cover',1366,273,$site_content_json);
       $data['blog_cover'] = $this->uploadSiteContentImage($request,$path,'blog_cover',1366,273,$site_content_json);


       $siteContent->site_content = json_encode($data);
       $siteContent->save();
       session()->flash('message',trans('site.updated_success'));
       return back();
   }























    //  site content view 
    
   public function mobile()
   {
       $site_content = Setting::where('language_id',language())->first();
       if(!$site_content)
       {
           // create instance for this setting language first time 
           $dataSite = new Setting();
           $dataSite->language_id = language();
           $dataSite->save();
           $data['site_content'] = $dataSite; 
       }
       $data['site_content'] = json_decode($site_content->site_content_mobile);
       return view('admin.setting.siteContentMobile')->with($data);
   }


   //  site content home 
   
   public function siteContentMobile(Request $request)
   {
       $data = $request->except('_token');
         // updating data in db
       $siteContent = Setting::where('language_id',language())->first();
       $site_content_json = json_decode($siteContent->site_content_mobile);

       $siteContent->site_content_mobile = json_encode($data);
       $siteContent->save();
       session()->flash('message',trans('site.updated_success'));
       return back();
   }









       //  upload site content image 


    public function uploadSiteContentImage($request,$path,$name,$w,$h,$siteContent)
    {
        if($request->hasFile($name))
        {
                $img = UploadClass::uploadOneImage($request,$name,$path,$w,$h);
                $image = $img;
        }
        else
        {
              if($siteContent)
              {
                if(isset($siteContent->$name))
                {
                  $image = $siteContent->$name;
                }
                else{
                      $image = '';
                    }
              }
              else{
                $image = '';
              }
          }

        return $image;
        
    }















    // return view of about us page  
    public function aboutus()
    {
        $data['about'] = Setting::where('language_id',language())->first();
        if(!$data['about'])
        {
            // create instance for this setting language first time 
            $aboutus = new Setting();
            $aboutus->language_id = language();
            $aboutus->save();
            $data['about'] = $aboutus; 
        }
        $data['aboutus'] = json_decode($data['about']['who_us']);
        return view('admin.setting.aboutus')->with($data);

    }



     // storing data in db 
    public function updateaboutus(Request $request)
    {

        $data = [];

        //  slug
        if($request->aboutUsSlug != ''){$data['aboutUsSlug'] = slug($request->aboutUsSlug);}
        else{$data['aboutUsSlug'] = slug($request->aboutUsTitle).uniqid();}
         // upload image of this module

        $about = Setting::where('language_id',language())->first();
        $site_content_json = json_decode($about->who_us);


       $data['img1'] = $this->uploadSiteContentImage($request,UPLOADS_PATH.SETTINGS_PATH,'img1',600,400,$site_content_json);

        // if($request->hasFile('img1'))
        // {
        //     $img = UploadClass::uploadOneImage($request,'img1',UPLOADS_PATH.SETTINGS_PATH,600,400);
        //     $data['img1'] = $img;
        // }

        $data['aboutUsTitle'] = $request->aboutUsTitle;
        $data['aboutUsSmallDescription'] = $request->aboutUsSmallDescription;
        $data['aboutUsDescription'] = $request->aboutUsDescription;


        // updating data in db
        $seoData = Setting::find($request->id);
        $seoData->who_us = json_encode($data);
        $seoData->save();
        session()->flash('message',trans('site.updated_success'));
        return back();
    }













        // return view of about us page  
    public function help()
    {
        $data['about'] = Setting::where('language_id',language())->first();
        if(!$data['about'])
        {
            // create instance for this setting language first time 
            $aboutus = new Setting();
            $aboutus->language_id = language();
            $aboutus->save();
            $data['about'] = $aboutus; 
        }
        $data['help'] = json_decode($data['about']['help']);
        return view('admin.setting.help')->with($data);

    }



     // storing data in db 
    public function updateHelp(Request $request)
    {

        $data = $request->except('_token');
        $d = Setting::where('language_id',language())->first();
        $help = json_decode($d->help);


       $data['img1'] = $this->uploadSiteContentImage($request,UPLOADS_PATH.SETTINGS_PATH,'img1',600,400,$help);

        $d->help = json_encode($data);
        $d->save();
        session()->flash('message',trans('site.updated_success'));
        return back();
    }














    // collections 

    

      // return view of about us page  
    public function collections()
    {
        $data['about'] = Setting::where('language_id',language())->first();
        if(!$data['about'])
        {
            // create instance for this setting language first time 
            $aboutus = new Setting();
            $aboutus->language_id = language();
            $aboutus->save();
            $data['about'] = $aboutus; 
        }
        $data['collections'] = json_decode($data['about']['collections']);
        return view('admin.setting.collections')->with($data);

    }



     // storing data in db 
    public function updateCollections(Request $request)
    {
   
       $data = $request->except('_token');

       // dd($data);
         // updating data in db
       $setting = Setting::where('language_id',language())->first();
       $collections = json_decode($setting->collections);


       // dd($collections);

         // upload image of this module
        if($request->hasFile('collectionImg1'))
        {
            $img = UploadClass::uploadOneImage($request,'collectionImg1',UPLOADS_PATH.COLLECTION_PATH,270,530);
            $data['collectionImg1'] = $img;
        }else{if($collections && isset($collections->collectionImg1) ){$data['collectionImg1'] = $collections->collectionImg1;}
          else{$data['collectionImg1'] = '';}
        }

       


        // for($i=1; $i<=4; $i++)
        // {
            //  image 2 


            if($request->hasFile('collectionImg2'))
            {
                $img = UploadClass::uploadOneImage($request,'collectionImg2',UPLOADS_PATH.COLLECTION_PATH,270,250);
                $data['collectionImg2'] = $img;
            }else{
              if($collections)
              {
                if(isset($collections->collectionImg2))
                {
                  $data['collectionImg2'] = $collections->collectionImg2;
                }
                else{
                      $data['collectionImg2'] = '';
                    }
              }
              else{
                $data['collectionImg2'] = '';
              }
            }



             if($request->hasFile('collectionImg3'))
            {
                $img = UploadClass::uploadOneImage($request,'collectionImg3',UPLOADS_PATH.COLLECTION_PATH,270,250);
                $data['collectionImg3'] = $img;
            }else{
              if($collections)
              {
                if(isset($collections->collectionImg3))
                {
                  $data['collectionImg3'] = $collections->collectionImg3;
                }
                else{
                      $data['collectionImg3'] = '';
                    }
              }
              else{
                $data['collectionImg3'] = '';
              }
            }


             if($request->hasFile('collectionImg4'))
            {
                $img = UploadClass::uploadOneImage($request,'collectionImg4',UPLOADS_PATH.COLLECTION_PATH,270,530);
                $data['collectionImg4'] = $img;
            }else{if($collections && isset($collections->collectionImg4) ){$data['collectionImg4'] = $collections->collectionImg4;}
              else{$data['collectionImg4'] = '';}
            }




            if($request->hasFile('collectionImg5'))
            {
                $img = UploadClass::uploadOneImage($request,'collectionImg5',UPLOADS_PATH.COLLECTION_PATH,270,250);
                $data['collectionImg5'] = $img;
            }else{
              if($collections)
              {
                if(isset($collections->collectionImg5))
                {
                  $data['collectionImg5'] = $collections->collectionImg5;
                }
                else{
                      $data['collectionImg5'] = '';
                    }
              }
              else{
                $data['collectionImg5'] = '';
              }
            }



            if($request->hasFile('collectionImg6'))
            {
                $img = UploadClass::uploadOneImage($request,'collectionImg6',UPLOADS_PATH.COLLECTION_PATH,270,250);
                $data['collectionImg6'] = $img;
            }else{
              if($collections)
              {
                if(isset($collections->collectionImg6))
                {
                  $data['collectionImg6'] = $collections->collectionImg6;
                }
                else{
                      $data['collectionImg6'] = '';
                    }
              }
              else{
                $data['collectionImg6'] = '';
              }
            }





            if($request->hasFile('hotdealImg'))
            {
                $img = UploadClass::uploadOneImage($request,'hotdealImg',UPLOADS_PATH.COLLECTION_PATH,560,600);
                $data['hotdealImg'] = $img;
            }else{
              if($collections)
              {
                if(isset($collections->hotdealImg))
                {
                  $data['hotdealImg'] = $collections->hotdealImg;
                }
                else{
                      $data['hotdealImg'] = '';
                    }
              }
              else{
                $data['hotdealImg'] = '';
              }
            }






            
        // }









        // updating data in db
        $setting->collections = json_encode($data);
        $setting->save();
        session()->flash('message',trans('site.updated_success'));
        return back();
    }











































}
