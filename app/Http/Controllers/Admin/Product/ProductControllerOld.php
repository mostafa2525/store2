<?php

namespace App\Http\Controllers\Admin\Product;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\Product;
use App\Models\Product\Color;
use App\Models\Product\Size;

use Image;
use Storage;
use UploadClass;
use App\Helpers\Classes\Code;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:products-show', ['only' => 'index', 'category', 'sub', 'getSub']);
        $this->middleware('permission:products-add', ['only' => 'add', 'store']);
        $this->middleware('permission:products-edit', ['only' => 'edit', 'update', 'seo', 'updateSeo', 'visibility', 'sort']);
        $this->middleware('permission:products-delete', ['only' => 'delete', 'deleteMulti']);
    }

    public function index()
    {

        $data['products'] = Product::select('id','name','language_id','category_id','sub_category_id','status','sort','slug','code')
        ->where('language_id',language())
        ->latest()
        ->paginate(30);
    	return view('admin.products.index')->with($data);
    }





    public function category($id)
    {
        $data['products'] = Product::select('id','name','language_id','category_id','sub_category_id','status','sort','slug')
        ->where('language_id',language())
        ->where('category_id',$id)
        ->orderBy('created_at')->latest()
        ->paginate(30);
        return view('admin.products.index')->with($data);
    }





    public function sub($id)
    {
        $data['products'] = Product::select('id','name','language_id','category_id','sub_category_id','status','sort','slug')
        ->where('language_id',language())
        ->where('sub_category_id',$id)
        ->orderBy('created_at')->latest()
        ->paginate(30);
        return view('admin.products.index')->with($data);
    }







    public function getSub(Request $request)
    {
        if($request->ajax())
        {
            Category::where('language_id',adminAuth()->user()->language_id)
            ->findOrFail($request->catId);
            $data['subcategories'] = SubCategory::where('language_id',adminAuth()->user()->language_id)
            ->where('category_id',$request->catId)->get();



            return view('admin.products.ajax.sub')->with($data);

        }
    }


    // return view of adding data 
    public function add()
    {
        $data['categories'] = Category::select('id','name','language_id')
        ->where('language_id',adminAuth()->user()->language_id)
        ->orderBy('sort')
        ->get();
        // colors 
            $data['colors'] = Color::select('id','title')->where('language_id',language())->get();
            $data['sizes'] = Size::select('id','title')->where('language_id',language())->get();
    	return view('admin.products.add')->with($data);
    }



    // storing data in db 
    public function store(ProductRequest $request)
    {

        if($request->ajax())
        { 
        	$data = $request->validated();
        	$data['language_id'] = language(); // insert language id of this user 

            //  slug
            if($request->slug != ''){$data['slug'] = slug($request->slug);}
            else{$data['slug'] = slug($request->name).uniqid();}

             //  tags
            if($request->tags != ''){$data['tags'] = slug($request->tags);}
            else{$data['tags'] = slug($request->name).uniqid();}

            $sizes = json_encode($request->size);
            $colors = json_encode($request->color);
            $data['sizes'] = $sizes; 
            $data['colors'] = $colors; 

            $data['code'] = Code::getNewProduct();

            // upload image of this module
            if($request->hasFile('img'))
            {
                // UPLOADS_PATH.PRODUCT_PATH 
                // -> static path ( you can change it from helpers/files/basehelper)
               $img = UploadClass:: uploadImageR($request,'img',UPLOADS_PATH.PRODUCT_PATH,450,400,250,200,100,90);
               $data['img'] = $img;
            }

        	// inserting data in db
        	Product::create($data);

            $message['success'] = trans('site.added_success');
            return response()->json($message);
        }
    }







    // return view of editing data 
    public function edit($id)
    {
        $data['categories'] = Category::select('id','name','language_id')
        ->where('language_id',adminAuth()->user()->language_id)
        ->orderBy('sort')
        ->get();
        $data['row'] = Product::findOrFail($id);

        $data['subcategories'] = SubCategory::select('id','name','language_id')
        ->where('category_id',$data['row']['category_id'])->get();

         // colors 
            $data['colors'] = Color::select('id','title')->where('language_id',language())->get();
            $data['sizes'] = Size::select('id','title')->where('language_id',language())->get();
    	return view('admin.products.edit')->with($data);
    }



    // storing data in db 
    public function update(ProductRequest $request)
    {

        if($request->ajax())
        { 

            // dd($request->all());
            $data = $request->validated();
            $old = Product::findOrFail($request->id);

            //  slug
            if($request->slug != ''){$data['slug'] = slug($request->slug);}
            else{$data['slug'] = slug($request->name).uniqid();}

            //  tags
            if($request->tags != ''){$data['tags'] = slug($request->tags);}
            else{$data['tags'] = slug($request->name).uniqid();}

            $sizes = json_encode($request->sizes);
            $colors = json_encode($request->colors);

            $data['sizes'] = $sizes; 
            $data['colors'] = $colors; 

            // dd($colors);
            
             // upload image of this module
            if($request->hasFile('img'))
            {
                // delete old image from server
                if($old->img)
                {
                    Storage::disk('public_uploads')->delete(PRODUCT_PATH.$old->img);
                }
                $img = UploadClass:: uploadImageR($request,'img',UPLOADS_PATH.PRODUCT_PATH,450,400,250,200,100,90);
                $data['img'] = $img;

            }


    	    // updating data in db
    	    Product::where('id', $request->id)->update($data);
            $message['success'] = trans('site.updated_success');
            return response()->json($message);

        }
    }



















    // deleteing row from db  ( soft delete )
    public function delete($id)
    {
    	SubCategory::findOrFail($id)->delete();
    	session()->flash('message',trans('site.deleted_success'));
    	return back();

    }

    // delete mutli row from table 
    public function deleteMulti(Request $request)
    {
    	foreach ($request->deleteMulti as  $value) 
    	{
    		SubCategory::findOrFail($value)->delete();
	    	
    	}

    	session()->flash('message',trans('site.deleted_success'));
	    return back();
    }


     // delete mutli row from table 
    public function sort(Request $request)
    {
    	$i = 1;
    	foreach ($request->sort as  $value) 
    	{
    		$data = SubCategory::find($value);
    		$data->sort = $i;
    		$data->save();
    		$i++;
    	}
    	session()->flash('message',trans('site.sorted_success'));
	    return back();
    }






    // visibility  for this item ( active or not active  -- change status of this item )
    public function visibility($id)
    {
    	$data = SubCategory::findOrFail($id);

    	switch ($data->status) 
    	{
    		case 'yes':
    			$data->status = 'no';
    			$data->save();
    			session()->flash('message',trans('site.deactivate_message'));
    			break;

    		case 'no':
    			$data->status = 'yes';
    			session()->flash('message',trans('site.activate_message'));
    			$data->save();
    			break;
    		
    		default:
    			# code...
    			break;
    	}

    	return back();

    }












      // return view of seo data 
    public function seo($id)
    {
        $data['seo'] = SubCategory::where('language_id',language())->findOrFail($id);
        $data['seoData'] = json_decode($data['seo']['seo']);
        return view('admin.products.seo')->with($data);

    }



     // updte seo data
    public function updateSeo(Request $request)
    {
        $data = $request->except('id','_token');
        // updating data in db
        $seoData = SubCategory::find($request->id);
        $seoData->seo = json_encode($data);
        $seoData->save();
        session()->flash('message',trans('site.updated_success'));
        return back();
    }


}
