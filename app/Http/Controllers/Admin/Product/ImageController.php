<?php

namespace App\Http\Controllers\Admin\Product;

use Illuminate\Http\Request;
use App\Http\Requests\CustomerRequest;
use App\Http\Controllers\Controller;
use App\Models\Product;


use Image;
use Storage;
use UploadClass;



class ImageController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:products-edit');
    }

   

   // return view of seo data 
    public function addImages($id)
    {
        $data ['row'] = Product::findOrFail($id);
        if($data['row']['images'])
        {
            $data['images'] = json_decode($data['row']['images']);
        }
        return view('admin.products.images.index')->with($data);

    }



     // storing data in db 
    public function moreImages(Request $request)
    {
        $this->validate($request, [

                'id'   => 'required|numeric|exists:products,id',
                'img'   => 'required',
                'img.*' => 'image|mimes:jpeg,png,jpg,gif,svg|max:50000'

        ]);


         $data = Product::findOrFail($request->id);
         if($data->images == null)
         {
            $all_images = array();
         }
         else
         {
            $all_images = json_decode($data->images);
         }
         

         if($request->hasfile('img'))
         {
            foreach($request->file('img') as $image)
            { 
                $path = UPLOADS_PATH.PRODUCT_PATH_IMAGES;
                $img = UploadClass::uploadMultiImage($request,$image,$path,450,400);
                array_push($all_images,$img);
  
            }
         }
         

        $images_json = json_encode($all_images);
        $data->images = $images_json;
        $data->save();
        session()->flash('message',trans('site.added_success'));
        return back();
    }




    public function deleteImage($id,$image_name)
    {
        $data = Product::findOrFail($id);

        $images = json_decode($data->images);
        $new_images = array();
        foreach($images as $img)
        {   
            if($img != $image_name)
            {
                array_push($new_images,$img);
            }
            else
            {
                Storage::disk('public_uploads')->delete(PRODUCT_PATH_IMAGES.$img);
            }
        }


        $data->images = json_encode($new_images);
        $data->save();
        session()->flash('message',trans('site.deleted_success'));
        return back();


    }








}
