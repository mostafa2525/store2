<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\AdminRequest;
use App\Http\Controllers\Controller;
use App\Models\Language;
use App\Models\AdminGroup;
use App\Models\Admin;
use App\Models\Product\Order;




class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:admins-show', ['only' => 'index']);
        $this->middleware('permission:admins-add', ['only' => 'add', 'store']);
        $this->middleware('permission:admins-edit', ['only' => 'edit', 'update']);
        $this->middleware('permission:admins-delete', ['only' => 'delete']);
    }


    public function index()
    {

        // $data['admins'] = Admin::where('id','!=',adminAuth()->user()->id)->get();
    	$data['admins'] = Admin::all();
    	return view('admin.admin.index')->with($data);
    }



    // return view of adding data 
    public function add()
    {
        $data['languages'] = Language::all();
        $data['groups'] = AdminGroup::all();
    	return view('admin.admin.add')->with($data);
    }





    // storing data in db 
    public function store(AdminRequest $request)
    {
    	$data = $request->except('confirm_password');
        $data['password'] = bcrypt($request->password);
    	Admin::create($data); // inserting data 
    	session()->flash('message',trans('site.added_success'));
    	return redirect(route('admin.get.admin.index'));
    }







    // return view of editing data 
    public function edit($id)
    {
    	$data['row'] = Admin::findOrFail($id);
        $data['languages'] = Language::all();
        $data['groups'] = AdminGroup::all();
    	return view('admin.admin.edit')->with($data);
    }



    // storing data in db 
    public function update(AdminRequest $request)
    {
        if($request->password != '')
        {
            $data = $request->except('confirm_password','_token','_method');
            $data['password'] = bcrypt($request->password);
        }
        else
        {
            $data = $request->except('confirm_password','_token','_method');
            $data['password'] = adminAuth()->user()->password;
        }
    	

    	// updating data in db
    	Admin::where('id', $request->id)->update($data);
    	session()->flash('message',trans('site.updated_success'));
    	return redirect(route('admin.get.admin.index'));
    }






    // statistics for admin ( about his orders)
    public function statistics($id)
    {
        $data['row'] = Admin::findOrFail($id);
        $data['totalOrders']        = Order::where('admin_id',$id)->count();


        $data['shippingOrders']     = Order::where('admin_id',$id)->where('status','shipping')->count();
        $data['acceptedOrders']     = Order::where('admin_id',$id)->where('status','accepted')->count();
        $data['refusedOrders']      = Order::where('admin_id',$id)->where('status','refused')->count();
        $data['canceledOrders']     = Order::where('admin_id',$id)->where('status','canceled')->count();
  

        $firstday = date('Y-m-d', strtotime("friday -1 week"));
        $today = date('Y-m-d',strtotime(now()));

        $data['weekOrders']         = Order::where('admin_id',$id)->where('created_at','>',$firstday)->count();
        $data['todayOrders']        = Order::where('admin_id',$id)->where('created_at','>',$today)->count();

        return view('admin.admin.statistics')->with($data);

    }






    public function statusOrder($id,$statusOrder)
    {
        $data['row'] = Admin::findOrFail($id);
        $data['orders']     = Order::where('admin_id',$id)->where('status',$statusOrder)->paginate(30);
        $data['status'] = $statusOrder;

        $data['totalOrders']        = Order::where('admin_id',$id)->count();


        $data['shippingOrders']     = Order::where('admin_id',$id)->where('status','shipping')->count();
        $data['acceptedOrders']     = Order::where('admin_id',$id)->where('status','accepted')->count();
        $data['refusedOrders']      = Order::where('admin_id',$id)->where('status','refused')->count();
        $data['canceledOrders']     = Order::where('admin_id',$id)->where('status','canceled')->count();
  

        $firstday = date('Y-m-d', strtotime("friday -1 week"));
        $today = date('Y-m-d',strtotime(now()));

        $data['weekOrders']         = Order::where('admin_id',$id)->where('created_at','>',$firstday)->count();
        $data['todayOrders']        = Order::where('admin_id',$id)->where('created_at','>',$today)->count();



        return view('admin.admin.statistics')->with($data);

    }















    // deleteing row from db  ( soft delete )
    public function delete($id)
    {
    	Admin::findOrFail($id)->delete();
    	session()->flash('message',trans('site.deleted_success'));
    	return redirect(route('admin.get.admin.index'));

    }

}
