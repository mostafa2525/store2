<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OfferRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name1'  => 'required|string|max:190',
            'link1'  => 'required|url',
            'name2'  => 'required|string|max:190',
            'link2'  => 'required|url',
            'name3'  => 'required|string|max:190',
            'link3'  => 'required|url',
            'name4'  => 'required|string|max:190',
            'link4'  => 'required|url',
            'name5'  => 'required|string|max:190',
            'link5'  => 'required|url',
            'name6'  => 'required|string|max:190',
            'link6'  => 'required|url',
        ];
    }
}
