<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminGroupRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) 
        {

            // adding data 
            case 'POST':

                    return [
                        'name'              => 'required|string|max:190',
                        'desc'              => 'required|string|max:1000',
                        'admins-show'       => 'sometimes|nullable|string|in:yes,no',
                        'admins-add'        => 'sometimes|nullable|string|in:yes,no',
                        'admins-edit'       => 'sometimes|nullable|string|in:yes,no',
                        'admins-delete'     => 'sometimes|nullable|string|in:yes,no',
                        'adminGroups-show'       => 'sometimes|nullable|string|in:yes,no',
                        'adminGroups-add'        => 'sometimes|nullable|string|in:yes,no',
                        'adminGroups-edit'       => 'sometimes|nullable|string|in:yes,no',
                        'adminGroups-delete'     => 'sometimes|nullable|string|in:yes,no',
                        'brands-show'       => 'sometimes|nullable|string|in:yes,no',
                        'brands-add'        => 'sometimes|nullable|string|in:yes,no',
                        'brands-edit'       => 'sometimes|nullable|string|in:yes,no',
                        'brands-delete'     => 'sometimes|nullable|string|in:yes,no',
                        'categories-show'       => 'sometimes|nullable|string|in:yes,no',
                        'categories-add'        => 'sometimes|nullable|string|in:yes,no',
                        'categories-edit'       => 'sometimes|nullable|string|in:yes,no',
                        'categories-delete'     => 'sometimes|nullable|string|in:yes,no',
                        'subCategories-show'       => 'sometimes|nullable|string|in:yes,no',
                        'subCategories-add'        => 'sometimes|nullable|string|in:yes,no',
                        'subCategories-edit'       => 'sometimes|nullable|string|in:yes,no',
                        'subCategories-delete'     => 'sometimes|nullable|string|in:yes,no',
                        'slider-show'       => 'sometimes|nullable|string|in:yes,no',
                        'slider-add'        => 'sometimes|nullable|string|in:yes,no',
                        'slider-edit'       => 'sometimes|nullable|string|in:yes,no',
                        'slider-delete'     => 'sometimes|nullable|string|in:yes,no',
                        'governates-show'       => 'sometimes|nullable|string|in:yes,no',
                        'governates-add'        => 'sometimes|nullable|string|in:yes,no',
                        'governates-edit'       => 'sometimes|nullable|string|in:yes,no',
                        'governates-delete'     => 'sometimes|nullable|string|in:yes,no',
                        'staticPages-show'       => 'sometimes|nullable|string|in:yes,no',
                        'staticPages-add'        => 'sometimes|nullable|string|in:yes,no',
                        'staticPages-edit'       => 'sometimes|nullable|string|in:yes,no',
                        'staticPages-delete'     => 'sometimes|nullable|string|in:yes,no',
                        'sizes-show'       => 'sometimes|nullable|string|in:yes,no',
                        'sizes-add'        => 'sometimes|nullable|string|in:yes,no',
                        'sizes-edit'       => 'sometimes|nullable|string|in:yes,no',
                        'sizes-delete'     => 'sometimes|nullable|string|in:yes,no',
                        'colors-show'       => 'sometimes|nullable|string|in:yes,no',
                        'colors-add'        => 'sometimes|nullable|string|in:yes,no',
                        'colors-edit'       => 'sometimes|nullable|string|in:yes,no',
                        'colors-delete'     => 'sometimes|nullable|string|in:yes,no',
                        'products-show'       => 'sometimes|nullable|string|in:yes,no',
                        'products-add'        => 'sometimes|nullable|string|in:yes,no',
                        'products-edit'       => 'sometimes|nullable|string|in:yes,no',
                        'products-delete'     => 'sometimes|nullable|string|in:yes,no',
                        'blog-show'       => 'sometimes|nullable|string|in:yes,no',
                        'blog-add'        => 'sometimes|nullable|string|in:yes,no',
                        'blog-edit'       => 'sometimes|nullable|string|in:yes,no',
                        'blog-delete'     => 'sometimes|nullable|string|in:yes,no',
                        'newsletter-show'       => 'sometimes|nullable|string|in:yes,no',
                        'newsletter-delete'     => 'sometimes|nullable|string|in:yes,no',
                        'messages-show'       => 'sometimes|nullable|string|in:yes,no',
                        'messages-delete'     => 'sometimes|nullable|string|in:yes,no',
                        'clients-show'       => 'sometimes|nullable|string|in:yes,no',
                        'clients-delete'     => 'sometimes|nullable|string|in:yes,no', 
                        'clients-edit'     => 'sometimes|nullable|string|in:yes,no', 
                        'baseData-edit'     => 'sometimes|nullable|string|in:yes,no', 
                        'seoData-edit'     => 'sometimes|nullable|string|in:yes,no', 
                        'siteContent-edit'     => 'sometimes|nullable|string|in:yes,no', 
                        'aboutUs-edit'     => 'sometimes|nullable|string|in:yes,no', 
                        'collections-edit'     => 'sometimes|nullable|string|in:yes,no', 

                        'orders-add-pending'        =>  'sometimes|nullable|string|in:yes,no',
                        'orders-show-pending'        =>  'sometimes|nullable|string|in:yes,no',
                        'orders-delete-pending'        =>  'sometimes|nullable|string|in:yes,no',
                        'orders-add-shipping'        =>  'sometimes|nullable|string|in:yes,no',
                        'orders-show-shipping'        =>  'sometimes|nullable|string|in:yes,no',
                        'orders-delete-shipping'        =>  'sometimes|nullable|string|in:yes,no',
                        'orders-add-canceled'        =>  'sometimes|nullable|string|in:yes,no',
                        'orders-show-canceled'        =>  'sometimes|nullable|string|in:yes,no',
                        'orders-delete-canceled'        =>  'sometimes|nullable|string|in:yes,no',
                        'orders-add-accepted'        =>  'sometimes|nullable|string|in:yes,no',
                        'orders-show-accepted'        =>  'sometimes|nullable|string|in:yes,no',
                        'orders-delete-accepted'        =>  'sometimes|nullable|string|in:yes,no',
                        'orders-add-refused'        =>  'sometimes|nullable|string|in:yes,no',
                        'orders-show-refused'        =>  'sometimes|nullable|string|in:yes,no',
                        'orders-delete-refused'        =>  'sometimes|nullable|string|in:yes,no',
                        'orderContents-accept-refuse'  => 'sometimes|nullable|string|in:yes,no',
                
                    ];


                break;


            //  editing data 
            case 'PUT':
                        
            return [
                'name'              => 'required|string|max:190',
                'desc'              => 'required|string|max:1000',
                'admins-show'       => 'sometimes|nullable|string|in:yes,no',
                'admins-add'        => 'sometimes|nullable|string|in:yes,no',
                'admins-edit'       => 'sometimes|nullable|string|in:yes,no',
                'admins-delete'     => 'sometimes|nullable|string|in:yes,no',
                'adminGroups-show'       => 'sometimes|nullable|string|in:yes,no',
                'adminGroups-add'        => 'sometimes|nullable|string|in:yes,no',
                'adminGroups-edit'       => 'sometimes|nullable|string|in:yes,no',
                'adminGroups-delete'     => 'sometimes|nullable|string|in:yes,no',
                'brands-show'       => 'sometimes|nullable|string|in:yes,no',
                'brands-add'        => 'sometimes|nullable|string|in:yes,no',
                'brands-edit'       => 'sometimes|nullable|string|in:yes,no',
                'brands-delete'     => 'sometimes|nullable|string|in:yes,no',
                'categories-show'       => 'sometimes|nullable|string|in:yes,no',
                'categories-add'        => 'sometimes|nullable|string|in:yes,no',
                'categories-edit'       => 'sometimes|nullable|string|in:yes,no',
                'categories-delete'     => 'sometimes|nullable|string|in:yes,no',
                'subCategories-show'       => 'sometimes|nullable|string|in:yes,no',
                'subCategories-add'        => 'sometimes|nullable|string|in:yes,no',
                'subCategories-edit'       => 'sometimes|nullable|string|in:yes,no',
                'subCategories-delete'     => 'sometimes|nullable|string|in:yes,no',
                'slider-show'       => 'sometimes|nullable|string|in:yes,no',
                'slider-add'        => 'sometimes|nullable|string|in:yes,no',
                'slider-edit'       => 'sometimes|nullable|string|in:yes,no',
                'slider-delete'     => 'sometimes|nullable|string|in:yes,no',
                'governates-show'       => 'sometimes|nullable|string|in:yes,no',
                'governates-add'        => 'sometimes|nullable|string|in:yes,no',
                'governates-edit'       => 'sometimes|nullable|string|in:yes,no',
                'governates-delete'     => 'sometimes|nullable|string|in:yes,no',
                'staticPages-show'       => 'sometimes|nullable|string|in:yes,no',
                'staticPages-add'        => 'sometimes|nullable|string|in:yes,no',
                'staticPages-edit'       => 'sometimes|nullable|string|in:yes,no',
                'staticPages-delete'     => 'sometimes|nullable|string|in:yes,no',
                'sizes-show'       => 'sometimes|nullable|string|in:yes,no',
                'sizes-add'        => 'sometimes|nullable|string|in:yes,no',
                'sizes-edit'       => 'sometimes|nullable|string|in:yes,no',
                'sizes-delete'     => 'sometimes|nullable|string|in:yes,no',
                'colors-show'       => 'sometimes|nullable|string|in:yes,no',
                'colors-add'        => 'sometimes|nullable|string|in:yes,no',
                'colors-edit'       => 'sometimes|nullable|string|in:yes,no',
                'colors-delete'     => 'sometimes|nullable|string|in:yes,no',
                'products-show'       => 'sometimes|nullable|string|in:yes,no',
                'products-add'        => 'sometimes|nullable|string|in:yes,no',
                'products-edit'       => 'sometimes|nullable|string|in:yes,no',
                'products-delete'     => 'sometimes|nullable|string|in:yes,no',
                'blog-show'       => 'sometimes|nullable|string|in:yes,no',
                'blog-add'        => 'sometimes|nullable|string|in:yes,no',
                'blog-edit'       => 'sometimes|nullable|string|in:yes,no',
                'blog-delete'     => 'sometimes|nullable|string|in:yes,no',
                'newsletter-show'       => 'sometimes|nullable|string|in:yes,no',
                'newsletter-delete'     => 'sometimes|nullable|string|in:yes,no',
                'messages-show'       => 'sometimes|nullable|string|in:yes,no',
                'messages-delete'     => 'sometimes|nullable|string|in:yes,no',
                'clients-show'       => 'sometimes|nullable|string|in:yes,no',
                'clients-delete'     => 'sometimes|nullable|string|in:yes,no',
                'clients-edit'     => 'sometimes|nullable|string|in:yes,no', 
                'baseData-edit'     => 'sometimes|nullable|string|in:yes,no', 
                'seoData-edit'     => 'sometimes|nullable|string|in:yes,no', 
                'siteContent-edit'     => 'sometimes|nullable|string|in:yes,no', 
                'aboutUs-edit'     => 'sometimes|nullable|string|in:yes,no', 
                'collections-edit'     => 'sometimes|nullable|string|in:yes,no', 

                'orders-add-pending'        =>  'sometimes|nullable|string|in:yes,no',
                'orders-show-pending'        =>  'sometimes|nullable|string|in:yes,no',
                'orders-delete-pending'        =>  'sometimes|nullable|string|in:yes,no',
                'orders-add-shipping'        =>  'sometimes|nullable|string|in:yes,no',
                'orders-show-shipping'        =>  'sometimes|nullable|string|in:yes,no',
                'orders-delete-shipping'        =>  'sometimes|nullable|string|in:yes,no',
                'orders-add-canceled'        =>  'sometimes|nullable|string|in:yes,no',
                'orders-show-canceled'        =>  'sometimes|nullable|string|in:yes,no',
                'orders-delete-canceled'        =>  'sometimes|nullable|string|in:yes,no',
                'orders-add-accepted'        =>  'sometimes|nullable|string|in:yes,no',
                'orders-show-accepted'        =>  'sometimes|nullable|string|in:yes,no',
                'orders-delete-accepted'        =>  'sometimes|nullable|string|in:yes,no',
                'orders-add-refused'        =>  'sometimes|nullable|string|in:yes,no',
                'orders-show-refused'        =>  'sometimes|nullable|string|in:yes,no',
                'orders-delete-refused'        =>  'sometimes|nullable|string|in:yes,no',
                'orderContents-accept-refuse'  => 'sometimes|nullable|string|in:yes,no',


            ];


                break;
        }
    }
}
