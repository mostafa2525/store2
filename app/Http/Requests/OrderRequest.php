<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) 
        {

            // adding data 
            case 'POST':

                    return [
                        'name'              => 'required|string|max:190',
                        'email'             => 'nullable|email|max:50',
                        'address'           => 'nullable|string|max:3000',
                        'mobile'            => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:8',
                        'notes'             => 'nullable|string|max:5000',
                        'product_id'        => 'required|array',
                        'product_id.*'      => 'required|numeric|exists:products,id',
                        'quantity'          => 'required|array',
                        'quantity.*'        => 'required|numeric|min:1',
                    ];


                break;


            //  editing data 
            case 'PUT':
                        
                        return [
                            'name'              => 'required|string|max:190',
                            'email'             => 'nullable|email|max:50',
                            'address'           => 'nullable|string|max:3000',
                            'mobile'            => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:8',
                            'notes'             => 'nullable|string|max:5000',
                            'product_id'        => 'required|array',
                            'product_id.*'      => 'required|numeric|exists:products,id',
                            'quantity'          => 'required|array',
                            'quantity.*'        => 'required|numeric|min:1',    
                        ];


                break;
        }
    }
}
