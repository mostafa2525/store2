<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) 
        {
            case 'POST':

                    return [
                        'name'              => 'required|string|max:190',
                        'category_id'       => 'required|numeric|exists:categories,id',
                        'sub_category_id'   => 'required|numeric|exists:sub_categories,id',
                        'price'             => 'required|numeric|digits_between:1,7',
                        'offer'             => 'nullable|numeric|digits_between:1,3',
                        'featured'          => 'required|string|in:no,yes',
                        'img'               => 'nullable|image|mimes:png,jpg,jpeg,gif',
                        'show_in_homePage'  => 'required|string|in:yes,no',
                        'qty'               => 'required|numeric|digits_between:1,7',
                        "color"             => "required|array",
                        "color.*"           => "required|string|distinct|exists:colors,id",
                        "size"              => "required|array",
                        "size.*"            => "required|string|distinct|exists:sizes,id",
                        'small_desc'        => 'required|string|max:30000',
                        'desc'              => 'nullable|string|max:300000',
                        'slug'              => 'nullable|string|max:150|unique:sub_categories,slug',
                        'tags'              => 'required|string|max:11150',
                        "img"               => "required|image|mimes:png,jpg,jpeg,gif|max:3000",
                        "image"             => "nullable|array",
                        "image.*"           => "nullable|image|mimes:png,jpg,jpeg,gif|max:3000",


                    ];


                break;
            
            case 'PUT':
                        
                        return [
                        'name'              => 'required|string|max:190',
                        'category_id'       => 'required|numeric|exists:categories,id',
                        'sub_category_id'   => 'required|numeric|exists:sub_categories,id',
                        'price'             => 'required|numeric|digits_between:1,7',
                        'offer'             => 'nullable|numeric|digits_between:1,3',
                        'featured'          => 'required|string|in:no,yes',
                        'img'               => 'nullable|image|mimes:png,jpg,jpeg,gif',
                        'show_in_homePage'  => 'required|string|in:yes,no',
                        'qty'               => 'required|numeric|digits_between:1,7',
                        "colors"             => "required|array",
                        "colors.*"           => "required|string|distinct|exists:colors,id",
                        "sizes"              => "required|array",
                        "sizes.*"            => "required|string|distinct|exists:sizes,id",
                        'small_desc'        => 'required|string|max:30000',
                        'desc'              => 'nullable|string|max:300000',
                        'slug'              => 'nullable|string|max:150|unique:sub_categories,slug,' . Request('id'),
                        'tags'              => 'required|string|max:11150',
                        "img"               => "nullable|image|mimes:png,jpg,jpeg,gif|max:3000",
                        "image"             => "nullable|array",
                        "image.*"           => "nullable|image|mimes:png,jpg,jpeg,gif|max:3000",
                        
                    ];


                break;
        }
        
    }
}
