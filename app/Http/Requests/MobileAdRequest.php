<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MobileAdRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'link1'  => 'nullable|url',
            'img1'   => 'nullable|image|mimes:png,jpg,jpeg,gif|max:10000',
            'link2'  => 'nullable|url',
            'img2'   => 'nullable|image|mimes:png,jpg,jpeg,gif|max:10000',
            'link3'  => 'nullable|url',
            'img3'   => 'nullable|image|mimes:png,jpg,jpeg,gif|max:10000',
            'link4'  => 'nullable|url',
            'img4'   => 'nullable|image|mimes:png,jpg,jpeg,gif|max:10000',
            'link5'  => 'nullable|url',
            'img5'   => 'nullable|image|mimes:png,jpg,jpeg,gif|max:10000',
            'link6'  => 'nullable|url',
            'img6'   => 'nullable|image|mimes:png,jpg,jpeg,gif|max:10000',
        ];
    }
}
