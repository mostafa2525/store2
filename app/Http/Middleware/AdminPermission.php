<?php

namespace App\Http\Middleware;

use Closure;

class AdminPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $group = auth()->guard('admin')->user()->group;
        $permissions = json_decode($group->permissions, true);
        if($permissions[$role] == 'no'){
            session()->flash('message', trans('site.noPermission'));
            return back();
        }
        return $next($request);
    }
}