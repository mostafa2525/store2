function getProductPanel(){

}

let tags = `
<div class="added-product">
  <div class="col-sm-6">
      <div class="form-group">
          <label> @lang('site.product') </label>
          <div class="input-group">
              <span class="input-group-addon input-circle-left">
              </span>
              <select id="select-product" class="form-control" name="product_id" required>
                  <option value="">@lang('site.choose')</option>
                  @foreach($products as $product)
                      <option value="{{ $product->id }}" @if(old('product_id')) {{ printSelect($product->id,old('product_id')) }} @endif >{{ $product->name }}</option>
                  @endforeach
              </select>
          </div>
      </div>
  </div>
  <div class="col-sm-6">
      <div class="form-group">
          <label> @lang('site.quantity') </label>
          <div class="input-group">
              <span class="input-group-addon input-circle-left">
                  <i class="fa fa-text-width"></i>
              </span>
              <input type="number" name="quantity" class="form-control input-circle-right " placeholder="Quantity" required value="{{ old('quantity') }}"> </div>
      </div>
  </div>
</div>
`;