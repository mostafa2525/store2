<?php 

return [

    // buttons 
    'viewMore'    			=> 'View More',
    'startService'    		=> 'Start a Service',
    'details'    			=> 'Details',
    'serviceDetails'    	=> 'Service Details',
    'projectDetails'    	=> 'Project Details',
    'usefulLinks'			=> 'Useful Links ',
    'workWithUs'			=> 'Work With Us', 
    'shopNow'				=> 'SHOP NOW',
    'addToCart'				=> 'Add To Cart',
    'sale'					=> 'Sale',
    'show'					=> 'Show',
    'showCart'				=> 'Show Your Cart',
    'checkout'				=> 'CHECKOUT',
    'shoppingCart'			=> 'SHOPPING CART',
    'choose'				=> 'Choose',
    'myAccount'				=> 'My Account',
    'logout'				=> 'Logout',
    'myCart'                => 'My Cart',
    'typeYourEmail'         => ' Type Your Email',
    'send'                  => ' Send',
    'forgotYourPassword'    => ' Forgot password?',
    'checkYourEmail'        => ' Check Your Email ',
    'emailNotFound'         => ' This Email Not Found',
    'resetPassword'         => ' Reset Your Password',
    'clickHere'             => ' Click Here',
    'newPassword'           => ' New Password',
    'dataNotCorrect'        => ' Data Not Correct',
    'myAccount'             => ' My Account',
    'myOrders'              => ' My Orders',
    'search'                => ' Search ',
    'login'                 => ' Login ',
    'register'              => ' Register ',
    'welcome'               => ' welcome ',
    'support'               => ' SUPPORT ',
    'about'                 => ' ABOUT ',
    'contactUs'             => ' CONTACT US ',
    'Copyright'             => ' Copyright ',
    'rightReserved'	        => ' All Rights Reserved. ',
    'aboutUs'               => ' About Us ',
    'contnueShopping'       => '  Continue Shopping    ',
    'contnueCheckout'       => '  Checkout    ',
    'successCart'           => ' Added To Cart',
    'password'              => '  Password  ',
    'confirmPassword'       => '  Confirm Password   ',
    'copyRight'             => '  All Rights Reserved For EraaSoft   ',
    'paymentDleviry'        => '  Paiement On Delivery   ',
    'city'                  => '  City   ',
    'orderDetails'          => '  Order Details   ',
    'quantity'              => '  Quantity   ',
    'status'                => '  Status   ',
    'orderSuccessMail'      => '  You Have Been Make Order With These Products ',
    
    





    // mobile 
    'help'                  => ' Support  ',
    'arabic'                => '  Arabic ',
    'english'               => '  English   ',

    




	'dashboard' 				=> ' لوحة التحكم  ',
	'menu' 	=> [
					'home'=>'  الرئيسية ',
					'all' => 'قائمة الطعام ',
					'branches' => ' فروعنا  ',
				],


	'language' => 'اللغة  ',
	'howToGetUs' => 'كيف تتواصل معنا  ',
	'office' => ' الادارة العامة    ',
	'price' => '  Price       ',
	'readMore' => '  اقرأ المزيد      ',
 


	//  messages 
	
	'added_success'			=> ' تم الاضافة بنجاح ',
	'updated_success'		=> ' تم التعديل بنجاح ',
	'deleted_success'		=> ' تم الحذف بنجاح ',
	'sorted_success'		=> ' تم ترتيب البيانات ',
	'dataNotFound'		    => ' Sorry , There Is No Data !',
	'activate_message' 		=> ' تم التفعيل بنجاح ',
	'deactivate_message' 	=> ' تم الغاء التفعيل بنجاح ',
	'orderSuccess' 			=> ' تم ارسال طلبك بنجاح وسيتم التوصل معك  ! ',
	'contactusMessage' 		=> ' Your Message Have Been Sent  ',
	'notFoundData'			=> ' Sorry , There Is No Any Data',
    'subscribeMessage'		=> '  Thanks For Your Subscribe ',
    'successOrderMessage'	=> '  Thanks , We Will Contact With You',
    'itemAddedToCart'		=> '  Added To Your Cart',
    'itemCartRemoved'		=> '  Item Has Been Removed ! ',
    'orderSuccess'			=> '  Your Order Have Been Sent ! ',
    'successRegister'		=> '  Success Operation , You Can Login Now !  ',
    'successEdit'			=> '  Success Operation , Your Data Have Been Updated  !  ',
    'passwordNotCorrect'    => '    Password Not Correct',
    'updatedSuccess'    	=> '   Updated Success',
    







	



];

