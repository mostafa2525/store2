<?php 

return [




	'viewMore'    			=> '  مشاهدة المزيد ',
    'details'    			=> ' التفاصيل  ',


    'shopNow'				=> ' تسوق  الان ',
    'addToCart'				=> ' اضافة الى السلة ',
    'sale'					=> '  Sale  ',
    'show'					=> ' عرض  ',
    'showCart'				=> ' مشاهدة السلة ',
    'checkout'				=> '  اتمام   الشراء  ',
    'shoppingCart'			=> ' سلة المشتريات ',
    'choose'				=> ' اختر ',
    'myAccount'				=>  '   حسابى  ',
    'logout'				=> ' خروج  ',
    'myCart'                => '  سلة المشتريات  ',
    'typeYourEmail'         => '  اكتب البريد الالكترونى  ',
    'send'                  => '  ارسال  ',
    'forgotYourPassword'    => '  نسيت كلمة المرور   ?',
    'checkYourEmail'        => ' تم  ارسال رسالة الى بريدك الالكترونى  ',
    'emailNotFound'         => ' هذا البريد الالكترونى غير متاح  ',
    'resetPassword'         => ' انشاء كلمة مرور جديدة ',
    'clickHere'             => ' اضغط هنا  ',
    'newPassword'           => ' كلمة مرور جديدة  ',
    'dataNotCorrect'        => ' البيانات غير صحيحة  ',
    'myAccount'             => '  حسابى   ',
    'myOrders'              => ' طلباتى  ',
    'search'                => ' بحث   ',
    'login'                 => ' تسجيل دخول   ',
    'register'              => ' تسجيل عضوية جديدة  ',
    'welcome'	            => '  اهلا   ',
    'support'               => '  الدعم  الفنى  ',
    'about'                 => ' عن  الشركة  ',
    'contactUs'             => '  اتصل  بنا  ',
    'Copyright'             => ' Copyright ',
    'rightReserved'	        => ' جميع  الحقوق محفوظة . ',
    'aboutUs'               => ' من  نحن  ',
    'contnueShopping'       => ' متابعة التسوق   ',
    'contnueCheckout'       => '  تأكيد الشراء    ',
    'successCart'           => '  تم اضافة المنتج الى السلة بنجاح  ',
    'password'              => '  كلمة المرور  ',
    'confirmPassword'       => '  تأكيد كلمة المرور ',
    'copyRight'             => '  جميع الحقوق محفوظة  لشركة ايراسوفت  ',
    'paymentDleviry'        => '   الدفع عند الاستلام    ',
    'city'                  => '  المدينة   ',
    'orderDetails'          => '   تفاصيل الطلب   ',
    'quantity'              => '  الكمية   ',
    'status'                => '  الحالة   ',
    'orderSuccessMail'      => '  لقد تم طلب هذه المنتجات  من  المتجر  ',










    // mobile 
    'help' 					=> 'المساعدة ',
    'arabic' 				=> ' العربية  ',
    'english' 				=> 'الانجليزية  ',









	'dashboard' 				=> ' لوحة التحكم  ',
	'menu' 	=> [
					'home'=>'  الرئيسية ',
					'all' => 'قائمة الطعام ',
					'branches' => ' فروعنا  ',
				],


	'language' 				=> 'اللغة  ',
	'howToGetUs' 			=> 'كيف تتواصل معنا  ',
	'office' 				=> ' الادارة العامة    ',
	'search' 				=> '  بحث     ',
	'price' 				=> '  السعر      ',
	'readMore' 				=> '  اقرأ المزيد      ',
	'myAccount'				=> 'حسابى ',
    'logout'				=> 'خروج',
    'myCart'				=> ' سلة المشريات ',
    'sale'					=> ' Sale ',

 


	//  messages 
	
	'added_success'			=> ' تم الاضافة بنجاح ',
	'updated_success'		=> ' تم التعديل بنجاح ',
	'deleted_success'		=> ' تم الحذف بنجاح ',
	'sorted_success'		=> ' تم ترتيب البيانات ',
	'dataNotFound'		    => ' للاسف   , لا توجد بيانات  !',
	'activate_message' 		=> ' تم التفعيل بنجاح ',
	'deactivate_message' 	=> ' تم الغاء التفعيل بنجاح ',
	'orderSuccess' 			=> ' تم ارسال طلبك بنجاح وسيتم التوصل معك  ! ',
	'subscribeMessage' 		=> ' شكرا لاشتراكك فى قائمتنا البريدية  ',
	'message_success' 		=> ' تم  ارسال رسالتك بنجاح  ',
	'notFoundData'			=> ' للاسف  لا توجد بيانات  ',


	'contactusMessage' 		=> '  تم ارسال  رسالتك لنجاح وسيتم التواصل معك  فى اقرب وقت   ',
	'notFoundData'			=> '   لا توجد بيانات  ',
    'subscribeMessage'		=> '  شكرا لاشتراكك فى قائمتنا البريدية  ',
    'successOrderMessage'	=> '   تم  تفعيل طلبك بنجاح وسيتم التواصل معك  . ',
    'itemAddedToCart'		=> '   اضافة الى السلة ',
    'itemCartRemoved'		=> '   تم ازالة الممنتج   بنجاح  ! ',
    'orderSuccess'			=> '  تم  ارسال طلبك بنجاح  ! ',
    'successRegister'		=> '  عملية ناجحة   , يمكنك تسجيل الدخول الان  !  ',
    'successEdit'			=> '  عملية ناجحة  , تم تعديل البيانات بنجاح  !  ',
    'passwordNotCorrect'    => '    كلمة  المرور غير صحيحة ',
    'updatedSuccess'    	=> '   تم التعديل بنجاح  ',



	



];

