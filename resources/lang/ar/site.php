<?php 

return [
	'dashboard' 				=> ' لوحة التحكم  ',
	'content' 					=> ' المحتوى  ',
	'categories'				=> '   الأقسام الرئيسية   ',
	'category'  				=> '   القسم الرئيسى   ',
	'add'						=> ' اضافة ',
	'view'						=> ' عرض الكل ',
	'home'						=> ' الرئيسية ',
	'name'						=> ' الاسم ',
	'aboutUsPage'				=> '  صفحة من نحن  ',
	'addItem'					=> ' اضافة جديد ',
	'actions'   				=> ' أكشنز',
	'edit'   					=> ' تعديل ',
	'delete'    				=> ' حذف ',
	'de-active' 				=> ' الغاء التفعيل ',
	'active'    				=> ' تفعيل ',
	'sort' 						=> ' ترتيب ',
	'show_sub'					=> ' عرض   المشاريع والتصاميم ',
	'deleteChecked'				=> ' حذف العناصر المختارة ',
	'editItem'					=> ' تعديل  البيانات ',
	'save'						=> ' حفظ ',
	'choose'         			=> ' اختر  ... ',
	'sub_categories'			=> '   الأقسام الفرعية ',
	'childs'					=> ' عرض التفرع ',
	'image'					    => ' الصورة ',
	'image1'					=> ' الصورة  الأولى',
	'image2'					=> ' الصورة  الثانية',
	'image3'					=> ' الصورة  الثالثة',
	'image4'					=> ' الصورة  الرابعة',
	'description'			    => ' الوصف ',
	'content'			    	=> 'المحتوى',
	'seo'			    		=> ' بيانات السيو ',
	'language'			    	=> 'اللغة ',
	'symbole'			    	=> 'سيمبول ',
	'icon'			    	    => ' ايكون ',
	'countries'			    	=> ' الدول ',
	'staticPage'			    => ' الصفحات الثابتة ',
	'static'			    	=> ' ثابت ة  ',
	'faq'			    		=> 'FAQ ',
	'type'			    		=> ' النوع  ',
	'addQuestion'			    => ' اضافة  سؤال  ',
	'question'			    	=> '  السؤال  ',
	'questiones'			    => ' الأسئلة  ',
	'editQuestion'			    => ' تعديل الأسئلة  ',
	'answer'			    	=> '  الاجابة  ',
	'newsletter'			    => ' القائمة البريدية  ',
	'email'			    		=> ' البريد الالكترونى  ',
	'department'			    => ' القسم  ',
	'settings'			        => '  الاعدادات  ',
	'base'			        	=> '   البيانات الرئيسية ',
	'baseSetting'			    => '  الاعدادات الرئيسية  ',
	'site_name'			    	=> ' اسم الموقع  ',
	'facebook'			    	=> '  فيسبوك  ',
	'twitter'			    	=> ' تويتر  ',
	'instagram'			    	=> ' انستجرام  ',
	'youtube'			    	=> ' اليوتيوب  ',
	'linkedin'			    	=> ' لينكد ان  ',
	'pinterest'			    	=> '  بنترست  ',
	'fax'			    		=> ' فاكس  ',
	'messages'			    	=> ' الرسائل  ',
	'phone'			    		=> ' الهاتف  ',
	'mobile'			    	=> ' الموبايل  ',
	'logo'			    		=> '  اللوجو  ',
	'all'			    		=> '  الكل  ',
	'logout'			    	=> '  خروج  ',
	'admins'			    	=> ' المديرين  ',
	'password'			    	=> '  كلمة المرور   ',
	'confirm_password'			=> ' تأكيد كلمة المرور   ',
	'contacts'					=> '  وسائل التواصل    ',
	'social'					=> '  السوشيال ميديا  ',
	'slug'						=> ' الرابط   ( url ) ',
	'price' 					=> ' السعر ',
	'no' 						=> '  لا  ',
	'yes' 						=> '  نعم  ',
	'showInHomePage' 			=> ' عرض فى الصفحة الرئيسية ',
	'services' 					=> ' الخدمات',
	'smallDescription' 			=> ' وصف مختصر ',
	'showInFront' 				=> ' مشاهدة ',
	'slider' 					=> ' عارض الصور ',
	'blog' 						=> ' المدونة ',
	'branch' 					=> ' الفروع ',
	'code'						=> '  كود ',
	'showInFooter'				=> ' عرض فى الفووتر ',
	'address'					=> ' العنوان  ',
	'map'						=> '  الخريطة ',
	'embedMap'					=> ' تضمين الخريطة ',
	'brand'						=> ' شركائنا ',
	'product'					=> ' المنتج ',
	'products'					=> ' المنتجات ',
	'orderCode'					=> ' تمييز كود الطلب ',
	'productCode'				=> ' تمييز كود المنتج ',
	



	'addMoreImages'				=> '  اضافة صور اخرى  ',
	'team'						=> ' فريق العمل  ',
	'position'					=> ' الدرجة الوظيفية  ',
	'image2'					=> ' الصورة الثانية  ',
	'testimonials'				=> '  اراء العملاء ',
	'link'						=> ' رابط ',
	'comments'					=> ' التعليقات ',
	'customers'					=> ' العملاء ',
	'customer'					=> ' العميل ',
	'seoService'				=> ' SEO ',
	'webService'				=> ' WEB ',
	'service'					=> ' الخدمة ',
	'more'						=> ' اضافة ',
	'addImages'					=> ' اضافة صور اخرى s ',
	'addDetails'				=> ' اضافة تق=فاصيل اخرى ',
	'images'					=> ' الصور ',
	'seoDetails'				=> '  تفاصيل السيو ',
	'order'						=> ' الطلب ',
	'pageNumber'				=> ' رقم الصفحة   ',
	'date'						=> ' التاريخ ',
	'word'						=> ' الكلمة ',
	'addMoreWords'				=> ' اضافة كلمات اخرى  ',







	// content 
	'homePage'       			=> '  الصفحة الرئيسية ',
	'moreImages'				=> '  صور اخرى   ',
	'message'					=> ' الرسالة ',
	'blogPageData'				=> ' بيانات صفحة المدونة ',
	'footer'					=> ' بيانات الفووتر ',
	'contactUsPage'				=> ' بيانات صفحة تواصل معنا  ',
	'pinterest'					=> ' بينترست  ',
	'desc'						=> '  الوصف ', 
	'totalOrders'				=> '  مجموع الطلبات ', 
	'pendingOrders'				=> '  الطلبات الحديثة  ', 
	'shippingOrders'			=> '  طلبات فى مرحة الشحن ', 
	'acceptedOrders'			=> '  الطلبات المقبولة ', 
	'refusedOrders'				=> '  الطلبات المرفوضة ', 
	'canceledOrders'			=> '  طلبات ملغية ', 
	'todayOrders'				=> '   طلبات اليوم ', 
	'weekOrders'				=> '   طلبات الاسبوع ', 
	'addShipping'				=> ' اضافة الى الشحن',
	'orderNum'					=> ' رقم الطلب',
	'show'						=> ' مشاهدة',
	'quantity'					=> ' الكلمية',
	'status'					=> 'الحالة',
	'accept'					=> 'قبول ',
	'refuse'					=> '  رفض ',
	'cancelled'					=> 'الغاء',
	'orderId'					=> 'رقم الاوردر',
	'submit'					=> 'حفظ',
	'cancel'					=> 'الغاء',
	'topBarData'				=> 'الجزء العلوى',
	'categoriesPage'			=> 'صفحة القسام  الرئيسية',
	'subCategoriesPage'			=> 'صفحة الاقسام الفرعية',
	'productPage'			    => 'صفحة المنتج',
	'aboutPage'			   		=> 'صفحة من نحن',
	'contactPage'			    => 'صفحة تواصل معنا',
	'profilePage'			    => 'صفحة الملف الشخصى للعميل',
	'blogPage'			    	=> 'صفحة المدونة',
	'loginPage'			    	=> 'صفحة الدخول',
	'registerPage'			    => 'صفحة  تسجيل   عضوية ',
	'cartPage'			    	=> 'صفحة سلة المشريات ',
	'checkoutPage'			    => 'صفحة الدفع ',
	'priceCharge'			    => ' سعر الشحن',
	'images'			   		=> 'الصور',


	'addAnotherProduct'			=> ' اضافة منتج اخر ',
	'totalClients'				=> ' االعملاء الحاليين ',
	'totalAdmins'				=> '  المشرفين ',
	'addNewOrder'				=> '  اضافة طلب جديد	 ',
	'searchAboutOrder'			=> '  بحث بالكود عن طلب   	 ',
	'totalVisitors'				=> '   عدد زيارات الموقع  	 ',
	'siteContent'				=> ' محتوى الموقع ',
	'tags'						=> '   الكلمات الدلالية    	 ',
	'changeStatus'				=> '   تغيير الحالة    	 ',
	'from'						=> '   من     	 ',
	'to'						=> '   الى     	 ',
	'adminOrders'				=> '  احصائيات الطلبات للمشرف  ',
	'siteContentMobile'			=> '  محتوى صفحات الموبايل  ',
	




	












	// newely added
	'title'							=> 'العنوان',
	'color'							=> 'اللون',
	'size'							=> 'المقاس او الحجم ',
	'desc'							=> 'الوصف',
	'gov'								=> 'المحافظة',
	'govs'							=> 'المحافظات',
	'addShipping'				=> 'اضافة الى الشحن',
	'orderNum'					=> 'رقم الاوردر',
	'show'							=> 'عرض',
	'quantity'					=> 'الكمية',
	'status'						=> 'الحالة',
	'accept'						=> 'موافقة ',
	'refuse'						=> 'رفض',
	'cancelled'					=> 'الغاء',
	'orderId'						=> 'رقم الاوردر',
	'submit'						=> 'حفظ',
	'cancel'						=> 'الغاء',
	'client'						=> 'العميل',
	'notes'							=> 'ملاحظات',
	'addedBy'						=> 'تمت الاضافة بواسطة',
	'nextStep'					=> 'الخطوة التالية ',
	'trust'							=>  'موثوق',
	'moreInfo'					=> 'معلومات إضافية',
	'clientNum'					=> ' عميل رقم',
	'showContent'				=> 'عرض المحتوى',
	'editProfilePage'				=> ' تعديل البانات الشخصية ',
	'changePassword'				=> '  تغيير  كلمة المرور   ',
	'ordersPage'				=> '   الطلبات      ',
	'helpPage'					=> '    صفحة المساعدة       ',
	'help'						=> '    المساعدة        ',
	'offer1'						=> 'عرض 1',
	'link1'							=> 'رابط 1',
	'offer2'						=> 'عرض 2',
	'link2'							=> 'رابط 2',
	'offer3'						=> 'عرض 3',
	'link3'							=> 'رابط 3',
	'offer4'						=> 'عرض 4',
	'link4'							=> 'رابط 4',
	'offer5'						=> 'عرض 5',
	'link5'							=> 'رابط 5',
	'offer6'						=> 'عرض 6',
	'link6'							=> 'رابط 6',
	'mobileAd'					=> 'إعلان الموبايل',
	'mobileAds'				=> 'إعلانات الموبايل',
	'img1'							=> 'صورة 1',
	'img2'							=> 'صورة 2',
	'img3'							=> 'صورة 3',
	'img4'							=> 'صورة 4',
	'img5'							=> 'صورة 5',
	'img6'							=> 'صورة 6',
	'reply'							=> 'الرد',
	'body'							=> 'المحتوى',
	'send'							=> 'إرسال',




	// sidebar

	'aboutUsPage'				=> ' صفحة من نحن   ',
	'messages'					=> ' الرسائل  ',
	'sub_categories'			=> ' الاقسام الفرعية  ',
	'products'					=> ' المنتجات  ',
	'sizes'						=> ' المقاسات   او الاحجام  ',
	'colors'					=> ' الالوان  ',
	'addProduct'				=> ' اضافة منتج جديد  ',
	'viewProduct'				=> ' عرض المنتجات  ',
	'orders'					=> ' الطلبات  ',
	'pending'					=> ' طلبات   حديثة   ',
	'shipping'					=> ' طلبات  الشحن  ',
	'accepted'					=> ' طلبات  المقبولة   ',
	'refused'					=> ' طلبات  مرفوضة   ',
	'brand'						=> ' الماركات ',
	'collections'				=> ' العروض ',
	'collection1'				=> ' Collection 1',
	'collection2'				=> ' Collection 2',
	'collection3'				=> ' Collection 3',
	'collection4'				=> ' Collection 4',
	'collection5'				=> ' Collection 5',
	'collection6'				=> ' Collection 6',
	'title'						=> '  العنوان ', 
	'size'						=> '  المقاس او الحجم ', 
	'color'						=> '  اللون ', 
	'govs'						=> '  المحافظة ', 
	'canceled'					=> '  الغيت ', 
	'hotdeal'					=> '  عرض مميز ', 
	'clients'					=> '  العملاء ', 
	'block'						=> '  قائمة سودائ ', 
	'subCategory'				=> '  القسم الفرعى ', 
	'offer'						=> '  العرض ', 
	'featured'					=> '  مميز ', 
	'number'					=> '  العدد ', 
	'qty'						=> '  الكمية ', 
	'filter'					=> ' تصفية ', 
	'offer'						=> 'العرض',
	'offers'					=> 'العروض',
	'baseImage'					=> ' الصورة الرئيسية ',
	'images'					=> ' الصور ',





	// site content  

	'section1' 			=> ' القسم الاول ',
	'section2' 			=> ' القسم الثانى ',
	'section3' 			=> ' القسم الثالث ',
	'section4' 			=> ' القسم الرابع  ',
	'section5' 			=> ' القسم الخامس  ',
	'section6' 			=> ' القسم السادس ',
	'section7' 			=> ' القسم السابع ',
	'section8' 			=> ' القسم  الثامن ',
	'links'    			=> '  الروابط ',
	'linksData'  		=> '  بيانات الروابط',


	

	'errors' 	=> [
					'dataNotCorrect'=>' البيانات غير صحيحة ',
				],


	'seoData' => [
				'metaKeywords' 		=> 'Meta Keywords -> Like ('.htmlspecialchars("<meta name='keywords' content=\"  \" /> ").')',
				'metaTitle' 		=> 'Meta Title  -> Like ('.htmlspecialchars("<title></title> ").')',
				'metaAuthor' 		=> 'Meta Author',
				'metaDescription' 	=> 'Meta Description -> Like ( '.htmlspecialchars("<meta name='description' content=\"  \"  >").')',
				'googleAnalytics' 	=> 'Google Analytics',
				'webMaster' 		=> 'Web Master',
				'tagMangerHead' 	=> 'Tag Manger Head',
				'tagMangerBody' 	=> 'Tag Manger Body',
				'otherMetaTags' 	=> 'Other Meta Tags',
				'structureData' 	=> 'Structure Data',
				'canonical' 		=> 'Canonical',
				'altImage' 			=> ' النص البديل   (alt) ',
				'titleImage' 		=> ' عنوان الصورة ',
			],


	//  messages 
	
	'added_success'			=> ' تم الاضافة بنجاح ',
	'updated_success'		=> ' تم التعديل بنجاح ',
	'deleted_success'		=> ' تم الحذف بنجاح ',
	'sorted_success'		=> ' تم ترتيب البيانات ',
	'sent_success'		=> 'تم الإرسال بنجاح',
	'dataNotFound'		    => ' للاسف   , لا توجد بيانات  !',
	'activate_message' 		=> ' تم التفعيل بنجاح ',
	'deactivate_message' 	=> ' تم الغاء التفعيل بنجاح ',
	'blocked_success'		=> ' تم اضافة هذا العميل الى القائمة السوداء  !',
	'orderSuccess'			=> ' تم اضافة الطلب بنجاح  الى المضاف حديثا !',
	'noPermission'			=> 'ليس لديك صلاحية لدخول هذه الصفحة ',



	



];

