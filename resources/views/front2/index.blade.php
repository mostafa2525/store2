 
@extends('front2.main')


@section('content')


		<!-- SLIDER AREA START -->
			<!-- SLIDER AREA START -->
		<section class="slider-area-main">
			<!-- slider -->
			<div class="slider-area">
				<div class="bend niceties preview-1">



					
					<div id="ensign-nivoslider-3" class="slides"  >	
						@foreach($slider as $sl)
                        <a id="ensign-nivoslider-3" class="slides" href="{{$sl->link}}" style="    display: block; width: 100%; height: 100%; position: absolute; z-index: 13;" target="_blank">
						<img src="{{getImage(SLIDER_PATH.$sl->img)}}" alt="{{$sl->name}}" title="#slider-direction-{{$sl->id}}"  /></a>
						@endforeach
					</div>
					<!-- direction 1 -->
					@foreach($slider as $sl)
					<!--<  class="pointer" style="    display: block; width: 100%; height: 100%; position: absolute; z-index: 8;" target="_blank" >-->
					<div id="slider-direction-{{$sl->id}}" class="t-cn slider-direction" >
						<div class="slider-content t-lfl s-tb slider-1"">
							<div class="title-container s-tb-c">
								{{--
								<h1 class="title1"> {{ \Str::Words($sl->name,4,'') }} </h1>
								<p class="title1">{{ \Str::Words($sl->description,30,'') }}</p>--}}
								{{--
								@if($sl->link)
								<h3 class="title3 btn-slider"><a href="{{$sl->link}}">{{json_data($site_content,'section1_btn')}}</a></h3>
								@endif  --}}
							</div>
						</div>
					</div>
					
					@endforeach






				</div>
			</div>
			<!-- slider end-->
		</section>
		<!-- SLIDER AREA END -->





			<!-- BANNER AREA START -->
		<?php  $collections = json_decode($setting->collections);  ?>
		<section class="banner-area">
			<div class="container">

				<div class="text-center">
					<div class="section-titel">
						<h3> {{ json_data($site_content,'section2_head') }}</h3>
					</div>
				</div>


				<div class="row">

					


					<div class="col-md-3 col-sm-3 xs-res-mrbtm">
						<div class="banner-left">
							<a class="promo-link" href="{{ json_data($collections,'collection1_link') }}">
								<img src="{{getImage(COLLECTION_PATH.json_data($collections,'collectionImg1'))}}" alt="" />
								<h1>{{ json_data($collections,'collection1_title') }}</h1>
								<span class="promo-hover"></span>
							</a>
						</div>
					</div>
					
			

					
					<div class="col-md-3 col-sm-3">
						<div class="banner-left-side">
							<a class="mr-btm promo-link" href="{{ json_data($collections,'collection2_link') }}">
								<img src="{{getImage(COLLECTION_PATH.json_data($collections,'collectionImg2'))}}" alt="" />
								<h1>{{ json_data($collections,'collection2_title')  }} </h1>
								<h1></h1>
								<span class="sl-btn">SALE</span>
								<div class="promo-hover"></div>
							</a>
							<a class="promo-link xs-res-mrbtm" href="{{ json_data($collections,'collection3_link') }}">
								<img src="{{getImage(COLLECTION_PATH.json_data($collections,'collectionImg3'))}}" alt="" />
								<h1>{{ json_data($collections,'collection3_title')  }}</h1>
								<h1></h1>
								<span class="sl-btn">SALE</span>
								<div class="promo-hover"></div>
							</a>
						</div>
					</div>

					<div class="col-md-3 col-sm-3 xs-res-mrbtm">
						<div class="banner-right">
							<a class="promo-link" href="{{ json_data($collections,'collection4_link') }}">
								<img src="{{getImage(COLLECTION_PATH.json_data($collections,'collectionImg4'))}}" alt="" />
								<h1>{{ json_data($collections,'collection4_title') }}</h1>
								<h1></h1>
								<span class="promo-hover"></span>
							</a>
						</div>
					</div>


				
					<div class="col-md-3 col-sm-3">
						<div class="banner-right-side">
							<a class="mr-btm promo-link" href="{{ json_data($collections,'collection5_link') }}">
								<img src="{{getImage(COLLECTION_PATH.json_data($collections,'collectionImg5'))}}" alt="" />
								<h1>{{ json_data($collections,'collection5_title')  }}</h1>
								<h1></h1>
								<span class="sl-btn">SALE</span>
								<div class="promo-hover"></div>
							</a>
							<a class="promo-link" href="{{ json_data($collections,'collection6_link') }}">
								<img src="{{getImage(COLLECTION_PATH.json_data($collections,'collectionImg6'))}}" alt="" />
								<h1>{{ json_data($collections,'collection6_title')  }}</h1>
								<h1></h1>
								<span class="sl-btn">SALE</span>
								<div class="promo-hover"></div>
							</a>
						</div>
					</div>


				</div>
			</div>
		</section>
		<!-- BANNER AREA END -->


















		<!-- FEATURED PRODUCT START -->
		<section class="featured-area">
			<div class="container">
				<div class="row">
					<div class="text-center">
						<div class="section-titel">
							<h3>{{ json_data($site_content,'section3_head') }}</h3>
						</div>
					</div>
					<div class="featured-tab">
						<ul class="text-center">
						
						@foreach($categories as $cat)
						  <li class="@if($loop->iteration == 1){{'active'}} @endif">
						  <a data-toggle="tab" href="#{{$cat->slug}}">{{$cat->name}}</a>
						  </li>
						@endforeach

						 
						</ul>
					</div>
					<div class="tab-content">

					
						@foreach($categories as $cat)
						<div id="{{$cat->slug}}" class="tab-pane @if($loop->iteration==1){{'active'}} @endif">
							<div id="" class="indicator-style features-curosel">
					
								@foreach($cat->fProduct() as $fpro)
								<div class="col-md-12">
									<div class="single-product">
										<div class="product-image">
											<a class="product-img prod-home-img" href="{{route('front.get.product.show',[$cat->slug,$fpro->sub->slug,$fpro->slug])}}">
												<img class="primary-img" src="{{getImage(PRODUCT_PATH.$fpro->img)}}" alt="{{$fpro->name}}"   />
												<img class="secondary-img" src="{{getImage(PRODUCT_PATH.$fpro->img)}}" alt="{{$fpro->name}}"   />
											</a>
										</div>
										@if($fpro->offer)
										<span class="onsale red">
											<span class="sale-text"> {{$fpro->offer}} %</span>
										</span>
										@else
										<span class="onsale">
											<span class="sale-text">@lang('frontSite.sale')</span>
										</span>
										@endif
										
										<div class="product-action">
											
											<h4><a href="{{route('front.get.product.show',[$cat->slug,$fpro->sub->slug,$fpro->slug])}}">{{\Str::Words($fpro->name,3,'..')}}</a></h4>
											<ul class="pro-rating">
												<li class="pro-ratcolor"><i class="fa fa-star"></i></li>
												<li class="pro-ratcolor"><i class="fa fa-star"></i></li>
												<li class="pro-ratcolor"><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
											</ul>
											<span class="price">EGP {{$fpro->price}}</span>
										</div>
										<div class="pro-action">
											<ul>

												<!-- <li>
													<a class="test all_src_icon" href="#" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
													<i class="fa fa-heart" aria-hidden="true"></i>
													</a>
												</li> -->
												
												<li>
													<a class="test all_src_icon"  href="{{route('front.get.product.show',[$cat->slug,$fpro->sub->slug,$fpro->slug])}}" title="" 
													data-toggle="tooltip" data-placement="top" data-original-title="@lang('frontSite.show')">
													<i class="fa fa-eye" aria-hidden="true"></i>
													</a>
												</li>

												<li>
													<a class="test all_src_icon  pointer"  href="{{route('front.get.product.show',[$cat->slug,$fpro->sub->slug,$fpro->slug])}}"
													  title=""  
													data-toggle="tooltip" data-placement="top" data-original-title="@lang('frontSite.addToCart')">
													<i class="fa fa-shopping-cart" aria-hidden="true"></i>
													</a>
												</li>

												
											</ul>
										</div>
									</div>
								</div>
								@endforeach

							</div>
						</div>
						@endforeach

		


					</div>
				</div>
			</div>
		</section>
		<!-- FEATURED PRODUCT END -->


		<!-- NEW ARRIVALS START -->
		<section class="featured-area new-arrival">
			<div class="container">
				<div class="row">
					<div class="text-center">
						<div class="section-titel">
							<h3>{{ json_data($site_content,'section4_head') }}</h3>
						</div>
					</div>
					<div class="newarrival-area">
						<div id="newarrival-curosel" class="indicator-style">

							@foreach($latest as $fpro)
								<div class="col-md-12">
									<div class="single-product">
										<div class="product-image">
											<a class="product-img prod-home-img" href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}">
												<img class="primary-img" src="{{getImage(PRODUCT_PATH.'mediummedium/'.$fpro->img)}}" alt="{{$fpro->name}}" height="250" />
												<img class="secondary-img" src="{{getImage(PRODUCT_PATH.'mediummedium/'.$fpro->img)}}" alt="{{$fpro->name}}" height="250" />
											</a>
										</div>
										@if($fpro->offer)
										<span class="onsale red">
											<span class="sale-text"> {{$fpro->offer}} %</span>
										</span>
										@else
										<span class="onsale">
											<span class="sale-text"> @lang('frontSite.sale') </span>
										</span>
										@endif
										
										<div class="product-action">
											
											<h4><a href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}">{{\Str::Words($fpro->name,3,'..')}}</a></h4>
											<ul class="pro-rating">
												<li class="pro-ratcolor"><i class="fa fa-star"></i></li>
												<li class="pro-ratcolor"><i class="fa fa-star"></i></li>
												<li class="pro-ratcolor"><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
											</ul>
											<span class="price">EGP {{$fpro->price}}</span>
										</div>
										<div class="pro-action">
											<ul>

												<!-- <li>
													<a class="test all_src_icon" href="#" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
													<i class="fa fa-heart" aria-hidden="true"></i>
													</a>
												</li> -->


												
												<li>
													<a class="test all_src_icon"  href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}" title="" 
													data-toggle="tooltip" data-placement="top" data-original-title="@lang('frontSite.show')">
													<i class="fa fa-eye" aria-hidden="true"></i>
													</a>
												</li>

												<li>
													<a class="test all_src_icon  pointer" href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}" 
													data-toggle="tooltip" data-placement="top" data-original-title="@lang('frontSite.addToCart')" >
													<i class="fa fa-shopping-cart" aria-hidden="true"></i>
													</a>
												</li>

												
											</ul>
										</div>
									</div>
								</div>
								@endforeach


					
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- NEW ARRIVALS END -->





		<!-- BEST SELLER START -->
		<section class="featured-area new-arrival">
			<div class="container">
				<div class="row">
					<div class="text-center">
						<div class="section-titel">
							<h3>{{ json_data($site_content,'section5_head') }}</h3>
						</div>
					</div>
					<div class="newarrival-area">
						<div id="newarrival-curosel2" class="indicator-style">

							@foreach($latest as $fpro)
								<div class="col-md-12">
									<div class="single-product">
										<div class="product-image">
											<a class="product-img prod-home-img" href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}">
												<img class="primary-img" src="{{getImage(PRODUCT_PATH.$fpro->img)}}" alt="{{$fpro->name}}" height="250" />
												<img class="secondary-img" src="{{getImage(PRODUCT_PATH.$fpro->img)}}" alt="{{$fpro->name}}" height="250" />
											</a>
										</div>
										@if($fpro->offer)
										<span class="onsale red">
											<span class="sale-text"> {{$fpro->offer}} %</span>
										</span>
										@else
										<span class="onsale">
											<span class="sale-text"> @lang('frontSite.sale') </span>
										</span>
										@endif
										
										<div class="product-action">
											
											<h4><a href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}">{{\Str::Words($fpro->name,3,'..')}}</a></h4>
											<ul class="pro-rating">
												<li class="pro-ratcolor"><i class="fa fa-star"></i></li>
												<li class="pro-ratcolor"><i class="fa fa-star"></i></li>
												<li class="pro-ratcolor"><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
											</ul>
											<span class="price">EGP {{$fpro->price}}</span>
										</div>
										<div class="pro-action">
											<ul>

												<!-- <li>
													<a class="test all_src_icon" href="#" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
													<i class="fa fa-heart" aria-hidden="true"></i>
													</a>
												</li>
 												-->

												
												<li>
													<a class="test all_src_icon"  href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}" title="" 
													data-toggle="tooltip" data-placement="top" data-original-title="@lang('frontSite.show')">
													<i class="fa fa-eye" aria-hidden="true"></i>
													</a>
												</li>

												<li>
													<a class="test all_src_icon  pointer" href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}" 
													data-toggle="tooltip" data-placement="top" data-original-title="@lang('frontSite.addToCart')" >
													<i class="fa fa-shopping-cart" aria-hidden="true"></i>
													</a>
												</li>

												
											</ul>
										</div>
									</div>
								</div>
								@endforeach


					
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- BEST SELLER END -->






		<!-- LATEST BLOG AREA START -->
		<section class="latest-blog-area">
			<div class="container">
				<div class="text-center">
					<div class="section-titel">
						<h3>  {{ json_data($site_content,'section6_head') }} </h3>
					</div>
				</div>
				<div class="row">
					<div class="blog-area">
					<div class="col-md-6 col-sm-12 res-mr-btm xs-res-mrbtm">
						<div class="blog-left">
							<a class="product-image-overlay" href="{{ json_data($collections,'hotdeal_link') }}">
								<img src="{{getImage(COLLECTION_PATH.json_data($collections,'hotdealImg'))}}" alt="" />
								<div class="left-content text-center">
									<h1> {{ json_data($collections,'hotdeal_title')  }} </h1>
									<p> {{ json_data($collections,'hotdeal_desc')  }}  </p>
								</div>
							</a>
						</div>
					</div>
					@foreach($posts as $pos)
					@if($loop->iteration == 1)
					<div class="col-md-3 col-sm-6 xs-res-mrbtm">
						<div class="blog-right">
							<a class="product-image-overlay" href="{{route('front.get.blog.show',[$pos->slug])}}">
								<img src="{{getImage(BLOG_PATH.'small/'.$pos->img)}}" alt="{{$pos->name}}" />
							</a>
							<div class="blog-content">
								<i class="fa fa-book"></i>
								<span>{{date('Y-M-D',strtotime($pos->created_at))}}</span>
								<p>{{\Str::Words($pos->small_description,'15','....')}}</p>
								<a href="{{route('front.get.blog.show',[$pos->slug])}}">  @lang('frontSite.viewMore') </a>
							</div>
						</div>
					</div>
					@else
					<div class="col-md-3 col-sm-6">
						<div class="blog-right">
							<div class="blog-content">
								<i class="fa fa-book"></i>
								<span>{{date('Y-M-D',strtotime($pos->created_at))}}</span>
								<p>{{\Str::Words($pos->small_description,'15','....')}}</p>
								<a href="{{route('front.get.blog.show',[$pos->slug])}}">  @lang('frontSite.viewMore') </a>
							</div>
							<a class="product-image-overlay" href="{{route('front.get.blog.show',[$pos->slug])}}">
								<img src="{{getImage(BLOG_PATH.'small/'.$pos->img)}}" alt="{{$pos->name}}" />
							</a>
						</div>
					</div>
					@endif

					@endforeach
					</div>
				</div>
			</div>
		</section>
		<!-- LATEST BLOG AREA END -->




@endsection

		
