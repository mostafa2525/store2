@extends('front2.main')

@section('style')
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />

<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
@endsection
<?php $cover = json_data($site_content,'blog_cover');  ?>
@section('content')



	<!-- BANNER AREA STRAT -->
		<section class="bannerhead-area" style="background: url('{{getImage(SETTINGS_PATH.$cover)}}'); background-repeat: no-repeat; background-size: cover;">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner-heading">
							<h1> {{$row->name}} </h1>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- BANNER AREA END -->


		<section class="latest-blog-area">
			<div class="container">
				<div class="text-center">
					<div class="section-titel" style="margin-top:100px;">
						<h3> {{$row->name}} </h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-10 col-sm-12 col-md-offset-1 xs-res-mrbtm">
						<div class="">
							
								<img src="{{getImage(BLOG_PATH.$row->img)}}" alt="{{$row->name}}" style="width: 400px; height: 400px; display: block; margin: auto;" />
							
							<div class="blog-content" style="margin: 50px; padding: 20px;">
									
								{!! $row->description !!}
								
							</div>



						</div>
					</div>
					<div class="col-md-10 col-sm-12 col-md-offset-1	 xs-res-mrbtm">
							<div id="share" class="text-center" ></div>
					</div>
				</div>
			</div>
		</section>





				<!-- LATEST BLOG AREA START -->
		<section class="latest-blog-area">
			<div class="container">
				<div class="text-center">
					<div class="section-titel" style="margin-top: 50px;">
						<h3> {{ json_data($site_content,'blog_title1') }}  </h3>
					</div>
				</div>
				<div class="row">
					<div class="blog-area">

					@foreach($posts as $pos)

					<div class="col-md-3 col-sm-6">
						<div class="blog-right">
							<div class="blog-content">
								<i class="fa fa-book"></i>
								<span>{{date('Y-M-D',strtotime($pos->created_at))}}</span>
								<p>{{\Str::Words($pos->small_description,'15','....')}}</p>
								<a href="{{route('front.get.blog.show',[$pos->slug])}}">READ MORE</a>
							</div>
							<a class="product-image-overlay" href="{{route('front.get.blog.show',[$pos->slug])}}">
								<img src="{{getImage(BLOG_PATH.'small/'.$pos->img)}}" alt="{{$pos->name}}" />
							</a>
						</div>
					</div>

					@endforeach
					
					</div>
				</div>
			</div>
		</section>
		<!-- LATEST BLOG AREA END -->










@endsection

@section('script')

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
<script>
    $("#share").jsSocials({
        shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon"]
    });
</script>


@endsection