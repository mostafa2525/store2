@extends('front2.main')


@section('content')



<!-- BANNER AREA STRAT -->
<section class="bannerhead-area">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="banner-heading">
					<h1>@lang('frontSite.search')</h1>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- BANNER AREA END -->
<!-- SHOP AREA START -->
<section class="wishlist-area shop-area">
	<div class="container">
		<div class="row">
			<div class="col-md-3">
				<div class="wishlist-left-area">

					<div class="category">
						<h4>{{json_data($site_content,'cat_title3')}}</h4>

						<div class="category-list">
							<ul>
								@foreach($categories as $cat)
								<li>
									<a href="{{route('front.get.product.category',[$cat->slug])}}">
										<i class="fa fa-angle-double-right"></i>{{\Str::Words($cat->name,'3','')}} <span>({{$cat->product()->count()}})</span>
									</a>
								
								</li>
								@endforeach
							</ul>
						</div>



						<div class="price-slider">
							<h4>{{json_data($site_content,'cat_title1')}}</h4>
							<aside class="widget shop-filter">
								<form class="price_filter" method="post" id="price_filter">
									@csrf
									<div id="slider-range"></div>
									<input type="hidden" name="min" id="min">
									<input type="hidden" name="max" id="max">
									<div class="price_slider_amount">
										<input type="text" id="amount" name="price"  placeholder="Add Your Price" />
										<input type="submit"  value="Filter"/>  
									</div>
								</form>							
							</aside>
						</div>
						<div class="price-slider size-area">
							<h4>{{json_data($site_content,'cat_title2')}}</h4>
							<ul>
								@foreach($sizes as $size)
								<li><a href="#">{{$size->title}}</a></li>
								@endforeach
							</ul>
						</div>
					</div>




					





					@if($offers->count())
					<div class="compare-products">
						<h4 class="text-center mt-10" style="margin-top: 20px;">{{json_data($site_content,'cat_title4')}}</h4>

						@foreach($offers as $offer)
						<div class="single-product">
							<a href="{{route('front.get.product.show',[$offer->category->slug,$offer->sub->slug,$offer->slug])}}">
								<img src="{{getImage(PRODUCT_PATH.$offer->img)}}" alt="{{$offer->name}}" />
							<span>EGP {{ $offer->price}}</span>
							<p>SAVE UP TO {{$offer->offer}} % OFF</p>
							</a>
						</div>
						@endforeach
					</div>
					@endif

					
					<div class="shop-top-seller">
						<h4>{{json_data($site_content,'cat_title5')}}</h4>



						
						@foreach($fetured as $feat)
						<div class="shop-single-main">
							<div class="top-seller-product">
								<img src="{{getImage(PRODUCT_PATH.$feat->img)}}" alt="{{$feat->name}}" style="width: 104px; height: 104px;"> 
							</div>
							<div class="top-seller-details">
								<h5>
									<a href="{{route('front.get.product.show',[$feat->category->slug,$feat->sub->slug,$feat->slug])}}">
										{{Str::Words($feat->name,3,'..')}}
									</a>
								</h5>
								<h5>EGP {{ $feat->price}}</h5>
								<ul>
									@if($feat->colors)
										<?php $colors = json_decode($feat->colors); ?>
										@foreach($colors as $co)
											@if($feat->getColor($co))
											<i class="fa fa-square" aria-hidden="true" style="color: {{$feat->getColor($co)->color}}" ></i>
											@endif
										@endforeach
									@endif
								</ul>
								
							</div>
						</div>
						@endforeach


					</div>
				</div>
			</div>
			<div class="col-md-9">
				<div class="shop-right-area">
					<div class="shop-banner">
						<img src="{{furl()}}/img/other-pg/shop-1.jpg" alt="" />
					</div>
					<div class="shop-tab-area">
						<!--NAV PILL-->
						<div class="shop-tab-pill">
						
						</div>
						<div class="coverLoading" style=""><img src="{{furl()}}/img/loading.gif"></div>
						<!--NAV PILL-->
						<div class="tab-content">
							<div class="row tab-pane active data-prod" id="grid">


			
							@if($allProducts->count())
								@foreach($allProducts as $fpro)
								<div class="col-md-4 col-sm-4">
									<div class="single-product">
										<div class="product-image">
											<a class="product-img prod-home-img" href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}">
												<img class="primary-img" src="{{getImage(PRODUCT_PATH.$fpro->img)}}" alt="{{$fpro->name}}" height="250" />
												<img class="secondary-img" src="{{getImage(PRODUCT_PATH.$fpro->img)}}" alt="{{$fpro->name}}" height="250" />
											</a>
										</div>
										@if($fpro->offer)
										<span class="onsale red">
											<span class="sale-text"> {{$fpro->offer}} %</span>
										</span>
										@else
										<span class="onsale">
											<span class="sale-text"> @lang('frontSite.sale') </span>
										</span>
										@endif
										
										<div class="product-action">
											<ul class="pro-rating">
												<li class="pro-ratcolor">{{ $fpro->sub->name }}</li>
											</ul>
											<h4><a href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}">{{\Str::Words($fpro->name,3,'..')}}</a></h4>
											<span class="price">EGP {{ $fpro->price}}</span>
										</div>
										<div class="pro-action">
											<ul>

												<!-- <li>
													<a class="test all_src_icon" href="#" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
													<i class="fa fa-heart" aria-hidden="true"></i>
													</a>
												</li> -->
												
												<li>
													<a class="test all_src_icon "  href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}" title="" 
													data-toggle="tooltip" data-placement="top" data-original-title="@lang('frontSite.show')">
													<i class="fa fa-eye" aria-hidden="true"></i>
													</a>
												</li>

												<li>
													<a class="test all_src_icon pointer"  href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}"
													data-toggle="tooltip" data-placement="top" data-original-title="@lang('frontSite.addToCart')">
													<i class="fa fa-shopping-cart" aria-hidden="true"></i>
													</a>
												</li>

												
											</ul>
										</div>
									</div>
								</div>
								@endforeach




								<div class="col-sm-12">
									<div class="single-product">
										{{ $allProducts->appends(request()->query())->links() }}
									</div>
								</div>


							@else
								<div class="col-sm-12">
									<div class="single-product text-center alert alert-danger">
										@lang('frontSite.dataNotFound')
									</div>
								</div>
							@endif


							</div>
						
						</div>
						<!--NAV PILL-->
						<div class="shop-tab-pill dwn">
							
						</div>
						<!--NAV PILL-->
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- SHOP AREA END -->



		
@endsection

@section('script')



<script type="text/javascript">
  



    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });


    $("#price_filter").submit(function(e) 
    {
        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#price_filter')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.post.product.filter')}}",
           data:formData,
           contentType: false,
           processData: false,
           beforeSend:function()
           {
           	   $(".coverLoading").css("display","block")
           	   $("#price_filter input[type='submit']").css("display","none");
           },
           success:function(data)
           {
             $(".data-prod").html('');
             $(".data-prod").html(data);
           	 $(".coverLoading").css("display","none")
           	 $("#price_filter input[type='submit']").css("display","block");


           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
             $(".coverLoading").css("display","none")
              
            }

        });

	});

</script>



@endsection

