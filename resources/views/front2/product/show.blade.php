@extends('front2.main')
@section('style')
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />

<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
@endsection

@section('content')









	<!-- PRODUCT SINGLE AREA START -->
		<div class="product-simple-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="single-product-image">
							<div class="single-product-tab">
							  <!-- Nav tabs -->
							  <!-- Nav tabs -->
							  <ul class="nav nav-tabs" role="tablist">
								

								
							  
								<li role="presentation" class="active">
									<a href="#home" aria-controls="home" role="tab" data-toggle="tab"><img alt="" src="{{getImage(PRODUCT_PATH.$row->img)}}"></a>
								</li>


								
								@foreach($row->allImages as $img)
								<li role="presentation">
									<a href="#t-{{$loop->iteration}}" aria-controls="t-{{$loop->iteration}}" role="tab" data-toggle="tab"><img alt="" src="{{getImage(PRODUCT_PATH_IMAGES.$img->image)}}"></a>
								</li>
								@endforeach
							



							  </ul>

							  <!-- Tab panes -->
							  <div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="home">
									<img alt="" src="{{getImage(PRODUCT_PATH.$row->img)}}" style="width:100%; height: 100%;">
								</div>


				
								@foreach($row->allImages as $img)
								<div role="tabpanel" class="tab-pane" id="t-{{$loop->iteration}}" >
									<img alt="" src="{{getImage(PRODUCT_PATH_IMAGES.$img->image)}}" style="width:100%; height: 100%;">
								</div>
								@endforeach
						



							  </div>
							</div>
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<div class="single-product-info">
							<h1 class="product_title">{{$row->name}}</h1>
							<div class="price-box">
								
								@if($row->offer)
								<span class="new-price"> EGP {{  $row->price - ($row->price * ($row->offer/100))}}</span> -  
								<span class="old-price"><del>EGP {{ $row->price}} </del></span>
								@else
								<span class="old-price"> EGP {{ $row->price}} </span>
								@endif
							</div>
							<div class="pro-rating">
								<a href="#"><i class="fa fa-star"></i></a>
								<a href="#"><i class="fa fa-star"></i></a>
								<a href="#"><i class="fa fa-star"></i></a>
								<a href="#"><i class="fa fa-star"></i></a>
								<a href="#"><i class="fa fa-star"></i></a>
							</div>
							<div class="short-description">
								<p>{{$row->small_desc}}</p>						
							</div>

							<div class="row">
								<div class="col-md-12 col-xs-12">
									<label> {{json_data($site_content,'product_title1')}} </label>
								</div>
								<div class="col-md-12 col-xs-12">
									<div class="btn-group" data-toggle="buttons">
										
									
											<?php $i = 1; ?>

										@foreach($row->allSizes as $size)
											@if($i==1)
											<?php $siz=$size->size_id; ?>
											@endif
										<label class="btn btn-default @if($i==1) active @endif size-cart" data-size="{{$size->id}}" >
											<input type="radio"  id="option{{$i}}"  autocomplete="off" 
											@if($i==1) checked @endif  > 
											{{$size->sizeData->title}}
										</label>
										<?php $i++; ?>
										@endforeach
										<input type="hidden" name="data-size" class="data-size" value="{{$siz}}">
									

										
										
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-xs-12">
									<br>
									<label>{{json_data($site_content,'product_title2')}}</label>
								</div>
								<div class="col-md-12 col-xs-12">
									<div class="btn-group color_box" data-toggle="buttons">

										

									<?php $i = 1; ?>
									@foreach($row->allColors as $co)
											@if($i==1)
											<?php $col=$co->color_id; ?>
											@endif
										<label class="btn btn-default color_select @if($i==1) active @endif color-cart" data-color="{{$co->id}}" style="background-color: {{$co->colorData->color}};">
											<input type="radio" name="options" id="option2" autocomplete="off" class="color-cart" value="{{$co->color_id}}" > <div class="img_selected" @if($i==1) checked @endif></div>
										</label>
										<?php $i++; ?>
									@endforeach
									<input type="hidden" name="data-size" class="data-color" value="{{$col}}">
								


									</div>
								</div>
							</div>


							<div class="row">
								<div class="col-md-12">
									<form action="#">
										<div class="panel-group" role="tablist">
											<div class="panel_cart">
												<div class="coverloading"><img src="{{furl()}}/img/loading.gif"></div>
												<div class="add_cart_new" role="tab" id="collapseListGroupHeading1">
													<h4 class="panel-title"> <a href="#collapseListGroup1" class="addToCart collapsed" role="button" data-toggle="collapse" aria-expanded="false" aria-controls="collapseListGroup1" 
														data-proId="{{$row->id}}"> <i class="fa fa-shopping-cart" aria-hidden="true"></i>  {{json_data($site_content,'product_btn1')}}  </a> </h4>
												</div>
												<div class="panel-collapse collapse" role="tabpanel" id="collapseListGroup1" aria-labelledby="collapseListGroupHeading1" aria-expanded="false" style="height: 0px;">
													<ul class="box_acr">
														<li class="cont-add-item"> <a href="{{route('front.get.home.index')}}"> {{json_data($site_content,'product_btn2')}}  </a></li>
														<li class="che-add-item"> <a href="{{route('front.get.cart.show')}}"> {{json_data($site_content,'product_btn3')}}  </a></li>
													</ul>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>




							<div class="share_buttons">
								<a href="#"><img src="img/share-img.png" alt="" /></a>
							</div>
						</div>
					</div>

					<div class="col-sm-12">
							<div id="share" class="text-center" ></div>
					</div>
				</div>
			</div>
		</div>
		<!-- PRODUCT SINGLE AREA END -->






























		<!-- PRODUCT TAB AREA START -->
		<div class="product-tab-area">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="product-tabs">
							<div>
							  <!-- Nav tabs -->
							  <ul class="nav nav-tabs" role="tablist">
								<li role="presentation" class="active"><a href="#tab-desc" aria-controls="tab-desc" role="tab" data-toggle="tab">{{json_data($site_content,'product_title3')}}</a></li>


							<!-- 	<li role="presentation"><a href="#page-comments" aria-controls="page-comments" role="tab" data-toggle="tab">Reviews (1)</a></li> -->
							  </ul>
							  <!-- Tab panes -->
							  <div class="tab-content">
								<div role="tabpanel" class="tab-pane active" id="tab-desc">
									<div class="product-tab-desc">
										<p>{!! $row->desc !!}</p>
									</div>
								</div>
					
							  </div>
							</div>						
						</div>
						<div class="clear"></div>
						<div class="upsells_products_widget">
							<div class="text-center">
								<div class="section-titel">
									<h3>Related Products</h3>
								</div>
							</div>
							<div class="row">






								@foreach($related as $fpro)
								<div class="col-md-3 col-sm-4">
									<div class="single-product">
										<div class="product-image">
											<a class="product-img prod-home-img" href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}">
												<img class="primary-img" src="{{getImage(PRODUCT_PATH.$fpro->img)}}" alt="{{$fpro->name}}" height="250" />
												<img class="secondary-img" src="{{getImage(PRODUCT_PATH.$fpro->img)}}" alt="{{$fpro->name}}" height="250" />
											</a>
										</div>
										@if($fpro->offer)
										<span class="onsale red">
											<span class="sale-text"> {{$fpro->offer}} %</span>
										</span>
										@else
										<span class="onsale">
											<span class="sale-text"> @lang('frontSite.sale') </span>
										</span>
										@endif
										
										<div class="product-action">
											<ul class="pro-rating">
												<li class="pro-ratcolor">{{ $fpro->category->name }}</li>
											</ul>
											<h4><a href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}">{{\Str::Words($fpro->name,3,'..')}}</a></h4>
											<span class="price">EGP {{$fpro->price}}</span>
										</div>
										<div class="pro-action">
											<ul>


												<!-- <li>
													<a class="test all_src_icon" href="#" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
													<i class="fa fa-heart" aria-hidden="true"></i>
													</a>
												</li> -->
												
												<li>
													<a class="test all_src_icon "  href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}" title="" 
													data-toggle="tooltip" data-placement="top" data-original-title="@lang('frontSite.show')">
													<i class="fa fa-eye" aria-hidden="true"></i>
													</a>
												</li>

												<li>
													<a class="test all_src_icon  pointer"  href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}"	data-toggle="tooltip" data-placement="top" data-original-title="@lang('frontSite.addToCart')">
													<i class="fa fa-shopping-cart" aria-hidden="true"></i>
													</a>
												</li>

												
											</ul>
										</div>
									</div>
								</div>
								@endforeach		
												
							</div>
						</div>






						<div class="clear"></div>
						<div class="upsells_products_widget">
							<div class="text-center">
								<div class="section-titel">
									<h3>Featured Products</h3>
								</div>
							</div>
							<div class="row">






								@foreach($fetured as $fpro)
								<div class="col-md-3 col-sm-4">
									<div class="single-product">
										<div class="product-image">
											<a class="product-img prod-home-img" href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}">
												<img class="primary-img" src="{{getImage(PRODUCT_PATH.$fpro->img)}}" alt="{{$fpro->name}}" height="250" />
											
											</a>
										</div>
										@if($fpro->offer)
										<span class="onsale red">
											<span class="sale-text"> {{$fpro->offer}} %</span>
										</span>
										@else
										<span class="onsale">
											<span class="sale-text"> @lang('frontSite.sale') </span>
										</span>
										@endif
										
										<div class="product-action">
											<ul class="pro-rating">
												<li class="pro-ratcolor">{{ $fpro->category->name }}</li>
											</ul>
											<h4><a href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}">{{\Str::Words($fpro->name,3,'..')}}</a></h4>
											<span class="price">$ {{$fpro->price}}</span>
										</div>
										<div class="pro-action">
											<ul>

												<!-- <li>
													<a class="test all_src_icon" href="#" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
													<i class="fa fa-heart" aria-hidden="true"></i>
													</a>
												</li> -->
												
												<li>
													<a class="test all_src_icon "  href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}" title="" 
													data-toggle="tooltip" data-placement="top" data-original-title="@lang('frontSite.show')">
													<i class="fa fa-eye" aria-hidden="true"></i>
													</a>
												</li>

												<li>
													<a class="test all_src_icon  pointer" href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}"
													data-toggle="tooltip" data-placement="top" data-original-title="View Details">
													<i class="fa fa-shopping-cart" aria-hidden="true"></i>
													</a>
												</li>

												
											</ul>
										</div>
									</div>
								</div>
								@endforeach		
												
							</div>
						</div>





					</div>
				
				</div>
			</div>
		</div>
		<!-- PRODUCT TAB AREA START -->



		
@endsection


@section('script')

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
<script>
    $("#share").jsSocials({
        shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon"]
    });



    var size = $(".data-size").val();
    var color = $(".data-color").val();

    $(document).on("click",".size-cart",function() 
    {
    	size = $(this).attr("data-size");
    });
    $(document).on("click",".color-cart",function() 
    {
    	color = $(this).attr("data-color");
    });


    $(document).on("click",".addToCart",function(e) 
    {

        e.preventDefault();
        el = $(this);

        // return false;
        var prodId = $(this).attr("data-proId");
        $.ajax({

           type:'POST',
           url:"{{route('front.post.cart.add')}}",
           data:{
            "_token": "{{ csrf_token() }}",
            "prodId": prodId,"size":size,"color":color },
            beforeSend:function()
            {
            	$(".coverloading").css("display","block");
            },
           success:function(data)
           {
              if(data.success)
              {
              	console.log(data.success)
                $.toast({
                    heading: 'Message',
                    text: data.success,
                    position: 'top-right',
                    stack: false,
                    icon: 'success',
                    hideAfter: 3000 
                })

                $(".coverloading").css("display","none");
                el.removeClass("addToCart");

              }
              else
              {
                 $("#cart-content").append(data);
                 var num = $(".header-middle-checkout").length;
                 $("#qCart span").text(num);


                 $.toast({
                    heading: 'Message',
                    text: "{{trans('frontSite.itemAddedToCart')}}",
                    position: 'top-right',
                    stack: false,
                    icon: 'success',
                    hideAfter: 3000 
                });

                $(".coverloading").css("display","none");


              }


           }
         

        });

    });



</script>





@endsection