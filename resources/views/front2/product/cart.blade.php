@extends('front2.main')

@section('content')

<?php $cover = json_data($site_content,'cart_cover');  ?>


<!-- BANNER AREA STRAT -->
		<section class="bannerhead-area"  style="background: url('{{getImage(SETTINGS_PATH.$cover)}}'); background-repeat: no-repeat; background-size: cover;"  >
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner-heading">
							<h1>{{ json_data($site_content,'cart_title1') }}</h1>
						</div>
					</div>
				</div>
			</div>
		</section>

		@if(\Cart::getTotalQuantity())

		<!-- BANNER AREA END -->
		<!-- SHOPING CART AREA START -->
		<section class="shoping-cart-area">
			<div class="container">
				<div class="row">


					<!-- DISCOUNT SUBTOTAL AREA STRAT -->
					<div class="col-md-3">
						<section class="discount-area">
							<div class="subtotal-main-area">
								<div class="subtotal-area">
									<h2 class="tit_subt total-price-cart">{{ json_data($site_content,'cart_title2') }}<span>EGP {{  \Cart::getSubTotal() }}</span></h2>
								</div>
								<div class="subtotal-area">
									<h2>{{ json_data($site_content,'cart_title3') }}<span>EGP {{$setting->price_charge}}</span></h2>
								</div>
								<div class="subtotal-area">
									<h2 class="tit_tot total-price-charge">{{ json_data($site_content,'cart_title4') }}<span> EGP - {{  \Cart::getSubTotal() + $setting->price_charge }}</span></h2>
								</div>
								<a href="{{route('front.get.order.checkout')}}">{{ json_data($site_content,'cart_title5') }}</a>
							</div>
						</section>
					</div>
					<!-- DISCOUNT SUBTOTAL AREA END -->



					<div class="col-md-9">
						<form method="post" action="{{route('front.post.cart.update')}}">
							@csrf
						<div class="wishlist-table-area table-responsive">
							<table>
								<thead>
									<tr>
										<th class="product-name">{{ json_data($site_content,'cart_title6') }}</th>
										<th class="product-unit-price">{{ json_data($site_content,'cart_title7') }}</th>
										<th class="product-quantity">{{ json_data($site_content,'cart_title8') }}</th>
										<th class="product-subtotal">{{ json_data($site_content,'cart_title9') }}</th>
									</tr>
								</thead>
								<tbody>
											
									@foreach(\Cart::getContent() as $cart)
									<tr>

										<td class="product-name">
											<h3><a href="{{route('front.get.product.show',[$cart->attributes->catSlug,$cart->attributes->subCatslug,$cart->attributes->slug])}}" class="name_cart">{{$cart->name}}</a></h3>
											<a href="#" class="img_cart">
												<img alt="{{$cart->name}}" src="{{getImage(PRODUCT_PATH.$cart->attributes->img)}}">
											</a>
											<a href="{{route('front.get.cart.delete',[$cart->id])}}" class="itemcartRemove clear_cart" >
												<i class="fa fa-trash-o"></i>
											</a>
										</td>


										<td class="product-unit-price">
											<p>  EGP {{ $cart->price}}  </p>
										</td>
										<td class=" product-cart-details">

							
											<input type="number" class="cart-quant" value="{{$cart->quantity}}" min="1" name="qunt[]" data-cartId="{{$cart->id}}" data-price="{{$cart->price}}" >

											<input type="hidden" name="prodCart[]" value="{{$cart->id}}" >
										</td>
										<td class="product-quantity" >
											<p>EGP {{ $cart->price * $cart->quantity}}</p>
										</td>
									</tr>
									@endforeach
											
								
								</tbody>
							</table>
									
						</div>

						<div class="shopingcart-bottom-area">
							<a href="{{route('front.get.home.index')}}" class="bottoma">{{ json_data($site_content,'cart_title10') }}</a>
							<div class="bottom-button">
								<a href="{{route('front.get.cart.clear')}}" class="bottomb">{{ json_data($site_content,'cart_title11') }}</a>
								<!-- <button type="submit" class=" update-cart-btn" >{{-- json_data($site_content,'cart_title12') --}}</button> -->
							</div>
						</div>


						</form>


					</div>
				</div>
			</div>
		</section>
		<!-- SHOPING CART AREA END -->



		@else
										
			<h3 class="alert alert-danger text-center">@lang('frontSite.dataNotFound')</h3>

		@endif
		<!-- DISCOUNT SUBTOTAL AREA END -->



@endsection








@section('script')



<script type="text/javascript">
  



    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });


    $(".cart-quant").change(function(e) 
    {
        e.preventDefault();
        var el = $(this);
        var qty = $(this).val();
        var cartId = $(this).attr("data-cartId");
        var price = $(this).attr("data-price");

        el.parents("tr").find(".product-quantity p").html("EGP " + price * qty  )  

        $.ajax({

           type:'GET',
           url:"{{route('front.get.cart.edit')}}",
           data:{'qty':qty,'cartId':cartId},
           success:function(data)
           {
           		$(".total-price-cart span").html("EGP "+data.toatal);
           		$(".total-price-charge span").html("EGP "+data.toatalWithCharge);
           },
            error: function(xhr, status, error) 
            {
            }

        });

	});

</script>



@endsection
