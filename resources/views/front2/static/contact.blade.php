@extends('front2.main')


@section('content')


	




		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="page_contact_us">
						<center>
							<img src="{{furl()}}/img/footer_contact_chat.png" alt="">
							<br>
							<h3> {{ json_data($site_content,'contact_title1') }} </h3>
						</center>
						<!-- CONTACT FROM AREA START -->
						<div class="col-md-12">
							<div class="contact-from-heading">
								<h3>{{ json_data($site_content,'contact_title2') }}</h3>
								<p>{{ json_data($site_content,'contact_title3') }}</p>
							</div>
						</div>
						<section class="contact-from-area">
							<form id="contact-form">
												@csrf
				                        <div class="col-md-12">
				                            <ul id="errors"></ul>
				                        </div>
										<div class="col-md-6">
											<div class="contact-from-left">
												<div class="input-text">
													<input type="text" placeholder="{{ json_data($site_content,'contact_title4') }}" name="name" id="name" required  />
												</div>
												<div class="input-text">
													<input type="email" placeholder="{{ json_data($site_content,'contact_title5') }}" name="email" id="email"  required />
												</div>
												<div class="input-text">
													<input type="tel" placeholder="{{ json_data($site_content,'contact_title6') }}" name="phone" id="Phone"  required />
												</div>
												<div class="input-message">
												<input type="submit" value="{{ json_data($site_content,'contact_title8') }}"  id="submitMessage" required/>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="contact-from-right">
												<div class="input-message">
													<textarea id="message" placeholder="{{ json_data($site_content,'contact_title7') }}" name="message"  required ></textarea>
												</div>
											</div>
										</div>
							</form>
						</section>
						<div class="col-md-12">
							<div class="contact-from-fot">
								<p>{{ json_data($site_content,'contact_title9') }}</p>
							</div>
						</div>
						<!-- CONTACT FROM AREA END -->
					</div>
				</div>
			</div>
		</div>





	




@endsection




@section('script')
<script type="text/javascript">
  
    $('#contact-form').parsley();


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#contact-form").submit(function(e) 
    {
        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#contact-form')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.post.contactus.sendMessage')}}",
           data:formData,
           contentType: false,
           processData: false,
           beforeSend:function()
           {
           		$("#submitMessage").attr("disabled",true);
           },
           success:function(data)
           {
             $("#errors").html('');
             $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
             $('.input-text').find("input").val("");
             $('.input-message').find("textarea").val("");
           		$("#submitMessage").attr("disabled",true);

           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
           		$("#submitMessage").attr("disabled",true);
              
            }

        });

	});

</script>



@endsection