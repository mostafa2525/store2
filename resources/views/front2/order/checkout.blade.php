@extends('front2.main')

@section('content')
<?php $cover = json_data($site_content,'checkout_cover');  ?>



<!-- BANNER AREA STRAT -->
		<section class="bannerhead-area"  style="background: url('{{getImage(SETTINGS_PATH.$cover)}}'); background-repeat: no-repeat; background-size: cover;" >
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner-heading">
							<h1>{{ json_data($site_content,'checkout_title1') }}</h1>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- BANNER AREA END -->





		<!-- ACOOUNT FROM AREA START -->
		<section class="login-area">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
                        <ul id="errors"></ul>
                    </div>
                    <div class="col-md-6">
						<div class="checkout-left-area">
							<div class="panel-group" id="accordion">
							
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a><span>2</span> {{ json_data($site_content,'checkout_title2') }} </a>
										</h4>
									</div>
									<div id="collapse5" class="panel-collapse collapse in">
										<div class="panel-body">
											<div class="order-review" id="checkout-review">
												<div class="" id="checkout-review-table-wrapper">
													<table class="data-table table table-responsive" id="checkout-review-table">
														<thead>
														<tr>
															<th style="    width: 40%;">{{ json_data($site_content,'checkout_title3') }}</th>
															<th >{{ json_data($site_content,'checkout_title4') }}</th>
															<th >{{ json_data($site_content,'checkout_title5') }}</th>
															<th >{{ json_data($site_content,'checkout_title6') }}</th>
														</tr>
														</thead>
														<tbody>
														@foreach(\Cart::getContent() as $cart)
														<tr>
															<td>
																<h3 class="product-name">{{$cart->name}}</h3>
															</td>
															<td><span class="cart-price">
																<span class="price">EGP {{$cart->price}}  </span></span>
															</td>
															<td>{{$cart->quantity}}</td>
															<!-- sub total starts here -->
															<td><span class="cart-price"><span class="price">EGP
															{{$cart->price * $cart->quantity}}</span></span></td>
														</tr>
														@endforeach
														
														</tbody>
														<tfoot>
														<tr>
															<td colspan="3">{{ json_data($site_content,'checkout_title7') }}</td>
															<td><span class="price">EGP {{\Cart::getSubTotal()}}</span></td>
														</tr>
														<tr>
															<td colspan="3">{{ json_data($site_content,'checkout_title8') }}</td>
															<td><span class="price">EGP {{$setting->price_charge}}</span></td>
														</tr>
														<tr class="grand-total">
															<td colspan="3" class="grand-total"><strong>
																{{ json_data($site_content,'checkout_title9') }}
															</strong></td>
															<td><strong><span class="price">
																EGP {{ \Cart::getSubTotal() + $setting->price_charge}}
															</span></strong></td>
														</tr>
														
														</tfoot>
													</table>
												</div>

											</div>
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="checkout-left-area">
							<div class="panel-group" id="accordion">
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a><span>1</span>{{ json_data($site_content,'checkout_title10') }}</a>
										</h4>
									</div>
									<div id="collapse2" class="panel-collapse collapse in">
										<div class="panel-body">
											<div class="row">

												<form  id="orderForm">
													<div class="coverLoading" style=""><img src="{{furl()}}/img/loading.gif"></div>


													@csrf
							                        
													

													<div class="col-md-6">
														<div class="checkout-form-list">
															<label> {{ json_data($site_content,'checkout_title12') }} <span class="required">*</span></label>										
															<input  required value="{{clientAuth()->user()->name}}" type="text" name="name" placeholder="" style="padding:15px;"   />
														</div>
													</div>

													<div class="col-md-6">
														<div class="checkout-form-list">
															<label> {{ json_data($site_content,'checkout_title13') }} <span class="required">*</span></label>										
															<input  required value="{{clientAuth()->user()->mobile1}}" type="text" name="mobile" placeholder="" style="padding:15px;"   />
														</div>
													</div>
		
													<div class="col-md-12">
														<div class="checkout-form-list">
															<label> {{ json_data($site_content,'checkout_title14') }}  <span class="required">*</span></label>
															<input  required type="text"  name="address"  placeholder="" value="{{clientAuth()->user()->address}}"  style="padding:15px;"  />
														</div>
													</div>


													<div class="col-md-12">
														<div class="">
															<br>
														<br>
															<label> {{ json_data($site_content,'checkout_title11') }} <span class="required">*</span></label>
															<select name="gov_id" required class="form-control">
															  <option value="">@lang('frontSite.choose')</option>
															 @foreach($govs as $gov)
															  <option value="{{$gov->id}}">{{$gov->name}}</option>
															  @endforeach
															</select> 										
														</div>
														<br>
														<br>
												
													</div>
													

													<div class="col-md-12">
														<div class="checkout-form-list">
															<label> @lang('frontSite.city') <span class="required">*</span></label>										
															<input  required type="text" name="city" style="padding:15px;"   placeholder="" />
														</div>
													</div>



													<div class="col-md-12" id="">
														<div class="cart-btn-3" id="review-buttons-container">
															<hr>
															<h3 class="text-center" style="color:#64B802;">ِ@lang('frontSite.paymentDleviry')</h3>
															<hr>
															
														</div>
													</div>	

													

													<div class="col-md-12" id="">
														<div class="cart-btn-3" id="review-buttons-container">
															<p class="left">
																<a href="{{route('front.get.cart.show')}}" class="pointer"> {{ json_data($site_content,'checkout_title15') }} </a></p>
															<div class="buttons-set">
																<button class="button" type="submit"><span>{{ json_data($site_content,'checkout_title16') }}</span></button>
															</div>
														</div>
													</div>	

												</form>
											</div>
										</div>
									</div>
								</div>
					
							</div>
						</div>
					</div>

					



				</div>
			</div>
		</section>
		<!-- ACOOUNT FROM AREA END -->





@endsection



@section('script')
<script type="text/javascript">
  
    $('#orderForm').parsley();


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#orderForm").submit(function(e) 
    {

        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#orderForm')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.post.order.sendOrder')}}",
           data:formData,
           contentType: false,
           processData: false,
           beforeSend:function()
           {
           	   $(".coverLoading").css("display","block")
           	   $("#orderForm input[type='submit']").css("display","none");
           },
           success:function(data)
           {
             $("#errors").html('');
             $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
             $('input').val("");
             $("textarea").val("");
             $(".coverLoading").css("display","none")
           	 $("#price_filter input[type='submit']").css("display","block");
           	 
           	 $.toast({
                    heading: 'Message',
                    text: data.success,
                    position: 'top-left',
                    stack: false,
                    icon: 'success',
                    hideAfter: 3000 
                })

             setTimeout(function(){ location.href = "{{route('front.get.home.index')}}"}, 3000);
           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
             $(".coverLoading").css("display","none")
              
            }

        });

	});

</script>



@endsection
