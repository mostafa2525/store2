@include('front2.inc._linksHeader')
@include('front2.inc._header')
@include('front2.inc._nav')

@yield('content')

@include('front2.inc._footer')
@include('front2.inc._linksFooter')