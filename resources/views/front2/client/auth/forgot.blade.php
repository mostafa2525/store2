@extends('front2.main')


@section('content')

<?php $cover = json_data($site_content,'login_cover');  ?>

	<!-- BANNER AREA STRAT -->
		<section class="bannerhead-area" style="background: url('{{getImage(SETTINGS_PATH.$cover)}}'); background-repeat: no-repeat; background-size: cover;">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner-heading">
							<h1>{{  json_data($site_content,'login_title1')}}</h1>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- BANNER AREA END -->
		<!-- ACOOUNT FROM AREA START -->
		<section class="login-area">
			<div class="container">
				<div class="row">
					<div class="account-details">
					
						<div class="col-lg-6 col-md-6 col-sm-6 col-sm-offset-3">
							<form method="post" class="login-form" id="forgot-form" >
								<div class="coverLoading" style=""><img src="{{furl()}}/img/loading.gif"></div>

								<h1 class="heading-title">@lang('frontSite.typeYourEmail')</h1>
								@csrf
		                        <div class="col-md-12">
		                            <ul id="errors"></ul>
		                        </div>
								
								<p class="form-row">
									<label>{{  json_data($site_content,'login_title3')}}</label>
									<input type="email" name="email" value="{{old('email')}}" required maxlength="100">
								</p>
								<!-- <p class="lost-password form-group"><a rel="nofollow" href="#">Forgot your password?</a></p> -->
								<p class="submit">				
									<button class="" name="SubmitLogin" id="submitlogin" type="submit">
										<span><i class="fa fa-lock"></i>@lang('frontSite.send')</span>
									</button>
								</p>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- ACOOUNT FROM AREA END -->



		




@endsection


@section('script')
<script type="text/javascript">
  
    $('#forgot-form').parsley();


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#forgot-form").submit(function(e) 
    {
        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#forgot-form')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.client.post.auth.forgotPassword')}}",
           data:formData,
           contentType: false,
           processData: false,
           beforeSend:function()
           {
           	   $(".coverLoading").css("display","block")
           	   $("#price_filter input[type='submit']").css("display","none");
           },
           success:function(data)
           {
             $("#errors").html('');
             $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
             $("input").val("");
             $(".coverLoading").css("display","none")

           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
             $(".coverLoading").css("display","none")

            }

        });

	});

</script>



@endsection