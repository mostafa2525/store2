@extends('front2.main')


@section('content')

<?php $cover = json_data($site_content,'login_cover');  ?>

	<!-- BANNER AREA STRAT -->
		<section class="bannerhead-area" style="background: url('{{getImage(SETTINGS_PATH.$cover)}}'); background-repeat: no-repeat; background-size: cover;">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner-heading">
							<h1>{{  json_data($site_content,'login_title1')}}</h1>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- BANNER AREA END -->






		<!-- ACOOUNT FROM AREA START -->
		<section class="login-area">
			<div class="container">
				<div class="row">
					<div class="account-details">
						<div class="col-sm-12">
							@include('_messages')
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6 xs-res-mrbtm">
							<form method="post" class="create-account-form" id="reg-form" action="{{route('front.client.post.auth.doRegister')}}">
								<h1 class="heading-title">{{ json_data($site_content,'login_title2') }}</h1>
					
								@csrf

							

								<p class="form-row">
									<label>{{ json_data($site_content,'login_title3') }}</label>
									<input type="text" name="name" value="{{old('name')}}" required maxlength="100">
									
								</p>

								<p class="form-row">
									<label>{{ json_data($site_content,'login_title4') }}</label>
									<input type="email" name="email" value="{{old('email')}}" required maxlength="100">
								</p>

								<p class="form-row">
									<label>{{ json_data($site_content,'login_title5') }}</label>
									<input type="tel" name="mobile1" value="{{old('email')}}" required maxlength="100">
								</p>

								<p class="form-row">
									<label>{{ json_data($site_content,'login_title6') }}</label>
									<input type="password" name="password" required maxlength="50">
								</p>

								<p class="form-row">
									<label> {{ json_data($site_content,'login_title7') }} </label>
									<input type="password" name="confirm-password" required maxlength="50">
								</p>

								<div class="submit">					
									<button name="submitcreate" id="submitcreate" type="submit" class="">
										<span>
											<i class="fa fa-user left"></i>
											{{ json_data($site_content,'login_title8') }}
										</span>
									</button>
								</div>		
							</form>
						</div>
						<div class="col-lg-6 col-md-6 col-sm-6">
							<form method="post" class="login-form" id="log-form" action="{{route('front.client.post.auth.doLogin')}}">
								<h1 class="heading-title">{{ json_data($site_content,'login_title9') }}</h1>
								@csrf
							
					
								<p class="form-row">
									<label>{{ json_data($site_content,'login_title10') }}</label>
									<input  type="email" name="email" value="{{old('email')}}" required maxlength="100">
									<input type="hidden" name="login" value="login">
								</p>
								<p class="form-row">
									<label>{{ json_data($site_content,'login_title11') }}</label>
									<input  type="password" name="password" required maxlength="50">
								</p>
								<p class="lost-password form-group">
									<a rel="nofollow" href="{{route('front.client.get.auth.forgot')}}">
									{{ json_data($site_content,'login_title12') }} </a>
								</p>
								<p class="submit">				
									<button class="" name="SubmitLogin" id="submitlogin" type="submit">
										<span><i class="fa fa-lock"></i>{{ json_data($site_content,'login_title13') }}</span>
									</button>
								</p>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- ACOOUNT FROM AREA END -->



		




@endsection

@section('script')
<script type="text/javascript">
  
    $('#log-form').parsley();

    $('#reg-form').parsley();

 </script>

@endsection