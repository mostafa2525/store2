<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="view_die_account">
			<h3>{{ json_data($site_content,'Eprofile_title1') }} </h3>
			<hr>
			<form method="post" class="create-account-form" id="reg-form" action="{{route('front.client.post.auth.editProfile')}}">
				@csrf
				@include('admin.msg._messages')

				<p class="form-row">
					<label>{{ json_data($site_content,'Eprofile_title2') }} ( * )</label>
					<input type="text" name="name" value="{{clientAuth()->user()->name}}" required maxlength="100">
					
				</p>

				<p class="form-row">
					<label>{{ json_data($site_content,'Eprofile_title3') }} ( * ) </label>
					<input type="text" name="address" value="{{clientAuth()->user()->address}}" required maxlength="300">
				</p>


				<p class="form-row">
					<label>{{ json_data($site_content,'Eprofile_title4') }} ( * )  </label>
					<input type="tel" name="mobile1" value="{{clientAuth()->user()->mobile1 }}" required maxlength="100" >
				</p>

				<p class="form-row">
					<label> {{ json_data($site_content,'Eprofile_title5') }} ( optional ) </label>
					<input type="tel" name="mobile2" value="{{clientAuth()->user()->mobile2 }}"  maxlength="100" >
				</p>


				<p class="form-row">
					<label> {{ json_data($site_content,'Eprofile_title6') }} ( optional )  </label>
					<input type="tel" name="more_info" value="{{clientAuth()->user()->more_info }}"  maxlength="100" >
				</p>

				<div class="submit">					
					<button name="submitcreate" id="submitcreate" type="submit" class="">
						<span>
							<i class="fa fa-user left"></i>
							{{ json_data($site_content,'Eprofile_title7') }}
						</span>
					</button>
				</div>		
			</form>
		</div>
	</div>
	
</div>