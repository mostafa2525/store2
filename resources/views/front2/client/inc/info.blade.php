<div class="row">
	<div class="col-md-6 col-xs-12">
		<div class="view_die_account">
			<h3>{{ json_data($site_content,'profile_title8') }}</h3>
			<label>{{ json_data($site_content,'profile_title9') }}</label>
			<p>
				@if(clientAuth()->user()->address)
					{{clientAuth()->user()->address}}
				@else
					There Is No Default Address Untl Now
				@endif
			</p>
			<a href="{{route('front.client.get.profile.index','edit-info')}}"> {{ json_data($site_content,'profile_title10') }}  <i class="fa fa-pencil" aria-hidden="true"></i>  </a>
		</div>
	</div>
	<div class="col-md-6 col-xs-12">
		<div class="view_die_account">
			<h3>{{ json_data($site_content,'profile_title11') }}</h3>
			<label>{{clientAuth()->user()->name}}</label>
			<p>{{clientAuth()->user()->email}}</p>
			<a href="{{route('front.client.get.profile.index','change-password')}}">{{ json_data($site_content,'profile_title12') }}  <i class="fa fa-pencil" aria-hidden="true"></i>  </a>
		</div>
	</div>
</div>