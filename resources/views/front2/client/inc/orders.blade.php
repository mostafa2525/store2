<div class="row">
	<div class="panel-body">
	<div class="coupon-info">
		<h1 class="heading-title">  </h1>
		


		@if(clientAuth()->user()->orders)

		<table class="table table-bordered table-responsive">
			<thead>
				<tr>
					<tr>
						<th class="product-name">{{ json_data($site_content,'orders_title1') }}</th>
						<th class="product-unit-price">{{ json_data($site_content,'orders_title2') }}</th>
						<th class="product-quantity">{{ json_data($site_content,'orders_title3') }}</th>
						<th class="product-subtotal">{{ json_data($site_content,'orders_title4') }}</th>
						<th class="product-subtotal">{{ json_data($site_content,'orders_title5') }}</th>
					</tr>
				</tr>
			</thead>
			<tbody>

			@foreach(clientAuth()->user()->orders as $ord)
						
				<tr>
					<td colspan="1" class="order-info">{{ json_data($site_content,'orders_title6') }} : {{ $ord->code}}</td>
					<td colspan="2" class="order-info">{{ json_data($site_content,'orders_title7') }} : {{ date('Y-m-D',strtotime($ord->created_at))}}</td>
					<td colspan="2" class="order-info">
						<a href="{{route('front.client.get.order.showDatails',$ord->id)}}" class="btn btn-primary" style="color: #fff;">{{ json_data($site_content,'orders_title8') }}</a>
					</td>
				</tr>
				@foreach($ord->content as $co)
	


				<tr>

					<td class="product-name">
						
						<a href="#" class="img_cart">
							<img alt="{{$co->product->name}}" src="{{getImage(PRODUCT_PATH.'small/'.$co->product->img)}}">
						</a>
						<h3><a href="{{route('front.get.product.show',[$co->product->category->slug,$co->product->sub->slug,$co->product->slug])}}" class="name_cart">{{$co->product->name}}</a></h3>
						
					</td>


					<td class="product-unit-price text-center">
						<p>  EGP {{ $co->product->price}}  </p>
					</td>
					<td class="product-quantity product-cart-details text-center">
						{{$co->quantity}}
					</td>
					<td class="product-quantity  text-center">
						<p>EGP {{ $co->product->price * $co->quantity}}</p>
					</td>
					<td class="product-quantity product-cart-details text-center product-status">
						{{$co->status}}
					</td>
				</tr>



				
				@endforeach

				
				<tr><td colspan="5"></td></tr>
				<tr><td colspan="5"></td></tr>

			@endforeach

				
						
			
			</tbody>
		</table>
		@else
			
			<div class="order-history">
				<p class="alert text-center">You have not placed any orders.</p>
			</div>

		@endif




		
	</div>
</div>
</div>