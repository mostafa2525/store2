@extends('front2.main')


@section('content')

<?php $cover = json_data($site_content,'profile_cover');  ?>

	<!-- BANNER AREA STRAT -->
		<section class="bannerhead-area">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner-heading">
							<h2> {{ json_data($site_content,'profile_title1') }} </h2>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- BANNER AREA END -->




		<!-- My Account Area Start -->
		<div class="my-account-area">
			<div class="container">
				<div class="row">
					<div class="addresses-lists">
						<div class="col-xs-3 col-sm-3 col-lg-3">
							<div class="left_menu_account">
								<h3> <i class="fa fa-user" aria-hidden="true"></i> {{ json_data($site_content,'profile_title2') }} </h3>
								<a href="{{route('front.client.get.profile.index','info')}}"  @if(Request::segment(4) =='info') class="active" @endif >{{ json_data($site_content,'profile_title3') }} </a>
								<a href="{{route('front.client.get.profile.index','edit-info')}}" @if(Request::segment(4) =='edit-info') class="active" @endif>{{ json_data($site_content,'profile_title4') }} </a>
								<a href="{{route('front.client.get.profile.index','change-password')}}" @if(Request::segment(4) =='change-password') class="active" @endif >{{ json_data($site_content,'profile_title5') }}</a>
								<a href="{{route('front.client.get.profile.index','my-orders')}}" @if(Request::segment(4) =='my-orders') class="active" @endif> {{ json_data($site_content,'profile_title6') }}</a>
								{{--
								<a href="{{route('front.client.get.profile.index','rating')}}" @if(Request::segment(4) =='rating') class="active" @endif >Rating</a>
								<a href="{{route('front.client.get.profile.index','wishlist')}}" @if(Request::segment(4) =='wishlist') class="active" @endif>Wishlist</a>--}}
								<a href="{{route('front.client.get.auth.logout')}}" class="exit_account">{{ json_data($site_content,'profile_title7') }}</a>


							</div>
						</div>
						<div class="col-xs-9 col-sm-9 col-lg-9">

							@if(Request::segment(4) =='info')
								@include('front2.client.inc.info')
							@endif

							@if(Request::segment(4) =='edit-info')
								@include('front2.client.inc.editInfo')
							@endif

							@if(Request::segment(4) =='change-password')
								@include('front2.client.inc.changePassword')
							@endif

							@if(Request::segment(4) =='my-orders')
								@include('front2.client.inc.orders')
							@endif

						</div>


					</div>
				</div>

			</div>
		</div>
		<!-- My Account Area End -->





		<!-- BEST SELLER START -->
		<section class="featured-area new-arrival">
			<div class="container">
				<div class="row">
					<div class="text-center">
						<div class="section-titel">
							<h3>{{ json_data($site_content,'profile_title13') }}</h3>
						</div>
					</div>
					<div class="newarrival-area">
						<div id="newarrival-curosel2" class="indicator-style">

							@foreach($latest as $fpro)
								<div class="col-md-12">
									<div class="single-product">
										<div class="product-image">
											<a class="product-img prod-home-img" href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}">
												<img class="primary-img" src="{{getImage(PRODUCT_PATH.$fpro->img)}}" alt="{{$fpro->name}}" height="250" />
												<img class="secondary-img" src="{{getImage(PRODUCT_PATH.$fpro->img)}}" alt="{{$fpro->name}}" height="250" />
											</a>
										</div>
										@if($fpro->offer)
										<span class="onsale red">
											<span class="sale-text"> {{$fpro->offer}} %</span>
										</span>
										@else
										<span class="onsale">
											<span class="sale-text"> @lang('frontSite.sale') </span>
										</span>
										@endif
										
										<div class="product-action">
											
											<h4><a href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}">{{\Str::Words($fpro->name,3,'..')}}</a></h4>
											<ul class="pro-rating">
												<li class="pro-ratcolor"><i class="fa fa-star"></i></li>
												<li class="pro-ratcolor"><i class="fa fa-star"></i></li>
												<li class="pro-ratcolor"><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
												<li><i class="fa fa-star"></i></li>
											</ul>
											<span class="price">EGP {{$fpro->price}}</span>
										</div>
										<div class="pro-action">
											<ul>

												<!-- <li>
													<a class="test all_src_icon" href="#" title="" data-toggle="tooltip" data-placement="top" data-original-title="Add Wishlist">
													<i class="fa fa-heart" aria-hidden="true"></i>
													</a>
												</li>
 												-->

												
												<li>
													<a class="test all_src_icon"  href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}" title="" 
													data-toggle="tooltip" data-placement="top" data-original-title="@lang('frontSite.show')">
													<i class="fa fa-eye" aria-hidden="true"></i>
													</a>
												</li>

												<li>
													<a class="test all_src_icon  pointer" href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}" 
													data-toggle="tooltip" data-placement="top" data-original-title="@lang('frontSite.addToCart')" >
													<i class="fa fa-shopping-cart" aria-hidden="true"></i>
													</a>
												</li>

												
											</ul>
										</div>
									</div>
								</div>
								@endforeach


					
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- BEST SELLER END -->







@endsection

@section('script')

<script type="text/javascript">
  
    $('#editInfo').parsley();

 </script>

<script type="text/javascript">
  
    $('#edit-password').parsley();


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#edit-password").submit(function(e) 
    {
        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#edit-password')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.client.post.auth.editPassword')}}",
           data:formData,
           contentType: false,
           processData: false,
           success:function(data)
           {
           	 if(data.errors)
           	 {
           	 	$("#errors").html('');
	            $("#errors").append("<li class='alert alert-danger text-center'>"+data.errors+"</li>")
           	 }
           	 else
           	 {

	             $("#errors").html('');
	             $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
           	 }

           	   $('#edit-password .input-text').find("input").val("");
	           $('#edit-password .input-message').find("textarea").val("");
           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
            }

        });

	});

</script>



@endsection


