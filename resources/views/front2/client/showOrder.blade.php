@extends('front2.main')


@section('content')


	<!-- BANNER AREA STRAT -->
		<section class="bannerhead-area">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner-heading">
							<h2>{{ json_data($site_content,'orders_title9') }}</h2>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- BANNER AREA END -->




		<!-- My Account Area Start -->
		<div class="my-account-area">
			<div class="container">
				<div class="row">
					<div class="addresses-lists">
				
						<div class="col-xs-12">

							@if($rowOrder)

								<table class="table table-bordered table-responsive">
									<thead>
										<tr>
											<tr>
												<th class="product-name">{{ json_data($site_content,'orders_title5') }}</th>
												<th class="product-unit-price">{{ json_data($site_content,'orders_title2') }}</th>
												<th class="product-quantity">{{ json_data($site_content,'orders_title3') }}</th>
												<th class="product-subtotal">{{ json_data($site_content,'orders_title4') }}</th>
												<th class="product-subtotal">{{ json_data($site_content,'orders_title5') }}</th>
											</tr>
										</tr>
									</thead>
									<tbody>

									
												
										<tr>
											<td colspan="2" class="order-info">{{ json_data($site_content,'orders_title6') }} : {{ $rowOrder->code}}</td>
											<td colspan="3" class="order-info">{{ json_data($site_content,'orders_title7') }} : {{ date('Y-m-D',strtotime($rowOrder->created_at))}}</td>
											
										</tr>
										@foreach($rowOrder->content as $co)
							


										<tr>

											<td class="product-name">
												
												<a href="#" class="img_cart">
													<img alt="{{$co->product->name}}" src="{{getImage(PRODUCT_PATH.'small/'.$co->product->img)}}">
												</a>
												<h3><a href="{{route('front.get.product.show',[$co->product->category->slug,$co->product->sub->slug,$co->product->slug])}}" class="name_cart">{{$co->product->name}}</a></h3>
												
											</td>


											<td class="product-unit-price text-center">
												<p>  EGP {{ $co->product->price}}  </p>
											</td>
											<td class="product-quantity product-cart-details text-center">
												{{$co->quantity}}
											</td>
											<td class="product-quantity  text-center">
												<p>EGP {{ $co->product->price * $co->quantity}}</p>
											</td>
											<td class="product-quantity product-cart-details text-center product-status">
												{{$co->status}}
											</td>
										</tr>



										
										@endforeach

										
										<tr><td colspan="5"></td></tr>
										<tr><td colspan="5"></td></tr>

								

										
												
									
									</tbody>
								</table>
								@else
									
									<div class="order-history">
										<p class="alert text-center">You have not placed any orders.</p>
									</div>

								@endif

						</div>



						<div class="col-sm-12">
							<div class="contDetails">
								<h4> <strong> {{ json_data($site_content,'orders_title10') }} : </strong>  {{$rowOrder->address}}</h4>
								<h4> <strong> {{ json_data($site_content,'orders_title11') }} : </strong>
								 @if($rowOrder->gov) {{$rowOrder->gov->name}} @endif
								</h4>
								<h4> <strong> {{ json_data($site_content,'orders_title12') }} : </strong>
								@if($rowOrder->status_change_date) 
									{{ date('Y-m-D',strtotime($rowOrder->status_change_date))}} 
								@endif
								</h4>
							</div>
							
						</div>


					</div>
				</div>

			</div>
		</div>
		<!-- My Account Area End -->












@endsection

@section('script')

<script type="text/javascript">
  
    $('#editInfo').parsley();

 </script>

<script type="text/javascript">
  
    $('#edit-password').parsley();


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#edit-password").submit(function(e) 
    {
        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#edit-password')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.client.post.auth.editPassword')}}",
           data:formData,
           contentType: false,
           processData: false,
           success:function(data)
           {
           	 if(data.errors)
           	 {
           	 	$("#errors").html('');
	            $("#errors").append("<li class='alert alert-danger text-center'>"+data.errors+"</li>")
           	 }
           	 else
           	 {

	             $("#errors").html('');
	             $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
           	 }

           	   $('#edit-password .input-text').find("input").val("");
	           $('#edit-password .input-message').find("textarea").val("");
           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
            }

        });

	});

</script>



@endsection


