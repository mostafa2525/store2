




			<!-- MENU AREA START -->
    <div class="menu-area">
			<div class="container">
				<div class="row">
					<div class="col-md-2 col-sm-2">
						<div class="logo-area">
							<a href="{{route('front.get.home.index')}}"><img src="{{ getImage(SETTINGS_PATH.$setting->logo) }}" alt="{{$setting->site_name}}"  /></a>
						</div>
					</div>
					<div class="col-md-6 col-sm-8">
						<div class="main-menu" style="    position: absolute;">
							<ul class="list-inline">
							<li class="{{active_link('')}}">
								<a href="{{route('front.get.home.index')}}">
									<i class="fa fa-home" aria-hidden="true"></i>{{json_data($site_content,'sectionLinks_home')}}
								</a>
							</li>

							 @foreach($categories as $cat)
							  <li class="{{active_link($cat->slug,3)}}" ><a href="{{route('front.get.product.category',[$cat->slug])}}">{{ $cat->name }} </a>
								<!-- Mega Menu Four Column -->
								<div class="mega-menu">

									@foreach($cat->sub() as $sub)
									<span>
										<a href="{{route('front.get.product.subCategory',[$cat->slug,$sub->slug])}}" class="mega-title">{{\Str::Words($sub->name,'3','')}}</a>
										@foreach($sub->productMenu() as $prod)
										<a href="{{route('front.get.product.show',[$cat->slug,$sub->slug,$prod->slug])}}">
											{{\Str::Words($prod->name,'3','')}}
										</a>
										@endforeach
									</span>
									@endforeach
									<span class="mega-menu-img">
										<a href="{{route('front.get.product.category',[$cat->slug])}}">
											<img alt="{{$cat->name}}" src="{{getImage(CATEGORY_PATH.$cat->img)}}">
										</a>
									</span>
								</div>
							  </li>
								@endforeach
							  {{--
												<li class="{{active_link('about')}}"><a href="{{route('front.get.home.about')}}">{{json_data($site_content,'sectionLinks_about')}}</a></li>
							  					<li class="{{active_link('contact-us')}}"><a href="{{route('front.get.contactus.contact')}}">{{json_data($site_content,'sectionLinks_contactUs')}}</a></li>--}}
							</ul>
						</div>
					</div>
					<div class="col-md-4 col-sm-2">
						<div class="input-group input-group-lg search_home">
							<form method="GET" action="{{route('front.get.product.search')}}" class="search-box">
								<input type="text" class="form-control box_search text_left_search" placeholder="Search Products" name="search">
							</form>
						</div>
					</div>
					<!-- MOBILE-MENU-AREA START --> 
					<div class="mobile-menu-area">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
								<div class="mobile-area">
									<div class="mobile-menu">
										<nav id="mobile-nav">
											<ul>
												<li class="{{active_link('')}}">
													<a href="{{route('front.get.home.index')}}">{{json_data($site_content,'sectionLinks_home')}}</a>
												</li>
												@foreach($categories as $cat)
												<li><a href="{{route('front.get.product.category',[$cat->slug])}}">{{ $cat->name }} </a>
													<ul>
														@foreach($cat->sub() as $sub)
														<a href="{{route('front.get.product.subCategory',[$cat->slug,$sub->slug])}}" class="mega-title">{{\Str::Words($sub->name,'3','')}}</a>
														@endforeach
														
													</ul>
												</li>
												@endforeach
												
											</ul>
										</nav>
									</div>	
									</div>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
	</div>
	<!-- MENU AREA END -->