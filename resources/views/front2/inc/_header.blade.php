</head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- HEADER TOP AREA START -->
		<section class="header-top-area">
			<div class="container">
				<div class="row">


					<div class="col-md-2 col-sm-2">
						<div class="menu-right-area">
							<ul>
								<li>
									<a href="{{route('front.get.cart.show')}}" class="cart-toggler mini-cart-icon" id="qCart">
										<i class="fa fa-shopping-cart"></i>
										<label style="font-weight: bold; font-size: 16px;">{{ json_data($site_content,'topBar_title1') }}</label>
										<span>{{\Cart::getContent()->count()}}</span>
									</a>
									<div class="top-cart-content">

										<div id="cart-content">
												@if(\Cart::getTotalQuantity())
												
												@foreach(\Cart::getContent() as $cart)
												<div class="media header-middle-checkout ">
													<div class="media-left check-img">
														<a href="{{route('front.get.product.show',[$cart->attributes->catSlug,$cart->attributes->subCatslug,$cart->attributes->slug])}}">
															<img alt="{{$cart->name}}" src="{{getImage(PRODUCT_PATH.$cart->attributes->img)}}" style="width: 70px; height: 70px;">
														</a>
													</div>
													<div class="media-body checkout-content">
														<h4 class="media-heading">
															<a href="{{route('front.get.product.show',[$cart->attributes->catSlug,$cart->attributes->subCatslug,$cart->attributes->slug])}}">{{\Str::Words($cart->name,2,'')}}</a>
															<!-- <span title="Remove This Item" class="btn-remove checkout-remove itemcartRemove" data-CartId="{{--$cart->id--}}" > 
																<i class="fa fa-trash"></i>
															</span> -->
														</h4>
														<p>EGP {{$cart->price}}</p>
													</div>
												</div>
												@endforeach
												
											@endif
										</div>
										<div class="actions">
											<button type="button" title="Checkout" class="Checkout-botton" style="width: 100%;">
												<span> <a href="{{route('front.get.cart.show')}}" style="color: #fff; "> {{ json_data($site_content,'topBar_title5') }} </a></span>
											</button>
										</div>
									</div>
								</li>

							</ul>
						</div>
					</div>

					<div class="col-md-4">
					<div class="left-msg pull-left">
					<h6>{{ json_data($site_content,'topBar_title2') }}</h6>
					</div>
					</div>

					<div class="col-md-6">
						<div class="right-login pull-right">
							<div class="currencies-block-top curency pull-left">
								<div class="current">
									<span> <i class="fa fa-user" aria-hidden="true"></i>
										@if(!clientAuth()->user())
									 		{{ json_data($site_content,'topBar_title3') }} 
									 	@else
									 		@lang('frontSite.welcome') {{\Str::Words(clientAuth()->user()->name,1,' ')}}
									 	@endif
									</span>
									<i class="fa fa-angle-down"></i>
								</div>
								<ul class="first-currencies toggle-content">
									@if(!clientAuth()->user())
									<li><a href="{{route('front.client.get.auth.login')}}">@lang('frontSite.login')</a></li>
									<li><a href="{{route('front.client.get.auth.register')}}">@lang('frontSite.register')</a></li>

									@else
									<li><a href="{{route('front.client.get.profile.index','info')}}">@lang('frontSite.myAccount')</a></li>
									<li><a href="{{route('front.client.get.profile.index','my-orders')}}">
									@lang('frontSite.myOrders')</a></li>
									{{--<li><a href="{{route('front.client.get.profile.index','wishlist')}}">
									My Wishlist</a></li>--}}
									<li class="logout-btn"><a href="{{route('front.client.get.auth.logout')}}"> 
									@lang('frontSite.logout') </a></li>
									@endif
								</ul>
							</div>
							<div class="languages-block-top curency pull-left">
								<div class="current">
									<span>   <img src="{{furl()}}/img/egypt.png" width="18">  {{ json_data($site_content,'topBar_title4') }} </span>
									<i class="fa fa-angle-down"></i>
								</div>
								<ul class="first-languages toggle-content">
									<li><a href="{{url('/ar')}}"> <img src="{{furl()}}/img/egypt.png" width="18"> عربى </a></li>
									<li><a href="{{url('/en')}}"> <img src="{{furl()}}/img/en.png" width="18"> ENGLISH</a></li>

								</ul>
							</div>

						</div>
					</div>
				</div>
			</div>
		</section>
        <!-- HEADER TOP AREA END -->