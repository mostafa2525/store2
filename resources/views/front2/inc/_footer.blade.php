



		@if(count($brands))
		<!-- OUR BRAND AREA START -->
		<section class="our-brand-area">
			<div class="container">
				<div class="text-center">
					<div class="section-titel">
						<h3>{{ json_data($site_content,'section7_head') }}</h3>
					</div>
				</div>
				<div class="row blog-area">
					<div id="ourbrand-owl">
						@foreach($brands as $brand)
						<div class="col-md-12"><img src="{{getImage(BRAND_PATH.$brand->img)}}" alt="{{$brand->name}}" /></div>
						@endforeach
						
					</div>
				</div>
			</div>
		</section>
		@endif
		<!-- OUR BRAND AREA END -->
		<!-- Footer Top Area Start -->
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-md-3 col-sm-3">
						<div class="social-icon">
							<ul>
								@if($setting->facebook)
		                        <li><a href="{{$setting->facebook}}"  target="_blank"><i class="fa fa-facebook-f fa-3x" ></i></a></li>
		                        @endif
		                         @if($setting->twitter)
		                        <li><a href="{{$setting->twitter}}" target="_blank"><i class="fa fa-twitter fa-3x"></i></a></li>
		                        @endif
		                         @if($setting->instagram)
		                        <li><a href="{{$setting->instagram}}" target="_blank"><i class="fa fa-instagram fa-3x"></i></a></li>
		                        @endif
		                         @if($setting->linkedin)
		                        <li><a href="{{$setting->linkedin}}" target="_blank"><i class="fa fa-linkedin fa-3x"></i></a></li>
		                        @endif
							</ul>
						</div>
					</div>
					<div class="col-md-9 col-sm-9">
						<div class="foteer-news">
							<h3>{{ json_data($site_content,'section8_head') }}</h3>
							<p>{{ json_data($site_content,'section8_desc') }}</p>
						</div>
						<div class="search-input">
							<form action="#" id="subscribe-form">
								<input type="email"  name="email" class="input-sub"  placeholder=" {{json_data($site_content,'section8_placeholder')}} " required>
								<button class="search-button" type="submit" value="Submit">{{json_data($site_content,'section8_btn')}}</button>
								@csrf
							</form>
							<div class="" style="margin-top:70px;">
		                        <ul id="errors-footer"></ul>
		                    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Footer Top Area End -->

		<!-- Footer Middle Area Start -->
		<section class="footer-middle">
			<div class="container">
				<div class="row">
				<!-- 	<div class="col-md-3 col-sm-4 col-xs-12 xs-res-mrbtm">
						<div class="footer-menu">
							<h4>MY ACCOUNT</h4>
							<ul>
								<li><a href="#">Login</a></li>
								<li><a href="#">My Account</a></li>
								<li><a href="#">My Cart</a></li>
								<li><a href="#">Wishlist</a></li>
								<li><a href="#">Checkout</a></li>
								<li><a href="#">Userinfor</a></li>
							</ul>
						</div>
					</div> -->
					<div class="col-md-4 hidden-sm hidden-xs">
						<div class="footer-menu">
							<h4> @lang('frontSite.support')  </h4>
							<ul>

								@foreach($staticPagesSupport as $pageSupport)
								<li>
									<a href="{{route('front.get.static.page',$pageSupport->slug)}}">{{$pageSupport->name}}
									</a>
								</li>
								@endforeach
								<li>
									<a href="{{route('front.get.contactus.contact')}}"> @lang('frontSite.contactUs') </a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12 xs-res-mrbtm">
						<div class="footer-menu">
							<h4> @lang('frontSite.about')  </h4>
							<ul>
								@foreach($staticPagesAbout as $pageAbout)
								<li>
									<a href="{{route('front.get.static.page',$pageAbout->slug)}}">{{$pageAbout->name}}
									</a>
								</li>
								@endforeach
								<li>
									<a href="{{route('front.get.home.about')}}">@lang('frontSite.aboutUs') </a>
								</li>

								
							</ul>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<div class="footer-menu">
							<h4> @lang('frontSite.contactUs')  </h4>
							<div class="address-area">
								<div class="contact-details">
									<ul>
										<li><i class="fa fa-phone"></i>{{$setting->mobile}}</li>
										<li><i class="fa fa-envelope-o"></i>{{$setting->email}}</li>
										<li><i class="fa fa-map-marker"></i> {{$setting->address}}</li>
									</ul>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>
		<!-- Footer Middle Area End -->

		<!-- Footer Bottom Area Start -->
		<div class="footer-bottom-area">
			<div class="container">
				<div class="row ">
					<div class="col-md-12 col-xs-12">
						<p class="text-center"> @lang('frontSite.copyright')   &copy;  <a href="https://eraasoft.com/"> EraaSoft. </a> @lang('frontSite.rightReserved')  </p>
					</div>
					
				</div>
			</div>
		</div>
		<!-- Footer Bottom Area End -->
