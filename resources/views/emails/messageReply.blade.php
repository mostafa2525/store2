
<style type="text/css">
	
</style>

<div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px; ">
    <h1 style="text-align: center;"></h1>
    <hr style="display: block;border: 2px solid #222">
    
    <div style="padding: 30px; text-align: center; ">
        <h2>{{ $title }}</h2>
        
				<hr>
				<p>{{ $body }}</p>
				<hr>
        
        Thanks,<br>
        {{ config('app.name') }}

    </div>
</div>
