<style type="text/css">
	
</style>

<div style="border: 2px solid #ccc; border-radius: 5px; padding: 20px; ">
    <h1 style="text-align: center;">@lang('frontSite.resetPassword')</h1>
    <hr style="display: block;border: 2px solid #222">

    <div style="padding: 30px; text-align: center; ">

    	<a href="{{url('/en/customer/change/password/'.$token)}}" target="_blank" style="padding: 15px; background: #00B2B2; color: #fff; text-decoration: none; border-radius: 3px; box-shadow: 3px 3px 10px #ccc;">@lang('frontSite.clickHere')</a>
    </div>
</div>