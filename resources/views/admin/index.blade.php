@extends('admin.main')



@section('content')



<!-- BEGIN PAGE BAR -->
<div class="page-bar">


    <ul class="page-breadcrumb">

    </ul>

</div>
<!-- END PAGE BAR -->



<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> @lang('site.homePage')  </h1>
<!-- END PAGE TITLE-->



<!-- END PAGE HEADER-->
<div class="note note-info">
   <!-- END PAGE HEADER-->
                        <div class="row widget-row">



                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered" style="    padding: 14px;">
                                    <h4 class="widget-thumb-heading">@lang('site.addNewOrder')</h4>
                                    <div class="widget-thumb-wrap">
                                        <a href="{{route('admin.get.order.add') }}" />
                                        <i class="widget-thumb-icon bg-green icon-plus"></i>
                                        
                                        </a>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>


                             <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered" style="    padding: 14px;">
                                    <h4 class="widget-thumb-heading">@lang('site.totalVisitors')</h4>
                                    <div class="widget-thumb-wrap">
                                        
                                        <i class="widget-thumb-icon bg-green fa fa-eye"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-body-stat" >{{$totalVisitors}} </span>
                                        </div>
                                       
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>






                             <div class="col-md-6">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading">@lang('site.searchAboutOrder')</h4>
                                    <div class="widget-thumb-wrap">
                                        <div class="form-group">
                                            <form method="post" action="{{route('admin.post.order.search')}}">
                                                @csrf
                                                <div class="input-group">
                                                    <span class="input-group-addon input-circle-left">
                                                        <i class="fa fa-text-width"></i>
                                                    </span>
                                                    <input type="text" name="code" class="form-control input-circle-right "  required > 
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>





                             <div class="col-sm-12">
                                <hr><hr>
                            </div>

                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading">@lang('site.products')</h4>
                                    <div class="widget-thumb-wrap">
                                        <a href="{{route('admin.get.product.index')}}">
                                        <i class="widget-thumb-icon bg-green icon-bulb"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-body-stat" >{{$totalProducts}} </span>
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>


                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading"> @lang('site.totalOrders') </h4>
                                    <div class="widget-thumb-wrap">
                                        <i class="widget-thumb-icon bg-red fa fa-sort"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-body-stat" >{{$totalOrders}}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>




                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading"> @lang('site.todayOrders') </h4>
                                    <div class="widget-thumb-wrap">
                                        <i class="widget-thumb-icon bg-yellow fa fa-sort"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-body-stat" >{{$todayOrders}}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>




                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading"> @lang('site.weekOrders') </h4>
                                    <div class="widget-thumb-wrap">
                                        <i class="widget-thumb-icon bg-green fa fa-sort"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-body-stat" >{{$weekOrders}}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>






                            
                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading">@lang('site.pendingOrders')</h4>
                                    <div class="widget-thumb-wrap">
                                        <a href="{{ route('admin.get.order.pending.index') }}">
                                        <i class="widget-thumb-icon bg-blue fa fa-pause"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-subtitle">@lang('site.pending')</span>
                                            <span class="widget-thumb-body-stat" >{{$pendingOrders}}</span>
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>



                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading">@lang('site.shippingOrders')</h4>
                                    <div class="widget-thumb-wrap">
                                        <a href="{{ route('admin.get.order.shipping.index') }}">
                                        <i class="widget-thumb-icon bg-blue fa fa-truck"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-subtitle">@lang('site.shipping')</span>
                                            <span class="widget-thumb-body-stat" >{{$shippingOrders}}</span>
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>




                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading">@lang('site.acceptedOrders')</h4>
                                    <div class="widget-thumb-wrap">
                                        <a href="{{ route('admin.get.order.accepted.index') }}">
                                        <i class="widget-thumb-icon bg-blue fa fa-check"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-subtitle">@lang('site.accepted')</span>
                                            <span class="widget-thumb-body-stat" >{{$acceptedOrders}}</span>
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>


                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading">@lang('site.refusedOrders')</h4>
                                    <div class="widget-thumb-wrap">
                                        <a href="{{ route('admin.get.order.refused.index') }}">
                                        <i class="widget-thumb-icon bg-blue fa fa-times"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-subtitle">@lang('site.refused')</span>
                                            <span class="widget-thumb-body-stat" >{{$refusedOrders}}</span>
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>



                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading"> @lang('site.canceledOrders') </h4>
                                    <div class="widget-thumb-wrap">
                                        <a href="{{ route('admin.get.order.canceled.index') }}">
                                        <i class="widget-thumb-icon bg-purple fa fa-ban"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-subtitle">@lang('site.canceled')</span>
                                            <span class="widget-thumb-body-stat" >{{$canceledOrders}}</span>
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>

                            <div class="col-sm-12">
                                <hr><hr>
                            </div>

                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading"> @lang('site.totalClients') </h4>
                                    <div class="widget-thumb-wrap">
                                        <a href="{{route('admin.get.client.index')}}">
                                        <i class="widget-thumb-icon bg-red fa fa-users"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-body-stat" >{{$totalClients}}</span>
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>


                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading"> @lang('site.totalAdmins') </h4>
                                    <div class="widget-thumb-wrap">
                                        <a href="{{ route('admin.get.admin.index') }}">
                                        <i class="widget-thumb-icon bg-purple fa fa-user-secret"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-body-stat" >{{$totalAdmins}}</span>
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>





                        </div>
</div>



@endsection



