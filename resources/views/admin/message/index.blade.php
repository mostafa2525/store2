@extends('admin.main')

@section('style')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{aurl()}}/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{aurl()}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')



<!-- BEGIN PAGE BAR -->
<div class="page-bar">


    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('admin.get.home.index') }}" class="active-bread">@lang('site.home')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>@lang('site.messages')</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->



<!-- END PAGE MESSAGE -->
@include('admin.msg._messages')

<!-- END PAGE HEADER-->
<div class="note note-info">
    <h3> @lang('site.view') ( @lang('site.messages') ) </h3>
</div>


    

<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
       
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                           
                            </div>
                        </div>
                   
                    </div>
                </div>




                @if($messages->count())
                @if(checkPermission('messages-delete'))                
               <form action="{{ route('admin.post.message.deleteMulti') }}" method="post" id="Form2">@csrf</form>
                @endif


                        <table class="table table-striped table-bordered table-hover table-checkable table-sort order-column column" id="sample_1">
                        <thead>
                            <tr>
                                @if(checkPermission('messages-delete'))                
                                <th>
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                @endif

                                <th> @lang('site.name') </th>
                                <th> @lang('site.email') </th>
                                <th> @lang('site.message') </th>

                                @if(checkPermission('messages-delete'))                
                                <th> @lang('site.actions') </th>
                                @endif
                            </tr>
                        </thead>
                        <tbody class="connected-sortable">
                            
                            @foreach($messages as $msg)
                            <tr class="odd gradeX ">
                                @if(checkPermission('messages-delete'))                
                                <td class="text-center">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="checkboxes" name="deleteMulti[]" form="Form2" multiple value="{{ $msg->id}}" />
                                        <span></span>
                                    </label>
                                </td>
                                @endif
                                <td class="text-center"> {{ $msg->name }} </td>
                                <td class="text-center"> {{ $msg->email }} </td>
                                <td class="text-center"> {{ $msg->message }} </td>

                                @if(checkPermission('messages-delete'))                
                                <td class="text-center">
                                    <a href="{{ route('admin.get.message.delete',$msg->id) }}" class="conform-delete btn btn-sm btn-danger">
                                        <i class="fa fa-close"></i> 
                                    </a>
                                    <a href="{{ route('admin.get.message.reply', $msg->id) }}"  class="btn btn-sm btn-success">
                                        <i class="fa fa-reply"></i> 
                                    </a>
                                </td>
                                @endif

                            </tr>
                            @endforeach

                        </tbody>
                        <hr>
                        
                    
                    </table>
                            
                
                        </form>
                    @if(checkPermission('messages-delete'))                
                    <button type="submit" class="btn btn-danger btn-sm item-checked" form="Form2">@lang('site.deleteChecked')</button>
                    @endif
              
            <!-- end form  -->

                @else
                        
                        @include('admin.msg.notFound')
                
                @endif



            </div>

    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>



@endsection



@section('script')

   

<script src="{{aurl()}}/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{aurl()}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<script type="text/javascript" src="{{aurl()}}/seoera/js/sortAndDataTable.js"></script>


@endsection