@extends('admin.main')



@section('content')

    

<!-- BEGIN PAGE BAR -->
<div class="page-bar">


    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('admin.get.home.index') }}" class="active-bread">@lang('site.home')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('admin.get.message.index') }}" class="active-bread">@lang('site.messages')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>@lang('site.reply')</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->





<!-- END PAGE HEADER-->
<div class="note note-info">
    <h3> @lang('site.reply') ( @lang('site.message') ) </h3>
</div>


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">


                {{-- <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> @lang('site.addItem') </span>
                    </div>
                </div> --}}

                <div class="portlet-body form">
                    <form role="form" method="post" action="{{ route('admin.post.message.sendReply') }}" enctype="multipart/form-data" class="">

                        <div class="form-body row">
                           @csrf
                            @include('admin.msg._errors')

                            <input type="hidden" name="id" value="{{ $id }}">

                           <div class="col-sm-12">
                               <div class="form-group">
                                    <label> @lang('site.title') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" name="title" class="form-control input-circle-right " placeholder="Title" required value="{{ old('title') }}"> </div>
                                </div>
                           </div>

                           <div class="col-md-12">
                              <div class="form-group">
                                  <label> @lang('site.body')  </label>
                                  <div class="input-group">
                                      <span class="input-group-addon input-circle-left">
                                          <i class="fa fa-text-width"></i>
                                      </span>
                                      <textarea name="body" class="form-control input-circle-right" placeholder="Body" required rows="10" >{{ old('body') }}</textarea>
                                       </div>
                              </div>
                          </div>

                        </div>

                        
                        <div class="form-actions">
                            <button type="submit" class="btn blue">@lang('site.send')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection
