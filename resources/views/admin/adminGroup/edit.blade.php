@extends('admin.main')



@section('content')


<!-- BEGIN PAGE BAR -->
<div class="page-bar">


    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('admin.get.home.index') }}" class="active-bread">@lang('site.home')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('admin.get.adminGroup.index') }}" class="active-bread">@lang('site2.adminGroup')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>@lang('site.edit')</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->





<!-- END PAGE HEADER-->
<div class="note note-info">
    <h3> @lang('site.editItem') ( @lang('site2.adminGroup') ) </h3>
</div>


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> @lang('site.editItem') </span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" method="post" action="{{ route('admin.put.adminGroup.update') }}"  enctype="multipart/form-data">
                        <div class="form-body row">
                           @csrf
                           {{ method_field('PUT') }}
                            @include('admin.msg._errors')

                           <div class="col-sm-6">
                                <div class="form-group">
                                    <label> @lang('site.name') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" name="name" class="form-control input-circle-right" placeholder="Name" required value="{{ $row->name }}"> 
                                        <input type="hidden" name="id" value="{{ $row->id }}"  >
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label> @lang('site.desc') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <textarea class="form-control count-text-desc-keywords" name="desc" rows="4" required> {{ strip_tags($row->desc) }} </textarea>
                                        <input type="hidden" name="id" value="{{ $row->id }}"  >
                                    </div>
                                </div>
                            </div>

                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <th>Module</th>
                                    <th>Show</th>
                                    <th>Add</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                @foreach ($crud_modules as $module)       
                                    <tr>
                                        <td class="text-center">{{ $module->name }}</td>
                                        <input type="hidden" name="{{ $module->name }}-show">                                        
                                        <td class="text-center"><input type="checkbox" name="{{ $module->name }}-show" value="yes" {{ json_decode($row->permissions, true)[$module->name.'-show'] == 'yes'?'checked':''}}></td>
                                        
                                        @if(!in_array($module->name, ['messages', 'newsletter', 'clients']))                                       
                                        <input type="hidden" name="{{ $module->name }}-add">
                                        <td class="text-center"><input type="checkbox" name="{{ $module->name }}-add" value="yes" {{ json_decode($row->permissions, true)[$module->name.'-add'] == 'yes'?'checked':''}}></td>                                       
                                        @endif
                                        
                                        @if(!in_array($module->name, ['messages', 'newsletter']))
                                        <input type="hidden" name="{{ $module->name }}-edit">                                        
                                        <td class="text-center"><input type="checkbox" name="{{ $module->name }}-edit" value="yes" {{ json_decode($row->permissions, true)[$module->name.'-edit'] == 'yes'?'checked':''}}></td>
                                        @endif

                                        <input type="hidden" name="{{ $module->name }}-delete">                                        
                                        <td class="text-center"><input type="checkbox" name="{{ $module->name }}-delete" value="yes" {{ json_decode($row->permissions, true)[$module->name.'-delete'] == 'yes'?'checked':''}}></td>
                                    </tr>
                                @endforeach
                            </table>


                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <th>Order Type</th>
                                    <th>Show</th>
                                    <th>Add to</th>
                                    <th>Delete</th>
                                </tr>
                                @foreach($order_types as $key => $type)
                                <tr>
                                    <td class="text-center">{{ $type }}</th>

                                    <input type="hidden" name="orders-show-{{ $type }}">                                        
                                    <td class="text-center"><input type="checkbox" id="show-order-{{$key}}" class="show-order" name="orders-show-{{ $type }}" value="yes" {{ json_decode($row->permissions, true)['orders-show-'.$type] == 'yes'?'checked':''}}></td>

                                    <input type="hidden" name="orders-add-{{ $type }}">                                        
                                    <td class="text-center"><input type="checkbox" id="add-order-{{$key}}" class="add-order" name="orders-add-{{ $type }}" value="yes" {{ json_decode($row->permissions, true)['orders-add-'.$type] == 'yes'?'checked':''}} onclick="checkShow()"></td>
                                    
                                    
                                    <input type="hidden" name="orders-delete-{{ $type }}">                                        
                                    <td class="text-center"><input type="checkbox" id="delete-order-{{$key}}" class="delete-order" name="orders-delete-{{ $type }}" value="yes" {{ json_decode($row->permissions, true)['orders-delete-'.$type] == 'yes'?'checked':''}} onclick="checkShow()"></td>
                                </tr>
                                @endforeach
                            </table>


                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <th>Settings</th>
                                    <td>Base Data</td>
                                    <td>Seo Data</td>
                                    <td>Site Content</td>
                                    <td>About Page</td>
                                    <td>Collections</td>
                                </tr>
                                <tr>
                                    <th class="text-center"></th>
                                    <input type="hidden" name="baseData-edit">                                        
                                    <td class="text-center"><input type="checkbox" name="baseData-edit" value="yes" {{ json_decode($row->permissions, true)['baseData-edit'] == 'yes'?'checked':''}}></td>
                                    
                                    <input type="hidden" name="seoData-edit">                                        
                                    <td class="text-center"><input type="checkbox" name="seoData-edit" value="yes" {{ json_decode($row->permissions, true)['seoData-edit'] == 'yes'?'checked':''}}></td>
                                    
                                    <input type="hidden" name="siteContent-edit">                                        
                                    <td class="text-center"><input type="checkbox" name="siteContent-edit" value="yes" {{ json_decode($row->permissions, true)['siteContent-edit'] == 'yes'?'checked':''}}></td>
                                    
                                    <input type="hidden" name="aboutUs-edit">                                        
                                    <td class="text-center"><input type="checkbox" name="aboutUs-edit" value="yes" {{ json_decode($row->permissions, true)['aboutUs-edit'] == 'yes'?'checked':''}}></td>
                                    
                                    <input type="hidden" name="collections-edit">                                        
                                    <td class="text-center"><input type="checkbox" name="collections-edit" value="yes" {{ json_decode($row->permissions, true)['collections-edit'] == 'yes'?'checked':''}}></td>
                                </tr>
                            </table>


                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn red">@lang('site.save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



    

@endsection



@section('script')

<script>

$('.add-order').each(function(){
    $(this).click(function(){
        if($(this).prop('checked')){
            var id = $(this).attr('id');
            var num = id.slice(-1);
            $('#show-order-'+ num).prop('checked', true);
        }

        if($(this).attr('name') == 'orders-add-shipping'){
            $("input[name='orders-show-pending']").prop('checked', true);
        }

        if($(this).attr('name') == 'orders-add-canceled'){
            $("input[name='orders-show-pending']").prop('checked', true);
        }

        if($(this).attr('name') == 'orders-add-accepted'){
            $("input[name='orders-show-shipping']").prop('checked', true);
        }

        if($(this).attr('name') == 'orders-add-refused'){
            $("input[name='orders-show-shipping']").prop('checked', true);
        }
    })
})

$('.delete-order').each(function(){
    $(this).click(function(){
        if($(this).prop('checked')){
            var id = $(this).attr('id');
            var num = id.slice(-1);
            $('#show-order-'+ num).prop('checked', true);
        }
    })
})

</script>

@endsection
