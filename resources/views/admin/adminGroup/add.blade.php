@extends('admin.main')



@section('content')

    

<!-- BEGIN PAGE BAR -->
<div class="page-bar">


    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('admin.get.home.index') }}" class="active-bread">@lang('site.home')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('admin.get.adminGroup.index') }}" class="active-bread">@lang('site2.adminGroup')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>@lang('site.add')</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->





<!-- END PAGE HEADER-->
<div class="note note-info">
    <h3> @lang('site.addItem') ( @lang('site2.adminGroup') ) </h3>
</div>


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">


                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> @lang('site.addItem') </span>
                    </div>
                </div>

                <div class="portlet-body form">
                    <form role="form" method="post" action="{{ route('admin.post.adminGroup.store') }}" enctype="multipart/form-data" class="">

                        <div class="form-body row">
                           @csrf
                            @include('admin.msg._errors')
                           <div class="col-sm-6">
                               <div class="form-group">
                                    <label> @lang('site.name') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" name="name" class="form-control input-circle-right " placeholder="Name" required value="{{ old('name') }}"> </div>
                                </div>
                           </div>

                           <div class="col-sm-12">
                                <div class="form-group">
                                     <label> @lang('site.desc') </label>
                                    <div class="input-group">
                                         <span class="input-group-addon input-circle-left">
                                             <i class="fa fa-text-width"></i>
                                         </span>
                                         <textarea class="form-control count-text-desc-keywords" name="desc" rows="4" required> {{ old('desc') }} </textarea>
                                 </div>
                            </div>

                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <th>Module</th>
                                    <th>Show</th>
                                    <th>Add</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                @foreach ($crud_modules as $module)       
                                    <tr>
                                        <td class="text-center">{{ $module->name }}</td>
                                        <input type="hidden" name="{{ $module->name }}-show">                                        
                                        <td class="text-center"><input type="checkbox" name="{{ $module->name }}-show" value="yes" {{ old($module->name.'-show')?'checked':''}}></td>
                                       
                                        @if(!in_array($module->name, ['messages', 'newsletter', 'clients']))
                                            <input type="hidden" name="{{ $module->name }}-add">
                                            <td class="text-center"><input type="checkbox" name="{{ $module->name }}-add" value="yes" {{ old($module->name.'-add')?'checked':''}}></td>
                                        @endif

                                        @if(!in_array($module->name, ['messages', 'newsletter']))
                                            <input type="hidden" name="{{ $module->name }}-edit">                                        
                                            <td class="text-center"><input type="checkbox" name="{{ $module->name }}-edit" value="yes" {{ old($module->name.'-edit')?'checked':''}}></td>
                                        @endif

                                        <input type="hidden" name="{{ $module->name }}-delete">                                        
                                        <td class="text-center"><input type="checkbox" name="{{ $module->name }}-delete" value="yes" {{ old($module->name.'-delete')?'checked':''}}></td>
                                    </tr>
                                @endforeach
                            </table>

                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <th>Order Type</th>
                                    <td>Show</td>
                                    <td>Add to</td>
                                    <td>Delete</td>
                                </tr>
                                @foreach($order_types as $key => $type)
                                <tr>
                                    <th class="text-center">{{ $type }}</th>

                                    <input type="hidden" name="orders-show-{{ $type }}">                                        
                                    <td class="text-center"><input type="checkbox" id="show-order-{{$key}}" class="show-order" name="orders-show-{{ $type }}" value="yes" {{ old('orders-show-'.$type)?'checked':''}}></td>

                                    <input type="hidden" name="orders-add-{{ $type }}">                                        
                                    <td class="text-center"><input type="checkbox"  id="add-order-{{$key}}" class="add-order" name="orders-add-{{ $type }}" value="yes" {{ old('orders-add-'.$type)?'checked':''}} onclick="checkShow()"></td>
                                    
                                    
                                    <input type="hidden" name="orders-delete-{{ $type }}">                                        
                                    <td class="text-center"><input type="checkbox" id="delete-order-{{$key}}" class="delete-order" name="orders-delete-{{ $type }}" value="yes" {{ old('orders-delete-'.$type)?'checked':''}} onclick="checkShow()"></td>
                                </tr>
                                @endforeach
                            </table>


                            <table class="table table-striped table-bordered table-hover">
                                <tr>
                                    <th>Settings</th>
                                    <th>Base Data</th>
                                    <th>Seo Data</th>
                                    <th>Site Content</th>
                                    <th>About Page</th>
                                    <th>Collections</th>
                                </tr>
                                <tr>
                                    <th class="text-center"></th>
                                    <input type="hidden" name="baseData-edit">                                        
                                    <td class="text-center"><input type="checkbox" name="baseData-edit" value="yes" {{ old('baseData-edit')?'checked':''}}></td>
                                    
                                    <input type="hidden" name="seoData-edit">                                        
                                    <td class="text-center"><input type="checkbox" name="seoData-edit" value="yes" {{ old('seoData-edit')?'checked':''}}></td>
                                    
                                    <input type="hidden" name="siteContent-edit">                                        
                                    <td class="text-center"><input type="checkbox" name="siteContent-edit" value="yes" {{ old('siteContent-edit')?'checked':''}}></td>
                                    
                                    <input type="hidden" name="aboutUs-edit">                                        
                                    <td class="text-center"><input type="checkbox" name="aboutUs-edit" value="yes" {{ old('aboutUs-edit')?'checked':''}}></td>
                                    
                                    <input type="hidden" name="collections-edit">                                        
                                    <td class="text-center"><input type="checkbox" name="collections-edit" value="yes" {{ old('collections-edit')?'checked':''}}></td>
                                </tr>
                            </table>


                        
                        <div class="form-actions">
                            <button type="submit" class="btn blue">@lang('site.add')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection


@section('script')

<script>

$('.add-order').each(function(){
    $(this).click(function(){
        if($(this).prop('checked')){
            var id = $(this).attr('id');
            var num = id.slice(-1);
            $('#show-order-'+ num).prop('checked', true);
        }

        if($(this).attr('name') == 'orders-add-shipping'){
            $("input[name='orders-show-pending']").prop('checked', true);
        }

        if($(this).attr('name') == 'orders-add-canceled'){
            $("input[name='orders-show-pending']").prop('checked', true);
        }

        if($(this).attr('name') == 'orders-add-accepted'){
            $("input[name='orders-show-shipping']").prop('checked', true);
        }

        if($(this).attr('name') == 'orders-add-refused'){
            $("input[name='orders-show-shipping']").prop('checked', true);
        }
    })
})

$('.delete-order').each(function(){
    $(this).click(function(){
        if($(this).prop('checked')){
            var id = $(this).attr('id');
            var num = id.slice(-1);
            $('#show-order-'+ num).prop('checked', true);
        }
    })
})



</script>

@endsection