@extends('admin.main')

@section('style')

  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.min.css">
  <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{ aurl() }}/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ aurl() }}/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->

@endsection


@section('content')
  

<!-- BEGIN PAGE BAR -->
<div class="page-bar">


    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('admin.get.home.index') }}" class="active-bread">@lang('site.home')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('admin.get.order.pending.index') }}" class="active-bread">@lang('site.orders')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>@lang('site.add')</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->





<!-- END PAGE HEADER-->
<div class="note note-info">
    <h3> @lang('site.addItem') ( @lang('site.order') ) </h3>
</div>


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">


                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> @lang('site.addItem') </span>
                    </div>
                </div>

                <div class="portlet-body form">
                    
                    <div class="coverLoading" style="width: 100%; display: none;"><img src="{{furl()}}/img/loading.gif" style="display: block; margin: auto;"></div>

                    <form role="form" method="post" action="{{ route('admin.post.order.store') }}" enctype="multipart/form-data" id="add-order-form" class="">
                        <div class="form-body row">
                        <div class="col-sm-12 text-center">
                            <ul id="errors">
                                
                            </ul>
                        </div>

                           @csrf
                            @include('admin.msg._errors')
                           <div class="col-sm-6">
                               <div class="form-group">
                                    <label> @lang('site.name') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" name="name" class="form-control input-circle-right " placeholder="Name" required value="{{ old('name') }}"> </div>
                                </div>
                           </div>

                           <div class="col-sm-6">
                               <div class="form-group">
                                    <label> @lang('site.email') (Optional)</label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" name="email" class="form-control input-circle-right " placeholder="Email" value="{{ old('email') }}"> </div>
                                </div>
                           </div>

                           <div class="col-sm-6">
                               <div class="form-group">
                                    <label> @lang('site.address') (Optional)</label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" name="address" class="form-control input-circle-right " placeholder="Address" value="{{ old('address') }}"> </div>
                                </div>
                           </div>


                           <div class="col-sm-6">
                               <div class="form-group">
                                    <label> @lang('site.mobile') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" required name="mobile" class="form-control input-circle-right " placeholder="Mobile" value="{{ old('mobile') }}"> </div>
                                </div>
                           </div>

                           <div class="col-sm-12">
                              <div class="form-group">
                                   <label> @lang('site.notes') (Optional) <span class="label label-success"></span> </label>
                                   <div class="input-group">
                                       <span class="input-group-addon input-circle-left">
                                           <i class="fa fa-text-width"></i>
                                       </span>
                                       <textarea class="form-control count-text-desc-keywords" name="notes" rows="4"> {{ old('notes') }} </textarea>
                                      
                                   </div>
                               </div>
                          </div>

                          <div class="add-products-area">
                          <div class="add-product-panel" style="position: relative;">


                               <div class="col-sm-12">
                                    <a class="btn btn-danger remove-item" style="float: right;">
                                    <i class="fa fa-times "></i>
                                    </a>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label> @lang('site.product') </label>
                                        <div class="input-group">
                                            <span class="input-group-addon input-circle-left">
                                            </span>
                                            <select   name="product_id[]" required class="form-control select2" >
                                                <option value="">@lang('site.choose')</option>
                                                @foreach($products as $product)
                                                    <option value="{{ $product->id }}">{{ $product->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                  </div>
    
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label> @lang('site.quantity') </label>
                                        <div class="input-group">
                                            <span class="input-group-addon input-circle-left">
                                                <i class="fa fa-text-width"></i>
                                            </span>
                                            <input type="number" name="quantity[]" class="form-control input-circle-right " placeholder="Quantity" required> </div>
                                    </div>
                                </div>

                                

                                

    
                          </div>
                          </div>

                        <div class="text-center">
                            <button class="btn btn-primary btn-sm add-product-btn">
                                <i class="fa fa-plus"></i> @lang('site.addAnotherProduct')
                            </button>    
                        </div>
                                                       

                        </div>

                        
                        <div class="form-actions">
                            <button type="submit" class="btn blue">@lang('site.add')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('script')

<!-- <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/chosen/1.1.0/chosen.jquery.min.js"></script> -->


  <script src="{{ aurl() }}/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
      <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ aurl() }}/pages/scripts/components-select2.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->

<script>


    // $(document).ready(function() {
    //     $('.select-product').chosen();
    // });

    $.ajaxSetup({
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });
    $('.add-product-panel .select2').select2();
    $(".add-product-panel:first .remove-item").attr("disabled","disabled");
    var s="Select a State";
    $('.add-product-btn').click(function(){

        $('select.select2').select2('destroy');
        $('.add-product-panel:first').clone().appendTo('.add-products-area');
        $('.add-product-panel .select2').select2({placeholder:s,width:null});
        $(".add-product-panel:not(:first) .remove-item").attr("disabled",false);
        $(".add-product-panel:not(:first) .remove-item").addClass("removeThis");
        // $(".add-product-panel .select2-container:eq()").css("display","none");

    });


    //  remove container of product 
    $(document).on("click",".removeThis",function(){
        // var el = $(this);
        $(this).parents(".add-product-panel").remove();
    })








    $('#add-order-form').submit(function(e){
        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery(this)[0]);
            
            $.ajax({
                type:'POST',
                url:"{{route('admin.post.order.store')}}",
                data:formData,
                contentType: false,
                processData: false,
                beforeSend:function()
                {
                    $(".coverLoading").css("display","block");
                    $("#add-order-form").css("display","none");
                },
                success:function(data)
                {
                    $(".coverLoading").css("display","none");
                    $("#add-order-form").css("display","block");
                    $('#add-order-form').trigger("reset");

                    $("#errors").html('');
                    $("#errors").append("<li class='alert alert-success show-success'>"+data.success+"</li>")
                    

                },
                error: function(xhr, status, error) 
                {
                    // console.log(error);
                    $("#errors").html('');
                    $.each(xhr.responseJSON.errors, function (key, item) 
                    {
                        $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
                    });

                }

            });
    })

</script>
@endsection

