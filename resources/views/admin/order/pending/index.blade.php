@extends('admin.main')

@section('style')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{aurl()}}/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{aurl()}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')



<!-- BEGIN PAGE BAR -->
<div class="page-bar">


    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('admin.get.home.index') }}" class="active-bread">@lang('site.home')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('admin.get.order.pending.index') }}" class="active-bread">@lang('site.pending')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>@lang('site.view')</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->



<!-- END PAGE MESSAGE -->
@include('admin.msg._messages')

<!-- END PAGE HEADER-->
<div class="note note-info">
    <h3> @lang('site.view') ( @lang('site.pending') ) </h3>
</div>


<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
       
            <div class="portlet-body">

                @if($orders->count())
               <form action="{{ route('admin.post.order.pending.deleteMulti') }}" method="post" id="Form2"> @csrf </form>
                </button>

                    <table class="table table-striped table-bordered table-hover table-checkable table-sort order-column column" id="sample_1">
                        <thead>
                            <tr>
                                @if(checkPermission('orders-delete-pending'))
                                <th>
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                @endif

                                <th> @lang('site.code') </th>
                                <th> @lang('site.name') </th>
                                <th> @lang('site.email') </th>
                                <th> @lang('site.mobile') </th>
                                <th> @lang('site.actions') </th>

                                @if(checkPermission('orders-add-shipping') || checkPermission('orders-add-canceled'))
                                <th>  </th>
                                @endif


                            </tr>
                        </thead>
                        <tbody class="connected-sortable droppable-area1">
                            <div class="coverLoading" style="width: 100%; display: none;"><img src="{{furl()}}/img/loading.gif" style="display: block; margin: auto;"></div>

                            @foreach($orders as $order)
                            <tr class="odd gradeX draggable-item" id="row-no-{{ $order->id }}">
                                @if(checkPermission('orders-delete-pending'))                                
                                <td class="text-center">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="checkboxes" name="deleteMulti[]" form="Form2" multiple value="{{ $order->id}}" />
                                        <span></span>
                                    </label>
                                </td>
                                @endif
                                <td class="text-center"> 
                                    {{ $order->code }} 
                                </td>
                                <td class="text-center"> 
                                    @if($order->name !== null)
                                        {{ $order->name }}
                                    @else 
                                        <div class="bold-red-text">Not available</div> 
                                    @endif
                                </td>
                              
                                <td class="text-center"> 
                                    @if($order->email !== null)
                                        {{ $order->email }}
                                    @else 
                                        <div class="bold-red-text">Not available</div> 
                                    @endif
                                </td>

                                <td class="text-center"> 
                                    @if($order->mobile !== null)
                                        {{ $order->mobile }}
                                    @else 
                                        <div class="bold-red-text">Not available</div> 
                                    @endif 
                                </td>
                                
                                <td class="text-center">

                                    <a href="{{ route('admin.get.order.pending.show', ['id'=>$order->id]) }}" class="btn btn-info btn-sm" title="@lang('site.show')">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                
                                    @if(checkPermission('orders-delete-pending'))
                                    <a href="{{ route('admin.get.order.pending.delete', ['id'=>$order->id]) }}" class="conform-delete btn btn-danger btn-sm" title="@lang('site.delete')">
                                            <i class="fa fa-close"></i>
                                    </a>
                                    @endif
                                 
                                </td>

                                @if(checkPermission('orders-add-shipping') || checkPermission('orders-add-canceled'))
                                <td class="text-center"> 
                                    @if(checkPermission('orders-add-shipping'))
                                    <form method="post" class="shipping-form" id="shipping-{{ $order->id }}"> 
                                        @csrf 
                                        <input type="hidden" name="id" value="{{ $order->id }}">
                                    </form>
                                    @endif

                                    @if(checkPermission('orders-add-canceled'))
                                    <form method="post" class="canceled-form" id="canceled-{{ $order->id }}"> 
                                        @csrf 
                                        <input type="hidden" name="id" value="{{ $order->id }}">
                                    </form>
                                    @endif

                                    @if(checkPermission('orders-add-shipping'))
                                    <button type="submit" class="btn btn-primary btn-sm" onclick="return confirm('Confirm shipping the order ?');" form="shipping-{{ $order->id }}">
                                        @lang('site.addShipping')
                                    </button>
                                    @endif

                                    @if(checkPermission('orders-add-canceled'))
                                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Confirm shipping the order ?');" form="canceled-{{ $order->id }}">
                                         @lang('site.cancel')
                                     </button>
                                     @endif
                                </td>
                                @endif
                                
                            </tr>
                            @endforeach

                        </tbody>
                        <hr>
                        
                    
                    </table>

                    <div class="text-center">
                        {{ $orders->render() }}
                    </div>

                            
                
                        </form>
                    
                    @if(checkPermission('orders-delete-pending'))                    
                    <button id="delete-checked" type="submit" class="btn btn-danger btn-sm item-checked" form="Form2">@lang('site.deleteChecked')</button>
                    @endif
            
              
            <!-- end form  -->

                @else
                        
                        @include('admin.msg.notFound')
                
                @endif

                
                <div id="msg-not-found" style="display: none">

                    @include('admin.msg.notFound')
                
                </div>





            </div>

    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>



@endsection



@section('script')

   
 <script src="{{aurl()}}/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{aurl()}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script> 
<!-- END PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<script type="text/javascript" src="{{aurl()}}/seoera/js/sortAndDataTable.js"></script>

<script type="text/javascript">

  
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

    $('.shipping-form').each(function(){
        $(this).submit(function(e){
            e.preventDefault();

            // return false;
            var formData  = new FormData(jQuery(this)[0]);
            var id = parseInt(formData.get("id"));
            
            $.ajax({
                type:'POST',
                url:"{{route('admin.post.order.pending.toShipping')}}",
                data:formData,
                contentType: false,
                processData: false,
                beforeSend:function()
                {
                    $(".coverLoading").css("display","block");
                    $("#sample_1").css("visibility","hidden");
                },
                success:function(data)
                {
                    $(".coverLoading").css("display","none");
                    $("#sample_1").css("visibility","visible");
                    $('#row-no-'+id).hide();
                    var rowCount = $('#sample_1 tr').length;
                    if(rowCount == 2){
                        $('#sample_1').hide();
                        $('#delete-checked').hide();
                        $('#msg-not-found').show();
                    }

                },
                error: function(xhr, status, error) 
                {
                    console.log(error);
                    $("#errors").html('');
                    $.each(xhr.responseJSON.errors, function (key, item) 
                    {
                        $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
                    });

                }

            });
        })
    })

    $('.canceled-form').each(function(){
        $(this).submit(function(e){
            e.preventDefault();

            // return false;
            var formData  = new FormData(jQuery(this)[0]);
            var id = parseInt(formData.get("id"));
            
            $.ajax({
                type:'POST',
                url:"{{route('admin.post.order.toCanceled')}}",
                data:formData,
                contentType: false,
                processData: false,
                beforeSend:function()
                {
                    $(".coverLoading").css("display","block");
                    $("#sample_1").css("visibility","hidden");
                },
                success:function(data)
                {
                    $(".coverLoading").css("display","none");
                    $("#sample_1").css("visibility","visible");
                    $('#row-no-'+id).hide();
                    var rowCount = $('#sample_1 tr').length;
                    if(rowCount == 2){
                        $('#sample_1').hide();
                        $('#delete-checked').hide();
                        $('#msg-not-found').show();
                    }
                },
                error: function(xhr, status, error) 
                {
                    console.log(error);
                    $("#errors").html('');
                    $.each(xhr.responseJSON.errors, function (key, item) 
                    {
                        $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
                    });

                }

            });
        })
    })

   
</script>


@endsection
