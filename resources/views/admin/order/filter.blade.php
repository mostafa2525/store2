@extends('admin.main')

@section('style')

<link href="{{aurl()}}/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{aurl()}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />

     <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{aurl()}}/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="{{aurl()}}/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="{{aurl()}}/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="{{aurl()}}/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->

    <!-- BEGIN PAGE LEVEL PLUGINS -->


@endsection


@section('content')
  

<!-- BEGIN PAGE BAR -->
<div class="page-bar">


    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('admin.get.home.index') }}" class="active-bread">@lang('site.home')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>@lang('site.add')</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->





<!-- END PAGE HEADER-->
<div class="note note-info">
    <h3> @lang('site.order') ( @lang('site.filter') ) </h3>
</div>


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">


                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> @lang('site.filter') </span>
                    </div>
                </div>

                <div class="portlet-body form">
                    
                    <div class="coverLoading" style="width: 100%; display: none;"><img src="{{furl()}}/img/loading.gif" style="display: block; margin: auto;"></div>

                    <form role="form" method="get" action="{{ route('admin.get.order.filterOrder') }}" enctype="multipart/form-data" id="add-order-form" class="">
                        <div class="form-body row">
                            <div class="col-sm-12 text-center">
                                <ul id="errors">
                                    
                                </ul>
                            </div>



                            <div class="col-sm-4">
                               <div class="form-group">
                                    <label> @lang('site.from')</label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" name="from" class="form-control input-circle-right  date-picker" placeholder="From" data-date-format="yyyy-mm-dd" value="{{ old('from') }}" required > </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                               <div class="form-group">
                                    <label> @lang('site.to')</label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" name="to" class="form-control input-circle-right  date-picker" placeholder="To" data-date-format="yyyy-mm-dd" value="{{ old('to') }}"  > </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                               <div class="form-group">
                                    <label> @lang('site.status')</label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <select name="status" class="form-control"  required >
                                            <option value="all">@lang('site.all')</option>
                                            <option value="pending">@lang('site.pending')</option>
                                            <option value="shipping">@lang('site.shipping')</option>
                                            <option value="accepted">@lang('site.accepted')</option>
                                            <option value="refused">@lang('site.refused')</option>
                                            <option value="canceled">@lang('site.canceled')</option>
                                        </select>
                                    </div>
                                </div>
                            </div>



            

                        </div>

                        
                        <div class="form-actions">
                            <button type="submit" class="btn blue">@lang('site.add')</button>
                        </div>
                    </form>

               

                @if(@isset($orders))
                @if($orders->count())

                <h3>@lang('site.from') : {{Request('from')}}</h3>
                @if(Request('to'))
                <h3>@lang('site.to') : {{Request('to')}}</h3>
                @endif
                @if(Request('status'))
                <h3>@lang('site.status') : {{Request('status')}}</h3>
                @endif
 
               <table class="table table-striped table-bordered table-hover table-checkable table-sort order-column column" id="sample_1">
                        <thead>
                            <tr>
                                <th> @lang('site.code') </th>
                                <th> @lang('site.name') </th>
                                <th> @lang('site.email') </th>
                                <th> @lang('site.mobile') </th>
                                <th> @lang('site.status') </th>
                                <th> @lang('site.actions') </th>


                            </tr>
                        </thead>
                        <tbody class="connected-sortable droppable-area1">
                            
                            @foreach($orders as $order)
                            <tr class="odd gradeX draggable-item" id="row-no-{{ $order->id }}">
                               
                                <td class="text-center"> 
                                    {{ $order->code }} 
                                </td>
                                <td class="text-center"> 
                                    @if($order->name !== null)
                                        {{ $order->name }}
                                    @else 
                                        <div class="bold-red-text">Not available</div> 
                                    @endif
                                </td>
                              
                                <td class="text-center"> 
                                    @if($order->email !== null)
                                        {{ $order->email }}
                                    @else 
                                        <div class="bold-red-text">Not available</div> 
                                    @endif 
                                </td>

                                <td class="text-center"> 
                                    @if($order->mobile !== null)
                                        {{ $order->mobile }}
                                    @else 
                                        <div class="bold-red-text">Not available</div> 
                                    @endif 
                                </td>

                                <td class="text-center"> 
                                    {{ $order->status }} 
                                </td>

                                <td class="text-center">

                                    <a href="{{ route('admin.get.order.accepted.show', ['id'=>$order->id]) }}" class="btn btn-info btn-sm" title="@lang('site.show')">
                                        <i class="fa fa-eye"></i>
                                    </a>

                                </td>
                                
                            
                                            
                            </tr>
                            @endforeach

                        </tbody>
                        <hr>
                        
                    
                    </table>

                    <div class="text-center">
                        {{ $orders->appends(request()->query())->links() }}
                    </div>
                            
                
           
              
                    
              
            <!-- end form  -->

                @else
                        
                        @include('admin.msg.notFound')
                
                @endif
                @endif


                </div>
            </div>
        </div>
    </div>



@endsection

@section('script')

 <script src="{{aurl()}}/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="{{aurl()}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>

 <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{aurl()}}/global/plugins/moment.min.js" type="text/javascript"></script>
        <script src="{{aurl()}}/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
        <script src="{{aurl()}}/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="{{aurl()}}/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
        <script src="{{aurl()}}/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->
        <script src="{{aurl()}}/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
        
       

    <script type="text/javascript" src="{{aurl()}}/seoera/js/sortAndDataTable.js"></script>



@endsection

