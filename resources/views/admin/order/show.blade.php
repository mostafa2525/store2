@extends('admin.main')

@section('style')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{aurl()}}/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{aurl()}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')



<!-- BEGIN PAGE BAR -->
<div class="page-bar">


    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('admin.get.home.index') }}" class="active-bread">@lang('site.home')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('admin.get.order.'. $order->status. '.index') }}" class="active-bread">@lang('site.'. $order->status. 'Orders')</a>
            <i class="fa fa-circle"></i>
        </li>

        <li>
            <a href="{{ route('admin.get.order.show', ['id'=>$order->id]) }}" class="active-bread">@lang('site.orderNum') {{ $order->id }}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>@lang('site.show')</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->



<!-- END PAGE MESSAGE -->
@include('admin.msg._messages')

<!-- END PAGE HEADER-->
<div class="note note-info">
    <h3> @lang('site.show') (@lang('site.'. $order->status. 'Orders') / @lang('site.orderNum') {{ $order->id }}) </h3>
</div>



<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
       
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover table-checkable table-sort order-column column" id="order-data-table">
                    <tr>
                        <th> @lang('site.name') </th>
                        <td> {{ $order->name }} </td>
                    </tr>
                    <tr>
                        <th> @lang('site.code') </th>
                        <td> {{ $order->code }} </td>
                    </tr>
                    <tr>
                        <th> @lang('site.email') </th>
                        <td> 
                            @if($order->email !== null)
                                {{ $order->email }} 
                            @else 
                                <div class="bold-red-text"> This field is empty </div>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th> @lang('site.address') </th>
                        <td> 
                            @if($order->address !== null)
                                {{ $order->address }} 
                            @else 
                                <div class="bold-red-text"> This field is empty </div>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th> @lang('site.mobile') </th>
                        <td> 
                            @if($order->mobile !== null)
                                {{ $order->mobile }} 
                            @else 
                                <div class="bold-red-text"> This field is empty </div>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th> @lang('site.addedBy') </th>
                        <td> 
                            @if($order->admin_id !== null)
                                {{ $order->admin->name }} 
                            @else 
                                <div> * Not added by admins * </div>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th> @lang('site.client') </th>
                        <td> 
                            @if($order->client_id !== null)
                              @if($order->client)
                                {{ $order->client->name }}
                              @endif 
                            @else 
                                <div class="bold-red-text"> This field is empty </div>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <th> @lang('site.date') </th>
                        <td> 
                            {{ date("y-m-d", strtotime($order->created_at)) }}    
                        </td>
                    </tr>
                    <tr>
                        <th> @lang('site.notes') </th>
                        <td> 
                            @if($order->notes !== null)
                                {{ $order->notes }} 
                            @else 
                                <div class="bold-red-text"> This field is empty </div>
                            @endif
                        </td>
                    </tr>
                </table>

                @if($orderContents->count())
               {{-- <form action="{{ route('admin.post.order.pending.deleteMulti') }}" method="post" id="Form2"> @csrf </form> --}}
                {{-- <button  class="btn btn-info btn-sm pull-right sort" type="submit" form="sortForm" >         @lang('site.sort') 
                </button> --}}
                        <table class="table table-striped table-bordered table-hover table-checkable table-sort order-column column" id="sample_1">
                        <thead>
                            <tr>
                                <th>
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                <th> @lang('site.product') </th>
                                <th> @lang('site.quantity') </th>
                                <th> @lang('site.status') </th>
                                <th> </th>


                            </tr>
                        </thead>
                        <tbody class="connected-sortable droppable-area1">
                            
                            @foreach($orderContents as $con)
                            <tr class="odd gradeX draggable-item" id="row-no-{{ $con->id }}">
                                <input type="hidden" name="sort[]" multiple value="{{ $con->id }}" form="sortForm">
                                <td class="text-center">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="checkboxes" name="deleteMulti[]" form="Form2" multiple value="{{ $con->id}}" />
                                        <span></span>
                                    </label>
                                </td>
                                <td class="text-center"> 
                                    {{ $con->product->name }}
                                </td>
                                <td class="text-center"> 
                                    {{ $con->quantity }} 
                                </td>
                                <td id="status-col-{{ $con->id }}" class="text-center status-col"> 
                                    {{ $con->status }} 
                                </td>
                                

                                <td class="text-center">
                                    @if($con->status == 'pending') 
                                        <form method="post" class="accept-form" id="accept-{{ $con->id }}">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $con->id }}">
                                            <input type="hidden" name="status" value="accepted">
                                        </form>

                                        <form method="post" class="refuse-form" id="refuse-{{ $con->id }}">
                                            @csrf
                                            <input type="hidden" name="id" value="{{ $con->id }}">
                                            <input type="hidden" name="status" value="refused">
                                        </form>

                                        <button type="submit" id="accept-btn-{{ $con->id }}" class="btn btn-success btn-sm" form="accept-{{ $con->id }}" onclick="return confirm('Confirm accepting the product?');">
                                            @lang('site.accept')
                                        </button>

                                        <button type="submit" id="refuse-btn-{{ $con->id }}" class="btn btn-danger btn-sm" form="refuse-{{ $con->id }}" onclick="return confirm('Confirm refusing the product?');">
                                            @lang('site.refuse')
                                        </button>
                                    @endif
                                    
                                </td>
                                
                            </tr>
                            @endforeach

                        </tbody>
                        <hr>
                        
                    
                    </table>
                            
                
                        </form>
                    {{-- <button type="submit" class="btn btn-danger btn-sm item-checked" form="Form2">@lang('site.deleteChecked')</button> --}}
              
                    
              
            <!-- end form  -->

                @else
                        
                        @include('admin.msg.notFound')
                
                @endif



            </div>

    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>



@endsection



@section('script')

   

{{-- <script src="{{aurl()}}/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{aurl()}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script> --}}
<!-- END PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

{{-- <script type="text/javascript" src="{{aurl()}}/seoera/js/sortAndDataTable.js"></script> --}}

<script>

  $.ajaxSetup({
      headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
  });

  $('.accept-form').each(function(){
      $(this).submit(function(e){
          e.preventDefault();

          // return false;
          var formData  = new FormData(jQuery(this)[0]);
          var id = parseInt(formData.get("id"));
          
          $.ajax({
              type:'POST',
              url:"{{route('admin.get.order.contentStatus')}}",
              data:formData,
              contentType: false,
              processData: false,
              success:function(data)
              {
                  $('#status-col-'+id).html(data);
                  $('#accept-btn-'+id).hide();
                  $('#refuse-btn-'+id).hide();
              },
              error: function(xhr, status, error) 
              {
                  console.log(error);
                  $("#errors").html('');
                  $.each(xhr.responseJSON.errors, function (key, item) 
                  {
                      $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
                  });

              }

          });
      })
  })

  $('.refuse-form').each(function(){
      $(this).submit(function(e){
          e.preventDefault();

          // return false;
          var formData  = new FormData(jQuery(this)[0]);
          var id = parseInt(formData.get("id"));
          
          $.ajax({
              type:'POST',
              url:"{{route('admin.get.order.contentStatus')}}",
              data:formData,
              contentType: false,
              processData: false,
              success:function(data)
              {
                  $('#status-col-'+id).html(data);
                  $('#accept-btn-'+id).hide();
                  $('#refuse-btn-'+id).hide();
              },
              error: function(xhr, status, error) 
              {
                  console.log(error);
                  $("#errors").html('');
                  $.each(xhr.responseJSON.errors, function (key, item) 
                  {
                      $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
                  });

              }

          });
      })
  })

 
</script>


@endsection
