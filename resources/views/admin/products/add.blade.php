@extends('admin.main')

@section('style')

       <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{ aurl() }}/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="{{ aurl() }}/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->

@endsection

@section('content')



<!-- BEGIN PAGE BAR -->
<div class="page-bar">


    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('admin.get.home.index') }}" class="active-bread">@lang('site.home')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('admin.get.product.index') }}" class="active-bread">@lang('site.products')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>@lang('site.add')</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->





<!-- END PAGE HEADER-->
<div class="note note-info">
    <h3> @lang('site.addItem')  ( @lang('site.products') ) </h3>
</div>


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> @lang('site.addItem') </span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="col-md-12">
                        <ul id="errors"></ul>
                    </div>
                    <form role="form" method="post" enctype="multipart/form-data" id="add-form" >
                        <div class="coverLoading" style=""><img src="{{furl()}}/img/loading.gif"></div>
                        <div class="form-body row">
                           @csrf
                             @include('admin.msg._errors')


                            <div class="col-md-4 col-sm-6">
                                <div class="form-group">
                                    <label> @lang('site.name') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" name="name" class="form-control input-circle-right" required value="{{ old('name') }}"> </div>
                                </div>
                            </div>

                            <div class="col-md-4 col-sm-6">
                                <div class="form-group">
                                    <label> @lang('site.category') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <select class="form-control" name="category_id" id="getSub" required>
                                                <option value="">@lang('site.choose')</option>
                                           @foreach($categories as $cat)
                                                <option value="{{ $cat->id }}" @if(old('category_id')) {{ printSelect($cat->id,old('category_id')) }} @endif >{{ $cat->name }}</option>
                                           @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>



                            <div class="col-md-4 col-sm-6">
                                <div class="form-group">
                                    <label> @lang('site.subCategory') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <select class="form-control" name="sub_category_id" id="sub" required>
                                                <option value="">@lang('site.choose')</option>
                                        </select>
                                    </div>
                                </div>
                            </div>




                            <div class="col-md-4 col-sm-6">
                                <div class="form-group">
                                    <label> @lang('site.price') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="number" name="price" class="form-control input-circle-right" required value="{{ old('price') }}"> </div>
                                </div>
                            </div>


                            <div class="col-md-4 col-sm-6">
                                <div class="form-group">
                                    <label> @lang('site.offer') (@lang('site.number') % ) </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="number" name="offer" class="form-control input-circle-right" required value="{{ old('offer') }}" max="100"> </div>
                                </div>
                            </div>


                            <div class="col-md-4  col-sm-6">
                                <div class="form-group">
                                    <label> @lang('site.featured') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <select class="form-control" name="featured" required>
                                            <option value="no"  old('featured') == 'no' : 'selected' ? '' >@lang('site.no')</option>
                                            <option value="yes" old('featured') == 'yes' : 'selected' ? ''>@lang('site.yes')</option>
                                        </select>
                                    </div>
                                </div>
                            </div>







                           

                            <div class="col-md-4 col-sm-6">
                               <div class="form-group">
                                    <label> @lang('site.image')  (450 * 400) </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-image"></i>
                                        </span>
                                    <input type="file" name="img" accept="image/*" onchange="loadFile(event)" class="form-control input-circle-right" required  /> 
                                    </div>
                                </div>
                           </div>



                           <div class="col-md-4 col-sm-6">
                                <div class="form-group">
                                    <label> @lang('site.showInHomePage') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <select class="form-control" name="show_in_homePage" required>
                                             <option value="no"  old('show_in_homePage') == 'no' : 'selected' ? '' >@lang('site.no')</option>
                                            <option value="yes" old('show_in_homePage') == 'yes' : 'selected' ? ''>@lang('site.yes')</option>
                                        </select>
                                    </div>
                                </div>
                            </div>

                             <div class="col-md-4 col-sm-6">
                                <div class="form-group">
                                    <label> @lang('site.qty') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="number" name="qty" class="form-control input-circle-right" required value="{{ old('qty') }}"> </div>
                                </div>
                            </div>

                            


                           <div class="col-sm-12">
                               <div class="form-group">
                                   <img id="output" class="privew-image"  />
                               </div>
                           </div>

                           <div class="col-md-12">
                                <div class="form-group">
                                    <label> @lang('site.tags') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" name="tags" class="form-control input-circle-right" required value="{{ old('tags') }}"> </div>
                                </div>
                            </div>


     

                            

                            <div class="col-md-6  col-sm-12">
                                <div class="form-group">
                                    <label> @lang('site.color') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <select id="multiple" name="color[]" required class="form-control select2-multiple" multiple>
                                            @foreach($colors as $color)
                                            <option value="{{$color->id}}"  >{{$color->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label> @lang('site.size') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <select id="multiple" name="size[]"  required class="form-control select2-multiple" multiple>
                                            @foreach($sizes as $size)
                                            <option value="{{$size->id}}">{{$size->title}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="form-group">
                                    <label> @lang('site.smallDescription')  </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <textarea name="small_desc" class="form-control input-circle-right" required rows="3" >{{ old('small_desc') }}</textarea>
                                         </div>
                                </div>
                            </div>


                           <div class="col-sm-12">
                               <div class="form-group">
                                    <label> @lang('site.description') </label>
                              
                                        <textarea required class="form-control ckeditor" name="desc" rows="5">{!! old('desc') !!}</textarea>
                                   
                                </div>
                           </div>


                           <div class="col-sm-12">
                               <div class="form-group">
                                    <label> @lang('site.slug')  /  @lang('site.seo')  </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" name="slug" class="form-control input-circle-right "   value="{{ old('slug') }}"> </div>
                                </div>
                           </div>


                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">@lang('site.add')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection

@section('script')

    <script src="{{ aurl() }}/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{ aurl() }}/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
      <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{ aurl() }}/pages/scripts/components-select2.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->


    <script type="text/javascript">
        
    $(document).on("change","#getSub",function(e) 
    {

        e.preventDefault();

        // return false;
        var catId = $(this).val();
        $.ajax({

           type:'POST',
           url:"{{route('admin.post.product.getSub')}}",
           data:{
            "_token": "{{ csrf_token() }}",
            "catId": catId },
           success:function(data)
           {
                $("#sub").html('');
                $("#sub").append(data);
           },
         

        });

    });





    $("#add-form").submit(function(e) 
    {

        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#add-form')[0]);
        $.ajax({

           type:'POST',
           url:"{{ route('admin.post.product.store') }}",
           data:formData,
           contentType: false,
           processData: false,
           beforeSend:function()
           {
               $(".coverLoading").css("display","block")
               $("#add-form input[type='submit']").css("display","none");
           },
           success:function(data)
           {
             $("#errors").html('');
             $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
             $('input').val("");
             $("textarea").val("");
             $(".coverLoading").css("display","none")

           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
             $(".coverLoading").css("display","none")
              
            }

        });

    });




    </script>

@endsection




