@extends('admin.main')

@section('style')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{aurl()}}/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{aurl()}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<!-- <link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css"> -->
@endsection

@section('content')



<!-- BEGIN PAGE BAR -->
<div class="page-bar">


    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('admin.get.home.index') }}" class="active-bread">@lang('site.home')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('admin.get.product.index') }}" class="active-bread">@lang('site.products')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>@lang('site.view')</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->



<!-- END PAGE MESSAGE -->
@include('admin.msg._messages')

<!-- END PAGE HEADER-->
<div class="note note-info">
    <h3> @lang('site.view')  ( @lang('site.products') ) </h3>
</div>


<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
       
            <div class="portlet-body">
                @if(checkPermission('products-add'))                                    
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group">
                                <a id="sample_editable_1_new" href="{{ route('admin.get.product.add') }}" class="btn sbold green"> @lang('site.add')
                                    <i class="fa fa-plus"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                @endif






            @if($products->count())
                @if(checkPermission('products-delete'))                                                    
               <form action="{{ route('admin.post.product.deleteMulti') }}" method="post" id="Form2"> @csrf </form>
               @endif

               @if(checkPermission('products-edit'))                                    
               <form action="{{ route('admin.post.product.sort') }}" method="post" id="sortForm">@csrf</form>
                @endif
       
                    
                        <table class="table table-striped table-bordered table-hover table-checkable table-sort order-column column" id="sample_1">
                        <thead>
                            <tr>
                                @if(checkPermission('products-delete'))                                    
                                <th>
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                @endif

                                <th> @lang('site.code') </th>
                                <th> @lang('site.name') </th>
                                <th> @lang('site.category') </th>
                                <th> @lang('site.subCategory') </th>

                                @if(checkPermission('products-edit'))                                                                    
                                <th> @lang('site.images') </th>
                                @endif

                                <th> @lang('site.actions') </th>

                                @if(checkPermission('products-edit'))                                    
                                <th> @lang('site.seo') </th>
                                @endif

                            </tr>
                        </thead>
                        <tbody class="connected-sortable droppable-area1">
                            
                            @foreach($products as $prod)

                             @if($prod->category)
                                <tr class="odd gradeX draggable-item">
                                    <input type="hidden" name="sort[]" multiple value="{{ $prod->id }}" form="sortForm">
                                    @if(checkPermission('products-delete'))                                                                        
                                    <td class="text-center">
                                        <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                            <input type="checkbox" class="checkboxes" name="deleteMulti[]" form="Form2" multiple value="{{ $prod->id}}" />
                                            <span></span>
                                        </label>
                                    </td>
                                    @endif

                                    <td class="text-center"> {{ $prod->code }} </td>
                                    <td class="text-center"> {{ $prod->name }} </td>
                                    <td class="text-center"> 
                                        <a href="{{route('admin.get.product.category',[$prod->category->id])}}">
                                        {{ $prod->category->name }}
                                        </a>
                                    </td>

                                    <td class="text-center"> 
                                        <a href="{{route('admin.get.product.sub',[$prod->sub->id])}}">
                                        {{ $prod->sub->name }}
                                         </a>
                                    </td>

                                    @if(checkPermission('products-edit'))                                                                         
                                     <td class="text-center"> 
                                        <a href="{{route('admin.get.product.addImages',[$prod->id])}}">
                                         <i class="fa fa-image fa-2x"></i>
                                         </a>
                                    </td>
                                    @endif
                                    

                                   <td class="text-center">
                                        <a href="{{route('front.get.product.show',[$prod->category->slug,$prod->sub->slug,$prod->slug])}}" class=" btn btn-success btn-sm" title="@lang('site.show')" target="_blank">
                                                <i class="fa fa-eye"></i>
                                        </a>
                                        {!!show_potions('product',$prod->id,$prod->status, 'products')!!}

                                    </td>

                                    @if(checkPermission('products-edit'))                                    
                                    <td class="text-center">  
                                      <a href="{{ route('admin.get.product.seo',$prod->id) }}" class="btn btn-sm btn-primary"> @lang('site.seo') </a> 
                                    </td>
                                    @endif

                                   

                                </tr>
                                @endif
                            @endforeach

                        </tbody>
                        <hr>
                        
                    
                    </table>
                            
                
                        </form>

                    @if(checkPermission('products-delete'))                                                        
                    <button type="submit" class="btn btn-danger btn-sm item-checked" form="Form2">@lang('site.deleteChecked')</button>
                    @endif
              
            <!-- end form  -->
                @else
                        
                        @include('admin.msg.notFound')
                
                @endif

            </div>

    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
        </div>
        <div class="col-sm-12">
            {{$products->links()}}
        </div>
</div>



@endsection



@section('script')

   

<script src="{{aurl()}}/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{aurl()}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>

<script type="text/javascript" src="{{aurl()}}/seoera/js/sortAndDataTable.js"></script>


<script type="text/javascript">
    




</script>


@endsection