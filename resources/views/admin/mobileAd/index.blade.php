@extends('admin.main')



@section('content')



<!-- BEGIN PAGE BAR -->
<div class="page-bar">


    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('admin.get.home.index') }}" class="active-bread">@lang('site.home')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('admin.get.mobilead.index') }}" class="active-bread">@lang('site.mobileAds')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>@lang('site.view')</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->





<!-- END PAGE HEADER-->
<div class="note note-info">
    <h3> @lang('site.view')  ( @lang('site.mobileAds') ) </h3>
</div>


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-red-sunglo">
                        <i class="icon-settings font-red-sunglo"></i>
                        <span class="caption-subject bold uppercase"> @lang('site.editItem') </span>
                    </div>
                </div>
                <div class="portlet-body form">
                    <form role="form" method="post" action="{{ route('admin.put.mobilead.update') }}"  enctype="multipart/form-data">
                        <div class="form-body row">
                           @csrf
                           {{ method_field('PUT') }}
                            @include('admin.msg._errors')

                        <div>
                           <div class="col-sm-4">
                                <div class="form-group">
                                    <label> @lang('site.link1') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" name="link1" class="form-control input-circle-right"  value="{{ $row->link1 }}"> 
                                        <input type="hidden" name="id" value="{{ $row->id }}"  >
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-5">
                                <div class="form-group">
                                        <label> @lang('site.img1') </label>
                                        <div class="input-group">
                                            <span class="input-group-addon input-circle-left">
                                                <i class="fa fa-image"></i>
                                            </span>
                                            <input type="file" name="img1" accept="image/*" onchange="loadFile(event)" class="form-control  input-circle-right" > </div>
                                    </div>
                            </div>
          
                            <div class="col-sm-3">
                                <div class="form-group">
                                        <img id="output" class="privew-image img-thumbnail" @if($row->img1) src="  {{ getImage(MOBILEAD_PATH.$row->img1) }}"  @endif />
                                </div>
                            </div>
                        </div>

                        <div>
                           <div class="col-sm-4">
                                <div class="form-group">
                                    <label> @lang('site.link2') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" name="link2" class="form-control input-circle-right"  value="{{ $row->link2 }}"> 
                                        <input type="hidden" name="id" value="{{ $row->id }}"  >
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-5">
                                <div class="form-group">
                                        <label> @lang('site.img2') </label>
                                        <div class="input-group">
                                            <span class="input-group-addon input-circle-left">
                                                <i class="fa fa-image"></i>
                                            </span>
                                            <input type="file" name="img2" accept="image/*" onchange="loadFile(event)" class="form-control  input-circle-right" > </div>
                                    </div>
                            </div>
          
                            <div class="col-sm-3">
                                <div class="form-group">
                                        <img id="output" class="privew-image img-thumbnail" @if($row->img2) src="  {{ getImage(MOBILEAD_PATH.$row->img2) }}"  @endif />
                                </div>
                            </div>
                        </div>

                        <div>
                           <div class="col-sm-4">
                                <div class="form-group">
                                    <label> @lang('site.link3') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" name="link3" class="form-control input-circle-right"  value="{{ $row->link3 }}"> 
                                        <input type="hidden" name="id" value="{{ $row->id }}"  >
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-5">
                                <div class="form-group">
                                        <label> @lang('site.img3') </label>
                                        <div class="input-group">
                                            <span class="input-group-addon input-circle-left">
                                                <i class="fa fa-image"></i>
                                            </span>
                                            <input type="file" name="img3" accept="image/*" onchange="loadFile(event)" class="form-control  input-circle-right" > </div>
                                    </div>
                            </div>
          
                            <div class="col-sm-3">
                                <div class="form-group">
                                        <img id="output" class="privew-image img-thumbnail" @if($row->img3) src="  {{ getImage(MOBILEAD_PATH.$row->img3) }}"  @endif />
                                </div>
                            </div>
                        </div>

                        <div>
                           <div class="col-sm-4">
                                <div class="form-group">
                                    <label> @lang('site.link4') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" name="link4" class="form-control input-circle-right"  value="{{ $row->link4 }}"> 
                                        <input type="hidden" name="id" value="{{ $row->id }}"  >
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-5">
                                <div class="form-group">
                                        <label> @lang('site.img4') </label>
                                        <div class="input-group">
                                            <span class="input-group-addon input-circle-left">
                                                <i class="fa fa-image"></i>
                                            </span>
                                            <input type="file" name="img4" accept="image/*" onchange="loadFile(event)" class="form-control  input-circle-right" > </div>
                                    </div>
                            </div>
          
                            <div class="col-sm-3">
                                <div class="form-group">
                                        <img id="output" class="privew-image img-thumbnail" @if($row->img4) src="  {{ getImage(MOBILEAD_PATH.$row->img4) }}"  @endif />
                                </div>
                            </div>
                        </div>

                        <div>
                           <div class="col-sm-4">
                                <div class="form-group">
                                    <label> @lang('site.link5') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" name="link5" class="form-control input-circle-right"  value="{{ $row->link5 }}"> 
                                        <input type="hidden" name="id" value="{{ $row->id }}"  >
                                    </div>
                                </div>
                            </div>


                            <div class="col-sm-5">
                                <div class="form-group">
                                        <label> @lang('site.img5') </label>
                                        <div class="input-group">
                                            <span class="input-group-addon input-circle-left">
                                                <i class="fa fa-image"></i>
                                            </span>
                                            <input type="file" name="img5" accept="image/*" onchange="loadFile(event)" class="form-control  input-circle-right" > </div>
                                    </div>
                            </div>
          
                            <div class="col-sm-3">
                                <div class="form-group">
                                        <img id="output" class="privew-image img-thumbnail" @if($row->img5) src="  {{ getImage(MOBILEAD_PATH.$row->img5) }}"  @endif />
                                </div>
                            </div>
                        </div>

                        <div>
                           <div class="col-sm-4">
                                <div class="form-group">
                                    <label> @lang('site.link6') </label>
                                    <div class="input-group">
                                        <span class="input-group-addon input-circle-left">
                                            <i class="fa fa-text-width"></i>
                                        </span>
                                        <input type="text" name="link6" class="form-control input-circle-right"  value="{{ $row->link6 }}"> 
                                        <input type="hidden" name="id" value="{{ $row->id }}"  >
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-5">
                                <div class="form-group">
                                        <label> @lang('site.img6') </label>
                                        <div class="input-group">
                                            <span class="input-group-addon input-circle-left">
                                                <i class="fa fa-image"></i>
                                            </span>
                                            <input type="file" name="img6" accept="image/*" onchange="loadFile(event)" class="form-control  input-circle-right" > </div>
                                    </div>
                            </div>
          
                            <div class="col-sm-3">
                                <div class="form-group">
                                        <img id="output" class="privew-image img-thumbnail" @if($row->img6) src="  {{ getImage(MOBILEAD_PATH.$row->img6) }}"  @endif />
                                </div>
                            </div>
                        </div>

                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn red">@lang('site.save')</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection




