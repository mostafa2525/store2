@extends('admin.main')

@section('style')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{aurl()}}/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{aurl()}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')

<!-- BEGIN PAGE BAR -->
<div class="page-bar">


    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('admin.get.home.index') }}" class="active-bread">@lang('site.home')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('admin.get.client.index') }}" class="active-bread">@lang('site.clients')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('admin.get.client.show', ['id'=>$row->id]) }}" class="active-bread">@lang('site.client') {{ $row->id }}</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>@lang('site.show')</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->

<!-- END PAGE MESSAGE -->
@include('admin.msg._messages')

<!-- END PAGE HEADER-->
<div class="note note-info">
    <h3> @lang('site.show') (@lang('site.clients') / @lang('site.clientNum') {{ $row->id }}) </h3>
</div>


<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
       
            <div class="portlet-body">

                <table class="table table-striped table-bordered table-hover table-checkable table-sort order-column column" id="order-data-table">
                    <tr>
                        <th> @lang('site.name') </th>
                        <td> {{ $row->name }} </td>
                    </tr>
                    <tr>
                        <th> @lang('site.email') </th>
                        <td> 
                            @if($row->email !== null)
                                {{ $row->email }} 
                            @else 
                                <div class="bold-red-text"> This field is empty </div>
                            @endif
                        </td>
                    </tr>
                    <tr>
                      <th> @lang('site.mobile') </th>
                      <td> 
                        {{ $row->mobile1 }} 
                        @if($row->mobile2 !== null)
                          , {{ $row->mobile2 }}
                        @endif
                      </td>
                    </tr>
                    <tr>
                        <th> @lang('site.address') </th>
                        <td> 
                            @if($row->address !== null)
                                {{ $row->address }} 
                            @else 
                                <div class="bold-red-text"> This field is empty </div>
                            @endif
                        </td>
                    </tr>    
                    <tr>
                        <th> @lang('site.status') </th>
                        <td> {{ $row->status }} </td>
                    </tr>
                    <tr>
                        <th> @lang('site.date') </th>
                        <td> 
                            {{ date("y-m-d", strtotime($row->created_at)) }}    
                        </td>
                    </tr>
                    <tr>
                        <th> @lang('site.moreInfo') </th>
                        <td> 
                            @if($row->more_info !== null)
                                {{ $row->more_info }} 
                            @else 
                                <div class="bold-red-text"> This field is empty </div>
                            @endif
                        </td>
                    </tr>
                </table>

                <br>

                @if($row->orders->count() >= 1)
                <table class="table table-striped table-bordered table-hover table-checkable table-sort order-column column" id="sample_1">
                    <thead>
                        <tr>
                            <th> @lang('site.code') </th>
                            <th> @lang('site.status') </th>
                            <th> @lang('site.showContent') </th>
                        </tr>
                    </thead>
                    <tbody class="connected-sortable droppable-area1">
                        
                        @foreach($row->orders as $order)
                        <tr>
                            <td class="text-center"> 
                                {{ $order->code }}
                            </td>
                            <td class="text-center"> 
                                {{ $order->status }} 
                            </td>
                            <td class="text-center">
                                <a href="{{ route('admin.get.order.'. $order->status .'.show', ['id'=>$order->id]) }}" class="btn btn-info btn-sm" title="@lang('site.show')">
                                    <i class="fa fa-eye"></i>
                                </a> 
                            </td>

                            
                        </tr>
                        @endforeach

                    </tbody>
                
                </table>
                @endif
 
 
            </div>
        </div>
        <!-- END EXAMPLE TABLE PORTLET-->
    </div>
</div>
@endsection

 