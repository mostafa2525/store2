@extends('admin.main')

@section('style')
<!-- BEGIN PAGE LEVEL PLUGINS -->
<link href="{{aurl()}}/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
<link href="{{aurl()}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- END PAGE LEVEL PLUGINS -->
<link rel="stylesheet" type="text/css" href="//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css">
@endsection

@section('content')



<!-- BEGIN PAGE BAR -->
<div class="page-bar">


    <ul class="page-breadcrumb"> 
        <li>
            <a href="{{ route('admin.get.home.index') }}" class="active-bread">@lang('site.home')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('admin.get.client.index') }}" class="active-bread">@lang('site.clients')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>@lang('site.view')</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->



<!-- END PAGE MESSAGE -->
@include('admin.msg._messages')

<!-- END PAGE HEADER-->
<div class="note note-info">
    <h3> @lang('site.view') ( @lang('site.clients') ) </h3>
</div>


<div class="row">
    <div class="col-md-12">
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light bordered">
       
            <div class="portlet-body">
                <div class="table-toolbar">
                    <div class="row">
                        <div class="col-md-12">
                                <ul id="errors">
                                    
                                </ul>
                        </div>
                   
                    </div>
                </div>




                @if($clients->count())
                @if(checkPermission('clients-delete'))
               <form action="{{ route('admin.post.client.deleteMulti') }}" method="post" id="Form2">@csrf</form>
               @endif 
                    <table class="table table-striped table-bordered table-hover table-checkable table-sort order-column column" id="sample_1">
                            <div class="coverLoading"><img src="{{furl()}}/img/loading.gif" ></div>
                        <thead>
                            <tr>
                                @if(checkPermission('clients-delete'))
                                <th>
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" />
                                        <span></span>
                                    </label>
                                </th>
                                @endif

                                <th> @lang('site.name') </th>
                                <th> @lang('site.email') </th>
                                <th> @lang('site.mobile') </th>
                                <th> @lang('site.orders') </th>
                                <th> @lang('site.status') </th>
                                <th> @lang('site.actions') </th>

                                @if(checkPermission('clients-edit'))
                                    <th>@lang('site.changeStatus')</th>
                                @endif

                            </tr>
                        </thead>
                        <tbody class="connected-sortable">
                            
                            @foreach($clients as $row)
                            <tr class="odd gradeX "   id="row-no-{{ $row->id }}">
                                @if(checkPermission('clients-delete'))                                
                                <td class="text-center">
                                    <label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
                                        <input type="checkbox" class="checkboxes" name="deleteMulti[]" form="Form2" multiple value="{{ $row->id}}" />
                                        <span></span>
                                    </label>
                                </td>
                                @endif

                                <td class="text-center"> {{ $row->name }} </td>
                                <td class="text-center"> {{ $row->email }} </td>

                                <td class="text-center"> 
                                    @if($row->mobile1 !== null)
                                        {{ $row->mobile1 }} 
                                        @if($row->mobile2 !== null)
                                            <br />
                                            {{ $row->mobile2 }}
                                        @endif
                                    @else 
                                        <div class="bold-red-text">Not available</div>
                                    @endif
                                </td>

                                <td class="text-center"> 
                                    {{ $row->orders->count() }}
                                </td>
                                <td class="text-center" id="status-{{$row->id}}" > {{ $row->status }} </td>
                                

                                <td class="text-center">

                                    <a href="{{ route('admin.get.client.show', ['id'=>$row->id]) }}" class="btn btn-info btn-sm" title="@lang('site.show')">
                                        <i class="fa fa-eye"></i>
                                    </a>
                                
                                    @if(checkPermission('clients-delete'))
                                    <a href="{{ route('admin.get.client.delete', ['id'=>$row->id]) }}" class="conform-delete btn btn-danger btn-sm" title="@lang('site.delete')">
                                        <i class="fa fa-close"></i>
                                    </a>
                                    @endif
            
                                </td>

                                @if(checkPermission('clients-edit'))

                                <td class="text-center">         
                                    @if($row->status == "active")
                                    <form method="post" class="trust-form form-action" id="trust-{{$row->id }}" > 
                                        @csrf 
                                        <input type="hidden" name="id" value="{{$row->id }}">
                                    </form>

                                    <form method="post" class="block-form form-action" id="block-{{$row->id }}" > 
                                            @csrf 
                                            <input type="hidden" name="id" value="{{$row->id }}">
                                    </form>
                                        
                                    <button type="submit" class="btn btn-primary btn-sm" id="btn-formtrust-{{$row->id}}" onclick="return confirm(' are you sure to trust this client !?');" form="trust-{{$row->id }}">
                                        @lang('site.trust')
                                    </button>

                                    <button type="submit" class="btn btn-warning btn-sm" id="btn-formblock-{{$row->id}}" onclick="return confirm(' are you sure to block this client !?');" form="block-{{$row->id }}">
                                        @lang('site.block')
                                    </button>
                                    @endif         
                                </td>
                                @endif

                            </tr>
                            @endforeach

                        </tbody>
                        <hr>
                        
                    
                    </table>
                            
                @if(checkPermission('clients-delete'))
                    </form>
                    <button type="submit" class="btn btn-danger btn-sm item-checked" form="Form2">@lang('site.deleteChecked')</button>
                @endif
            <!-- end form  -->

                @else
                        
                        @include('admin.msg.notFound')
                
                @endif



            </div>

    </div>
    <!-- END EXAMPLE TABLE PORTLET-->
</div>
</div>



@endsection



@section('script')

   

<script src="{{aurl()}}/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="{{aurl()}}/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<!-- END PAGE LEVEL PLUGINS -->

<script type="text/javascript">


  
    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });


    $('.trust-form').each(function(){
        $(this).submit(function(e){
            e.preventDefault();
            var formData  = new FormData(jQuery(this)[0]);
            var id = parseInt(formData.get("id"));

        var  el = $(this);
            
            $.ajax({
                type:'POST',
                url:"{{route('admin.post.client.trust')}}",
                data:formData,
                contentType: false,
                processData: false,
                beforeSend:function()
                {
                    $(".coverLoading").css("display","block");
                },
                success:function(data)
                {
                    $(".coverLoading").css("display","none");
                    $('#status-'+id).text("trust");
                    $('#btn-formblock-'+id).remove();
                    $('#btn-formtrust-'+id).remove();
                    $("#errors").html('');
                    $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")

                },
                error: function(xhr, status, error) 
                {
                $("#errors").html('');
                $.each(xhr.responseJSON.errors, function (key, item) 
                {
                    $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
                });
                }

            });
        })
    })


    
    $('.block-form').each(function(){
        $(this).submit(function(e){
            e.preventDefault();
            var formData  = new FormData(jQuery(this)[0]);
            var id = parseInt(formData.get("id"));

        var  el = $(this);
            
            $.ajax({
                type:'POST',
                url:"{{route('admin.post.client.block')}}",
                data:formData,
                contentType: false,
                processData: false,
                beforeSend:function()
                {
                    $(".coverLoading").css("display","block");
                },
                success:function(data)
                {
                    $(".coverLoading").css("display","none");
                    $('#status-'+id).text("block");
                    $('#btn-formblock-'+id).remove();
                    $('#btn-formtrust-'+id).remove();
                    $("#errors").html('');
                    $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")

                },
                error: function(xhr, status, error) 
                {
                $("#errors").html('');
                $.each(xhr.responseJSON.errors, function (key, item) 
                {
                    $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
                });
                }

            });
        })
    })









</script>




@endsection