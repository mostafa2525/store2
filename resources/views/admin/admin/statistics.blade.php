@extends('admin.main')



@section('content')



<!-- BEGIN PAGE BAR -->
<div class="page-bar">


    <ul class="page-breadcrumb">

    </ul>

</div>
<!-- END PAGE BAR -->



<!-- BEGIN PAGE TITLE-->
<h1 class="page-title"> @lang('site.adminOrders')  </h1>
<!-- END PAGE TITLE-->



<!-- END PAGE HEADER-->
<div class="note note-info">
   <!-- END PAGE HEADER-->
                        <div class="row widget-row">


                            <div class="col-sm-12">
                                <table class="table table-striped table-bordered table-hover table-checkable table-sort order-column column" id="order-data-table">
                                    <tr>
                                        <th> @lang('site.name') </th>
                                        <td> {{ $row->name }} </td>
                                    </tr>
                                    <tr>
                                        <th> @lang('site.email') </th>
                                        <td> 
                                            {{ $row->email }} 
                                       
                                        </td>
                                    </tr>
                               
                                </table>
                            </div>

                            <hr>
                            <hr>
                            <hr>







                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading"> @lang('site.totalOrders') </h4>
                                    <div class="widget-thumb-wrap">
                                        <i class="widget-thumb-icon bg-red fa fa-sort"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-body-stat" >{{$totalOrders}}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>




                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading"> @lang('site.todayOrders') </h4>
                                    <div class="widget-thumb-wrap">
                                        <i class="widget-thumb-icon bg-yellow fa fa-sort"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-body-stat" >{{$todayOrders}}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>




                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading"> @lang('site.weekOrders') </h4>
                                    <div class="widget-thumb-wrap">
                                        <i class="widget-thumb-icon bg-green fa fa-sort"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-body-stat" >{{$weekOrders}}</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>


                            <div class="col-md-12">
                            </div>









                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading">@lang('site.shippingOrders')</h4>
                                    <div class="widget-thumb-wrap">
                                        <a href="{{ route('admin.get.admin.statusOrder',[$row->id,'shipping']) }}">
                                        <i class="widget-thumb-icon bg-blue fa fa-truck"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-subtitle">@lang('site.shipping')</span>
                                            <span class="widget-thumb-body-stat" >{{$shippingOrders}}</span>
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>




                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading">@lang('site.acceptedOrders')</h4>
                                    <div class="widget-thumb-wrap">
                                        <a href="{{ route('admin.get.admin.statusOrder',[$row->id,'accepted']) }}">
                                        <i class="widget-thumb-icon bg-blue fa fa-check"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-subtitle">@lang('site.accepted')</span>
                                            <span class="widget-thumb-body-stat" >{{$acceptedOrders}}</span>
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>


                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading">@lang('site.refusedOrders')</h4>
                                    <div class="widget-thumb-wrap">
                                        <a href="{{ route('admin.get.admin.statusOrder',[$row->id,'refused']) }}">
                                        <i class="widget-thumb-icon bg-blue fa fa-times"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-subtitle">@lang('site.refused')</span>
                                            <span class="widget-thumb-body-stat" >{{$refusedOrders}}</span>
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>



                            <div class="col-md-3">
                                <!-- BEGIN WIDGET THUMB -->
                                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                    <h4 class="widget-thumb-heading"> @lang('site.canceledOrders') </h4>
                                    <div class="widget-thumb-wrap">
                                        <a href="{{ route('admin.get.admin.statusOrder',[$row->id,'canceled']) }}">
                                        <i class="widget-thumb-icon bg-purple fa fa-ban"></i>
                                        <div class="widget-thumb-body">
                                            <span class="widget-thumb-subtitle">@lang('site.canceled')</span>
                                            <span class="widget-thumb-body-stat" >{{$canceledOrders}}</span>
                                        </div>
                                        </a>
                                    </div>
                                </div>
                                <!-- END WIDGET THUMB -->
                            </div>

                        </div>


                        <div class="row">
                            <div class="col-sm-12">
                                @if(@isset($orders))
                                    @if($orders->count())


                                    <h3>@lang('site.status') : {{$status}}</h3>
                              
                     
                                   <table class="table table-striped table-bordered table-hover table-checkable table-sort order-column column" id="sample_1">
                                            <thead>
                                                <tr>
                                                    <th> @lang('site.code') </th>
                                                    <th> @lang('site.name') </th>
                                                    <th> @lang('site.email') </th>
                                                    <th> @lang('site.mobile') </th>
                                                    <th> @lang('site.status') </th>
                                                    <th> @lang('site.actions') </th>


                                                </tr>
                                            </thead>
                                            <tbody class="connected-sortable droppable-area1">
                                                
                                                @foreach($orders as $order)
                                                <tr class="odd gradeX draggable-item" id="row-no-{{ $order->id }}">
                                                   
                                                    <td class="text-center"> 
                                                        {{ $order->code }} 
                                                    </td>
                                                    <td class="text-center"> 
                                                        @if($order->name !== null)
                                                            {{ $order->name }}
                                                        @else 
                                                            <div class="bold-red-text">Not available</div> 
                                                        @endif
                                                    </td>
                                                  
                                                    <td class="text-center"> 
                                                        @if($order->email !== null)
                                                            {{ $order->email }}
                                                        @else 
                                                            <div class="bold-red-text">Not available</div> 
                                                        @endif 
                                                    </td>

                                                    <td class="text-center"> 
                                                        @if($order->mobile !== null)
                                                            {{ $order->mobile }}
                                                        @else 
                                                            <div class="bold-red-text">Not available</div> 
                                                        @endif 
                                                    </td>

                                                    <td class="text-center"> 
                                                        {{ $order->status }} 
                                                    </td>

                                                    <td class="text-center">

                                                        <a href="{{ route('admin.get.order.accepted.show', ['id'=>$order->id]) }}" class="btn btn-info btn-sm" title="@lang('site.show')">
                                                            <i class="fa fa-eye"></i>
                                                        </a>

                                                    </td>
                                                    
                                                
                                                                
                                                </tr>
                                                @endforeach

                                            </tbody>
                                            <hr>
                                            
                                        
                                        </table>

                                        <div class="text-center">
                                            {{ $orders->appends(request()->query())->links() }}
                                        </div>
                                                
                                    
                               
                                  
                                        
                                  
                                <!-- end form  -->

                                    @else
                                            
                                            @include('admin.msg.notFound')
                                    
                                    @endif
                                    @endif
                            </div>
                            
                        </div>
</div>



@endsection



