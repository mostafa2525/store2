

        <div class="form-body row">


          <div class="col-sm-12">
            <div>
              <hr>
                <h3>@lang('site.collection6')</h3>
              <hr>
            </div>
          </div>




          <div class="col-sm-12">
               <div class="form-group">
                        <label> @lang('site.title') </label>

                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>
                        <input type="text" name="collection6_title" 
                        class="form-control input-circle-right count-text-meta-title"  
                        value="{{ json_data($collections,'collection6_title') }}"  
                        > 
                    </div>
                </div>
           </div>




            <div class="col-sm-12">
               <div class="form-group">
                        <label> @lang('site.link') </label>

                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>

                        <input type="text" name="collection6_link" 
                        class="form-control input-circle-right count-text-meta-title"  
                        value="{{ json_data($collections,'collection6_link') }}"  
                        > 
                    </div>
                </div>
           </div>

           <div class="col-sm-12">
               <div class="form-group">
                    <label> @lang('site.image') (270 * 250) </label>
                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-image"></i>
                        </span>
                        <input type="file" name="collectionImg6" accept="image/*"  class="form-control input-circle-right"  > </div>
                </div>
           </div>












		 



        </div>
















