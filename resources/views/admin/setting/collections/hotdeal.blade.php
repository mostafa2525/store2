

        <div class="form-body row">


          <div class="col-sm-12">
            <div>
              <hr>
                <h3>@lang('site.hotdeal')</h3>
              <hr>
            </div>
          </div>




          <div class="col-sm-12">
               <div class="form-group">
                        <label> @lang('site.title') </label>

                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>
                        <input type="text" name="hotdeal_title" 
                        class="form-control input-circle-right count-text-meta-title"  
                        value="{{ json_data($collections,'hotdeal_title') }}"  
                        > 
                    </div>
                </div>
           </div>




           <div class="col-sm-12">
               <div class="form-group">
                        <label> @lang('site.desc') </label>

                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>
                        <input type="text" name="hotdeal_desc" 
                        class="form-control input-circle-right count-text-meta-title"  
                        value="{{ json_data($collections,'hotdeal_desc') }}"  
                        > 
                    </div>
                </div>
           </div>




            <div class="col-sm-12">
               <div class="form-group">
                        <label> @lang('site.link') </label>

                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>

                        <input type="text" name="hotdeal_link" 
                        class="form-control input-circle-right count-text-meta-title"  
                        value="{{ json_data($collections,'hotdeal_link') }}"  
                        > 
                    </div>
                </div>
           </div>

           <div class="col-sm-12">
               <div class="form-group">
                    <label> @lang('site.image') (560 * 600) </label>
                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-image"></i>
                        </span>
                        <input type="file" name="hotdealImg" accept="image/*"  class="form-control input-circle-right"  > </div>
                </div>
           </div>












		 



        </div>
















