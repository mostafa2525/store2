

        <div class="form-body row">


          <div class="col-sm-12">
            <div>
              <hr>
                <h3>@lang('site.collection3')</h3>
              <hr>
            </div>
          </div>




          <div class="col-sm-12">
               <div class="form-group">
                        <label> @lang('site.title') </label>

                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>
                        <input type="text" name="collection3_title" 
                        class="form-control input-circle-right count-text-meta-title"  
                        value="{{ json_data($collections,'collection3_title') }}"  
                        > 
                    </div>
                </div>
           </div>




            <div class="col-sm-12">
               <div class="form-group">
                        <label> @lang('site.link') </label>

                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>

                        <input type="text" name="collection3_link" 
                        class="form-control input-circle-right count-text-meta-title"  
                        value="{{ json_data($collections,'collection3_link') }}"  
                        > 
                    </div>
                </div>
           </div>

           <div class="col-sm-12">
               <div class="form-group">
                    <label> @lang('site.image') (270 * 250) </label>
                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-image"></i>
                        </span>
                        <input type="file" name="collectionImg3" accept="image/*"  class="form-control input-circle-right"  > </div>
                </div>
           </div>












		 



        </div>
















