



        <div class="form-body row">
           @csrf
           @include('admin.msg._errors')

          <div class="col-sm-12">
            <div>
              <hr>
                <h3>@lang('site.section1')</h3>
              <hr>
            </div>
          </div>

          <div class="col-sm-12">
               <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>
                        <input type="text" name="section1_btn" class="form-control input-circle-right count-text-meta-title"  
                        value="{{ json_data($site_content,'section1_btn') }}"  
                        > 
                    </div>
                </div>
           </div>


           
         
         




           <div class="col-sm-12">
            <div>
              <hr>
                <h3>@lang('site.section2')</h3>
              <hr>
            </div>
          </div>

          <div class="col-sm-12">
               <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>
                        <input type="text" name="section2_head" class="form-control input-circle-right count-text-meta-title" 
                        value="{{ json_data($site_content,'section2_head') }}"   
                        > 
                    </div>
                </div>
           </div>




            <div class="col-sm-12">
            <div>
              <hr>
                <h3>@lang('site.section3')</h3>
              <hr>
            </div>
          </div>

          <div class="col-sm-12">
               <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>
                        <input type="text" name="section3_head" class="form-control input-circle-right count-text-meta-title"
                         value="{{ json_data($site_content,'section3_head') }}"   
                        > 
                    </div>
                </div>
           </div>


   


           <div class="col-sm-12">
            <div>
              <hr>
                <h3>@lang('site.section4')</h3>
              <hr>
            </div>
          </div>

          <div class="col-sm-12">
               <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>
                        <input type="text" name="section4_head" class="form-control input-circle-right count-text-meta-title" 
                        value="{{ json_data($site_content,'section4_head') }}"   
                        > 
                    </div>
                </div>
           </div>






           <div class="col-sm-12">
            <div>
              <hr>
                <h3>@lang('site.section5')</h3>
              <hr>
            </div>
          </div>

          <div class="col-sm-12">
               <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>
                        <input type="text" name="section5_head" class="form-control input-circle-right count-text-meta-title" 
                        value="{{ json_data($site_content,'section5_head') }}"   
                        > 
                    </div>
                </div>
           </div>







           <div class="col-sm-12">
            <div>
              <hr>
                <h3>@lang('site.section6')</h3>
              <hr>
            </div>
          </div>

          <div class="col-sm-12">
               <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>
                        <input type="text" name="section6_head" class="form-control input-circle-right count-text-meta-title" 
                        value="{{ json_data($site_content,'section6_head') }}"   
                        > 
                    </div>
                </div>
           </div>




           <div class="col-sm-12">
            <div>
              <hr>
                <h3>@lang('site.section7')</h3>
              <hr>
            </div>
          </div>

          <div class="col-sm-12">
               <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>
                        <input type="text" name="section7_head" class="form-control input-circle-right count-text-meta-title" 
                        value="{{ json_data($site_content,'section7_head') }}"   
                        > 
                    </div>
                </div>
           </div>







           <div class="col-sm-12">
            <div>
              <hr>
                <h3>@lang('site.section8')</h3>
              <hr>
            </div>
          </div>

          <div class="col-sm-12">
               <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>
                        <input type="text" name="section8_head" class="form-control input-circle-right count-text-meta-title" 
                        value="{{ json_data($site_content,'section8_head') }}"   
                        > 
                    </div>
                </div>
           </div>


           <div class="col-sm-12">
               <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>
                        <input type="text" name="section8_desc" class="form-control input-circle-right count-text-meta-title" 
                        value="{{ json_data($site_content,'section8_desc') }}"   
                        > 
                    </div>
                </div>
           </div>



           <div class="col-sm-12">
               <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>
                        <input type="text" name="section8_placeholder" class="form-control input-circle-right count-text-meta-title" 
                        value="{{ json_data($site_content,'section8_placeholder') }}"   
                        > 
                    </div>
                </div>
           </div>




           <div class="col-sm-12">
               <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-addon input-circle-left">
                            <i class="fa fa-text-width"></i>
                        </span>
                        <input type="text" name="section8_btn" class="form-control input-circle-right count-text-meta-title" 
                        value="{{ json_data($site_content,'section8_btn') }}"   
                        > 
                    </div>
                </div>
           </div>

























        </div>

















