@extends('admin.main')



@section('content')



<!-- BEGIN PAGE BAR -->
<div class="page-bar">


    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('admin.get.home.index') }}" class="active-bread">@lang('site.home')</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>@lang('site.settings')</span>
        </li>
    </ul>

</div>
<!-- END PAGE BAR -->


<!-- END PAGE MESSAGE -->
@include('admin.msg._messages')


<!-- END PAGE HEADER-->
<div class="note note-info">
    <h3> @lang('site.baseSetting') ( @lang('site.settings') ) </h3>
</div>


    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN SAMPLE FORM PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-cogs fa-4x"></i> @lang('site.siteContent') </div>
                       
                        </div>
                        <div class="portlet-body">
                            <div class="row">
                                <div class="col-md-3 col-sm-3 col-xs-3">
                                  <ul class="nav nav-tabs tabs-left">
                                      
                                      

                                      <li class="active">
                                          <a href="#tab_6_10" data-toggle="tab"> @lang('site.topBarData')  
                                           </a>
                                      </li>

                                      <li class="">
                                          <a href="#tab_6_11" data-toggle="tab"> @lang('site.links')  
                                           </a>
                                      </li>

                                      <li>
                                          <a href="#tab_6_12" data-toggle="tab"> @lang('site.home') 
                                          </a>
                                      </li>


                                       <li>
                                          <a href="#tab_6_13" data-toggle="tab"> @lang('site.categoriesPage') 
                                          </a>
                                      </li>

                                      <li>
                                          <a href="#tab_6_14" data-toggle="tab"> @lang('site.subCategoriesPage') 
                                          </a>
                                      </li>


                                      <li>
                                          <a href="#tab_6_15" data-toggle="tab"> @lang('site.productPage') 
                                          </a>
                                      </li>

                                      <li>
                                          <a href="#tab_6_16" data-toggle="tab"> @lang('site.aboutPage') 
                                          </a>
                                      </li>

                                      <li>
                                          <a href="#tab_6_17" data-toggle="tab"> @lang('site.contactPage') 
                                          </a>
                                      </li>

                                      <li>
                                          <a href="#tab_6_18" data-toggle="tab"> @lang('site.loginPage') 
                                          </a>
                                      </li>

                                     {{--<li>
                                          <a href="#tab_6_19" data-toggle="tab"> @lang('site.registerPage') 
                                          </a>
                                      </li>--}}
                                      

                                       <li>
                                          <a href="#tab_6_20" data-toggle="tab"> @lang('site.cartPage') 
                                          </a>
                                      </li>



                                      <li>
                                          <a href="#tab_6_21" data-toggle="tab"> @lang('site.checkoutPage') 
                                          </a>
                                      </li>


                                       <li>
                                          <a href="#tab_6_22" data-toggle="tab"> @lang('site.profilePage') 
                                          </a>
                                      </li>


                                      <li>
                                          <a href="#tab_6_23" data-toggle="tab"> @lang('site.editProfilePage') 
                                          </a>
                                      </li>


                                    <li>
                                          <a href="#tab_6_24" data-toggle="tab"> @lang('site.changePassword') 
                                          </a>
                                      </li>

                                      <li>
                                          <a href="#tab_6_25" data-toggle="tab"> @lang('site.ordersPage') 
                                          </a>
                                      </li>






                                      <li >
                                          <a href="#tab_6_2" data-toggle="tab"> @lang('site.blog') 
                                          </a>
                                      </li>



                               
                               </ul>
                                </div>
                              
                                <div class="col-md-9 col-sm-9 col-xs-9">
                                  <form role="form" method="post" action="{{ route('admin.post.settings.homeSiteContent') }}" enctype="multipart/form-data" class="row">
                                    <div class="tab-content">
                                        
                                        @csrf
                                        <div class="tab-pane active" id="tab_6_10">
                                            @include('admin.setting.inc._topBar')
                                        </div>

                                        <div class="tab-pane" id="tab_6_11">
                                            @include('admin.setting.inc._links')
                                        </div>

                                        <div class="tab-pane " id="tab_6_12">
                                           @include('admin.setting.inc._home')
                                        </div>

                                        <div class="tab-pane " id="tab_6_13">
                                           @include('admin.setting.inc._cat')
                                        </div>


                                        <div class="tab-pane " id="tab_6_14">
                                           @include('admin.setting.inc._sub')
                                        </div>

                                        <div class="tab-pane " id="tab_6_15">
                                           @include('admin.setting.inc._product')
                                        </div>



                                        <div class="tab-pane " id="tab_6_16">
                                           @include('admin.setting.inc._about')
                                        </div>

                                        <div class="tab-pane " id="tab_6_17">
                                           @include('admin.setting.inc._contact')
                                        </div>

                                        <div class="tab-pane " id="tab_6_18">
                                           @include('admin.setting.inc._login')
                                        </div>


                                        {{--<div class="tab-pane " id="tab_6_19">
                                           @include('admin.setting.inc._register')
                                        </div>--}}

                                        <div class="tab-pane " id="tab_6_20">
                                           @include('admin.setting.inc._cart')
                                        </div>
                                        <div class="tab-pane " id="tab_6_21">
                                           @include('admin.setting.inc._checkout')
                                        </div>

                                        <div class="tab-pane " id="tab_6_22">
                                           @include('admin.setting.inc._profile')
                                        </div>


                                        <div class="tab-pane " id="tab_6_23">
                                           @include('admin.setting.inc._editProfile')
                                        </div>

                                         <div class="tab-pane " id="tab_6_24">
                                           @include('admin.setting.inc._changePassword')
                                        </div>

                                        <div class="tab-pane " id="tab_6_25">
                                           @include('admin.setting.inc._orders')
                                        </div>

  




                                        <div class="tab-pane" id="tab_6_2">
                                           @include('admin.setting.inc._blog')
                                        </div>





                                    </div>
                                       <div class="form-actions">
                                          <button type="submit" class="btn blue">@lang('site.save')</button>
                                      </div>
                                    </form>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection


@section('script')

    <script src="{{ aurl() }}/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
    <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{ aurl() }}/global/plugins/bootstrap-tabdrop/js/bootstrap-tabdrop.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->

@endsection


