<div class="row">
	<div class="col-md-12 col-xs-12">
		<div class="view_die_account">
			<h3> {{ json_data($site_content,'password_title1') }} </h3>
			<hr>
			<form method="post"   id="edit-password">
				<div class="col-md-12">
                    <ul id="errors"></ul>
                </div>
				@csrf
				<p class="required">*Required field</p>

				<p class="form-row">
					<label> {{ json_data($site_content,'password_title2') }} <span class="required">*</span></label>
					<input type="password" name="oldPassword" required maxlength="100"  />
				</p>

				<p class="required">*Required field</p>
				
				<p class="form-row">
					<label>{{ json_data($site_content,'password_title3') }}<span class="required">*</span></label>
					<input type="password" name="password" required maxlength="100"  />
				</p>


				<p class="required">*Required field</p>
				
				<p class="form-row">
					<label>{{ json_data($site_content,'password_title4') }}<span class="required">*</span></label>
					<input type="password" name="confirm-password" required maxlength="100"  />
				</p>


			
				<button title="Save" class="btn btn-default button button-small" type="submit">
					<span>
						  {{ json_data($site_content,'password_title5') }}
						<i class="fa fa-chevron-right"></i>
					</span>
				</button>
			</form>
		</div>
	</div>
	
</div>