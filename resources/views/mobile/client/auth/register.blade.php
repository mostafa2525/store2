@extends('mobile.main')


@section('content')





<div class="clearfix"></div>
	<div class="row st-col">
		<h3 class="num_pro">
			{{ json_data($scm,'Mregister_title1') }}
		</h3>
	</div> <!------- end number products --------->

	<div class="clearfix"></div>

	<div class="row">
		<div class="col s12 m12 l12">
			<div class="page-about">

				<div class="col s12 m12 l12 box-form">
					<div class="col-sm-12">
						@include('_messages')
					</div>
					<form method="post" class="create-account-form" id="reg-form" action="{{route('front.client.post.auth.doRegister')}}">
					
								@csrf

								<p class="form-row">
									<label>  {{ json_data($scm,'Mregister_title2') }}   </label>
									<input type="text" name="name" value="{{old('name')}}" required maxlength="100">
									
								</p>

								<p class="form-row">
									<label> {{ json_data($scm,'Mregister_title3') }}  </label>
									<input type="email" name="email" value="{{old('email')}}" required maxlength="100">
								</p>

								<p class="form-row">
									<label>  {{ json_data($scm,'Mregister_title4') }}   </label>
									<input type="tel" name="mobile1" value="{{old('email')}}" required maxlength="100">
								</p>

								<p class="form-row">
									<label>  {{ json_data($scm,'Mregister_title5') }}  </label>
									<input type="password" name="password" required maxlength="50">
								</p>

								<p class="form-row">
									<label>  {{ json_data($scm,'Mregister_title6') }}    </label>
									<input type="password" name="confirm-password" required maxlength="50">
								</p>

								<div class="submit col s12 m12">					
									<button name="submitcreate" id="submitcreate" type="submit" class="color-2 col s12 m12">
										<span class="col s12 m12">
											<i class="fa fa-user"></i>
											{{ json_data($scm,'Mregister_title7') }} 
										</span>
									</button>
								</div>		
							</form>
				</div>

			</div>
		</div>
	</div>

		




@endsection

@section('script')

<script type="text/javascript">
  
    $('#reg-form').parsley();

 </script>

@endsection