@extends('mobile.main')


@section('content')





    <div class="row">
    <div class="col s12 m12 l12">
      <div class="page-about">

        <div class="col s12 m12 l12 box-form">
           <h1 class="heading-title text-center" style="font-size: 20px;">@lang('frontSite.typeYourEmail')</h1>
          <form method="post" class="login-form" id="forgot-form" >
            <div class="coverLoading" style=""><img src="{{furl()}}/img/loading.gif"></div>
             @csrf
              <div class="col-md-12">
                  <ul id="errors"></ul>
              </div>
            <div class="input-field col s12">
              <input id="last_name1" type="email" name="email" value="{{old('email')}}" required maxlength="100" >
              <label for="last_name1"> {{ json_data($scm,'Mlogin_title2') }} </label>
            </div>


            <div class="input-field col s12">
              <button class="waves-effect waves-light right btn color-2"  name="SubmitLogin" id="submitlogin" type="submit">
                <span><i class="fa fa-lock"></i> @lang('frontSite.send')  </span>
              </button>
            </div>


          </form>
        </div>

      </div>
    </div>
  </div>



		<!-- ACOOUNT FROM AREA END -->



		




@endsection


@section('script')
<script type="text/javascript">
  
    $('#forgot-form').parsley();


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#forgot-form").submit(function(e) 
    {
        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#forgot-form')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.client.post.auth.forgotPassword')}}",
           data:formData,
           contentType: false,
           processData: false,
           beforeSend:function()
           {
           	   $(".coverLoading").css("display","block")
           	   $("#price_filter input[type='submit']").css("display","none");
           },
           success:function(data)
           {
             $("#errors").html('');
             $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
             $("input").val("");
             $(".coverLoading").css("display","none")

           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
             $(".coverLoading").css("display","none")

            }

        });

	});

</script>



@endsection