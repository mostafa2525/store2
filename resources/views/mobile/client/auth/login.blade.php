@extends('mobile.main')


@section('content')
	

<div class="clearfix"></div>
	<div class="row st-col">
		<h3 class="num_pro">
			{{ json_data($scm,'Mlogin_title1') }}
		</h3>
	</div> <!------- end number products --------->

	<div class="clearfix"></div>

	<div class="row">
		<div class="col s12 m12 l12">
			<div class="page-about">

				<div class="col s12 m12 l12 box-form">
					<form method="post"  action="{{route('front.client.post.auth.doLogin')}}">
						<div class="col s12 m12">
							@include('_messages')
							@csrf
						</div>
						<div class="input-field col s12">
							<input id="last_name1" type="email" name="email" value="{{old('email')}}" required maxlength="100" >
							<label for="last_name1"> {{ json_data($scm,'Mlogin_title2') }} </label>
							<input type="hidden" name="login" value="login">
						</div>

						<div class="input-field col s12">
							<input id="last_name2" type="password" name="password" required maxlength="50" >
							<label for="last_name2"> {{ json_data($scm,'Mlogin_title3') }}  </label>
						</div>


						<div class="input-field col s12">
							<button class="waves-effect waves-light right btn color-2"  name="SubmitLogin" id="submitlogin" type="submit">
								<span><i class="fa fa-lock"></i>  {{ json_data($scm,'Mlogin_title4') }}    </span>
							</button>
						</div>

						<div class="input-field col s12 m12 right-align">
							<a class="waves-effect waves-light right btn color-2" 
							href="{{route('front.client.get.auth.forgot')}}">   {{ json_data($scm,'Mlogin_title5') }}    </a>
	
						</div>


						<div class="input-field col s12 m12 right-align">
							<a class="waves-effect waves-light right btn color-2" href="{{route('front.client.get.auth.register')}}">  {{ json_data($scm,'Mlogin_title6') }}      </a>
						</div>

					</form>
				</div>

			</div>
		</div>
	</div>


@endsection

@section('script')
<script type="text/javascript">
  
    $('#log-form').parsley();

    $('#reg-form').parsley();

 </script>

@endsection