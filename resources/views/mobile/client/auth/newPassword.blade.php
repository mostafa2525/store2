@extends('mobile.main')


@section('content')


		<!-- ACOOUNT FROM AREA START -->
		<section class="login-area">
			<div class="container">
				<div class="row">
					<div class="account-details">
					
						<div class="col-lg-6 col-md-6 col-sm-6 col-sm-offset-3">
							<form method="post" class="login-form" id="forgot-form">
								<div class="coverLoading" style=""><img src="{{furl()}}/img/loading.gif"></div>

								<h1 class="heading-title text-center" style="font-size: 20px;"> @lang('frontSite.newPassword')</h1>
								@csrf
								
								
								<p class="form-row">
									<label>@lang('frontSite.password')</label>
									<input type="password" name="password" required maxlength="50">
									<input type="hidden" name="base_token" value="{{$row->token}}" >
								</p>

								<p class="form-row">
									<label>@lang('frontSite.confirmPassword') </label>
									<input type="password" name="confirm-password" required maxlength="50">
								</p>
								<!-- <p class="lost-password form-group"><a rel="nofollow" href="#">Forgot your password?</a></p> -->
								<div class="total-price color-1 center color-2">				
									<button   type="submit" style="display: block; background: transparent; color: #fff; text-align: center; width:100%; ">
										@lang('frontSite.send')
									</button>
								</div>
                <br>
                <br>
                <br>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- ACOOUNT FROM AREA END -->



		




@endsection




@section('script')
<script type="text/javascript">
  
    $('#forgot-form').parsley();


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#forgot-form").submit(function(e) 
    {
        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#forgot-form')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.client.post.auth.newPassword')}}",
           data:formData,
           contentType: false,
           processData: false,
           beforeSend:function()
           {
           	   $(".coverLoading").css("display","block")
           	   $("#price_filter input[type='submit']").css("display","none");
           },
           success:function(data)
           {
           	 if(data.success)
           	 {
           	 	location.href = "{{route('front.client.get.auth.login')}}";
           	 }
           	 if(data.error)
           	 {
           	 	 location.href = "{{route('front.get.home.index')}}";
           	 }

           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
             $(".coverLoading").css("display","none")

            }

        });

	});

</script>



@endsection