@extends('mobile.main')


@section('content')


	<!-- BANNER AREA STRAT -->
		<section class="bannerhead-area">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner-heading" style="padding: 20px 0;">
							<h4> @lang('frontSite.orderDetails') </h4>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- BANNER AREA END -->


		<div class="row st-col">
			<div class="col s12 m12 l12">
				<div class="slide-home">
					<!-- <span class="icon_wish"><a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a></span> -->
					<div class="slider">
						<ul class="slides">
							<li>
								<img src="{{getImage(PRODUCT_PATH.$rowOrder->product->img)}}"> <!-- random image -->
							</li>							
						</ul>
					</div>
				</div>
			</div>
		</div>
		<hr>
		<br>
		<div class="clearfix"></div>

		<div class="row st-col">
			<div class="dit_view_pro text-center">
				<h4> {{$rowOrder->product->name}} </h4>
				<br>
				<h2 class="text-center">  @lang('frontSite.price') : EGP {{ $rowOrder->price_at_this_time}}</h2>
				<h2 class="text-center">  @lang('frontSite.quantity') : {{ $rowOrder->quantity}}</h2>
				<h2 class="text-center">  @lang('frontSite.status') :  {{ $rowOrder->status}}</h2>

			</div>
		</div>
		<hr>

		<div class="col-sm-12">
			<div class="contDetails text-center">
				<h4> <strong> {{ json_data($site_content,'orders_title11') }} : </strong>  {{$rowContent->address}}</h4>
				<h4> <strong> {{ json_data($site_content,'orders_title10') }} : </strong>
				 @if($rowContent->gov) {{$rowContent->gov->name}} @endif
				</h4>
				<h4> <strong> {{ json_data($site_content,'orders_title12') }} : </strong>
				@if($rowContent->status_change_date) 
					{{ date('Y-m-D',strtotime($rowContent->status_change_date))}} 
				@endif
				</h4>
			</div>
		</div>




		<!-- My Account Area Start -->
		<div class="my-account-area">
			<div class="container">
				<div class="row">
					<div class="addresses-lists">
							


					</div>
				</div>

			</div>
		</div>
		<!-- My Account Area End -->












@endsection

@section('script')

<script type="text/javascript">
  
    $('#editInfo').parsley();

 </script>

<script type="text/javascript">
  
    $('#edit-password').parsley();


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#edit-password").submit(function(e) 
    {
        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#edit-password')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.client.post.auth.editPassword')}}",
           data:formData,
           contentType: false,
           processData: false,
           success:function(data)
           {
           	 if(data.errors)
           	 {
           	 	$("#errors").html('');
	            $("#errors").append("<li class='alert alert-danger text-center'>"+data.errors+"</li>")
           	 }
           	 else
           	 {

	             $("#errors").html('');
	             $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
           	 }

           	   $('#edit-password .input-text').find("input").val("");
	           $('#edit-password .input-message').find("textarea").val("");
           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
            }

        });

	});

</script>



@endsection


