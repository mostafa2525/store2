@extends('mobile.main')


@section('content')


	<div class="clearfix"></div>
	<div class="row st-col">
		<h3 class="tit_my_acciunt">
			 <b>{{ json_data($scm,'Mprofile_title1') }}</b>
		</h3>
	</div> <!------- end number products --------->

	<div class="clearfix"></div>

		<div class="row st-col">
			<div class="col s12 m 12">
				<div class="my_account">
					<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingOne">
								<h4 class="panel-title">
									<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" @if(Request::segment(4) =='info') aria-expanded="true" @endif aria-controls="collapseOne">
									{{ json_data($scm,'Mprofile_title2') }}
									</a>
								</h4>
							</div>
							<div id="collapseOne" class="panel-collapse collapse @if(Request::segment(4) =='info') in @endif" role="tabpanel" aria-labelledby="headingOne">
								<div class="panel-body">
									<div class="row">
										<div class="col s12 m12">
											<div class="view_die_account">
												<h3>  	{{ json_data($scm,'Mprofile_title3') }}    </h3>
												<p>@if(clientAuth()->user()->address)
													{{clientAuth()->user()->address}}
													@else
														There Is No Default Address Untl Now
													@endif
												</p>

												<h3>  {{ json_data($scm,'Mprofile_title4') }}     </h3>
												<p>
													{{clientAuth()->user()->email}}
													
												</p>


												<h3> {{ json_data($scm,'Mprofile_title5') }}  </h3>
												<p>
													{{clientAuth()->user()->mobile1}}
													
												</p>

												<h3>  {{ json_data($scm,'Mprofile_title6') }}  </h3>
												<p>
											
													@if(clientAuth()->user()->more_info)
													{{clientAuth()->user()->more_info}}
													@else
														لا توجد معلومات 
													@endif
												</p>

												<h3> </h3>

												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingTwo">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" @if(Request::segment(4) =='my-orders') aria-expanded="true" @endif  aria-controls="collapseTwo">
										{{ json_data($scm,'Morders_title1') }}
									</a>
								</h4>
							</div>
							<div id="collapseTwo" class="panel-collapse collapse @if(Request::segment(4) =='my-orders') in @endif" role="tabpanel" aria-labelledby="headingTwo">
								<div class="panel-body">
									<div class="row">
										<div class="col s12 m12">

											@foreach(clientAuth()->user()->orders as $ord)
												@foreach($ord->content as $co)
												<div class="col s12 m12">
													<div class="item-silder item_list">
														<div class="img-pr">
															<a href="{{route('front.client.get.order.showDatailsM',$co->id)}}" style="width: 100%; height: 100%;" >
															<img class="responsive-img" src="{{getImage(PRODUCT_PATH.'small/'.$co->product->img)}}" />
															</a>
														</div>
														<div class="des-pr right-align">
															<h4>{{$co->product->category->name}}</h4>
															<a href="{{route('front.client.get.order.showDatailsM',$co->id)}}">{{$co->product->name}}</a>
															<label> EGP {{ $co->product->price}}  </label>
														</div>
													</div>
												</div>
												@endforeach
											@endforeach
								
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingThree">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
										{{ json_data($scm,'MEprofile_title1') }}
									</a>
								</h4>
							</div>
							<div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
								<div class="panel-body">
									<form method="post" class="create-account-form" id="reg-form" action="{{route('front.client.post.auth.editProfile')}}">
										@csrf
										@include('admin.msg._messages')

										<p class="form-row">
											<label> {{ json_data($scm,'MEprofile_title2') }}   ( * )</label>
											<input type="text" name="name" value="{{clientAuth()->user()->name}}" required maxlength="100">
											
										</p>

										<p class="form-row">
											<label> {{ json_data($scm,'MEprofile_title3') }}   ( * ) </label>
											<input type="text" name="address" value="{{clientAuth()->user()->address}}" required maxlength="300">
										</p>


										<p class="form-row">
											<label> {{ json_data($scm,'MEprofile_title4') }}  ( * )  </label>
											<input type="tel" name="mobile1" value="{{clientAuth()->user()->mobile1 }}" required maxlength="100" >
										</p>

										<p class="form-row">
											<label>  {{ json_data($scm,'MEprofile_title5') }}     ( optional ) </label>
											<input type="tel" name="mobile2" value="{{clientAuth()->user()->mobile2 }}"  maxlength="100" >
										</p>


										<p class="form-row">
											<label>  {{ json_data($scm,'MEprofile_title6') }}   ( optional )  </label>
											<input type="tel" name="more_info" value="{{clientAuth()->user()->more_info }}"  maxlength="100" >
										</p>

										<div class="submit">					
											<button name="submitcreate" id="submitcreate" type="submit" class="">
												<span>
													<i class="fa fa-user left"></i>
													{{ json_data($scm,'MEprofile_title7') }} 
												</span>
											</button>
										</div>		
									</form>
								</div>
							</div>
						</div>
						<div class="panel panel-default">
							<div class="panel-heading" role="tab" id="headingfor">
								<h4 class="panel-title">
									<a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefor" aria-expanded="false" aria-controls="collapsefor">
										 {{ json_data($scm,'Mpassword_title1') }}
									</a>
								</h4>
							</div>
							<div id="collapsefor" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfor">
								<div class="panel-body">
									<div class="row st-col">
										<div class="section_white">
											<div class="col s12 m12">
												<form method="post"   id="edit-password">
													<div class="col-md-12">
									                    <ul id="errors"></ul>
									                </div>
													@csrf
													<p class="required">* حقل مطلوب   </p>

													<p class="form-row">
														<label>  {{ json_data($scm,'Mpassword_title2') }}   <span class="required">*</span></label>
														<input type="password" name="oldPassword" required maxlength="100"  />
													</p>

													<p class="required">* حقل مطلوب   </p>
													
													<p class="form-row">
														<label> {{ json_data($scm,'Mpassword_title3') }}  <span class="required">*</span></label>
														<input type="password" name="password" required maxlength="100"  />
													</p>


													<p class="required">* حقل مطلوب   </p>
													
													<p class="form-row">
														<label> {{ json_data($scm,'Mpassword_title4') }}    <span class="required">*</span></label>
														<input type="password" name="confirm-password" required maxlength="100"  />
													</p>


												
													<button title="Save" class="btn btn-default button button-small" type="submit">
														<span>
															 {{ json_data($scm,'Mpassword_title5') }}
															<i class="fa fa-chevron-right"></i>
														</span>
													</button>
												</form>
											</div>
										
										</div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>













@endsection

@section('script')

<script type="text/javascript">
  
    $('#editInfo').parsley();

 </script>

<script type="text/javascript">
  
    $('#edit-password').parsley();


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#edit-password").submit(function(e) 
    {
        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#edit-password')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.client.post.auth.editPassword')}}",
           data:formData,
           contentType: false,
           processData: false,
           success:function(data)
           {
           	 if(data.errors)
           	 {
           	 	$("#errors").html('');
	            $("#errors").append("<li class='alert alert-danger text-center'>"+data.errors+"</li>")
           	 }
           	 else
           	 {

	             $("#errors").html('');
	             $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
           	 }

           	   $('#edit-password .input-text').find("input").val("");
	           $('#edit-password .input-message').find("textarea").val("");
           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
            }

        });

	});

</script>



@endsection


