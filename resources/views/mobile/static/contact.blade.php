@extends('mobile.main')


@section('content')


	


		<div class="clearfix"></div>
	<div class="row st-col">
		<h3 class="num_pro">
			{{ json_data($scm,'Mcontact_title1') }} 
		</h3>
	</div> <!------- end number products --------->

	<div class="clearfix"></div>

		<div class="row">
			<div class="des-page-contact">
			<div class="col s12 m12 l12">




				<span class="alarm_cart text-center">
					<img src="{{furl()}}/img/footer_contact_chat.png" alt="">
					<br>
						{{ json_data($scm,'Mcontact_title2') }} 
				</span>

				<br>


				</div>
				<div class="col s12 m12 l12 box-form no-padding">
					<form id="contact-form">
						@csrf
                        <div class="input-field col s12">
                            <ul id="errors"></ul>
                        </div>
						<div class="input-field col s12">
							<input id="last_name" type="text" class="validate" name="name">
							<label for="last_name"> {{ json_data($scm,'Mcontact_title3') }}  </label>
						</div>

						<div class="input-field col s12">
							<input id="last_name" type="text" class="validate" name="phone">
							<label for="last_name"> {{ json_data($scm,'Mcontact_title4') }}   </label>
						</div>

						<div class="input-field col s12">
							<input id="last_name" type="email" class="validate" name="email">
							<label for="last_name">  {{ json_data($scm,'Mcontact_title5') }}  </label>
						</div>

						<div class="input-field col s12">
							<textarea id="textarea1" class="materialize-textarea"  name="message"></textarea>
							<label for="textarea1">{{ json_data($scm,'Mcontact_title6') }} </label>
						</div>

						<div class="input-field col s12 right">
							<button class="waves-effect waves-light btn color-2" type="submit"> {{ json_data($scm,'Mcontact_title7') }}  </button>
						</div>
					</form>
				</div>
			</div>
		</div>








	




@endsection




@section('script')
<script type="text/javascript">
  
    $('#contact-form').parsley();


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#contact-form").submit(function(e) 
    {
        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#contact-form')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.post.contactus.sendMessage')}}",
           data:formData,
           contentType: false,
           processData: false,
           beforeSend:function()
           {
           		$("#submitMessage").attr("disabled",true);
           },
           success:function(data)
           {
             $("#errors").html('');
             $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
             $('.input-text').find("input").val("");
             $('.input-message').find("textarea").val("");
           		$("#submitMessage").attr("disabled",true);

           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
           		$("#submitMessage").attr("disabled",true);
              
            }

        });

	});

</script>



@endsection