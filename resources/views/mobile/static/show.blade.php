@extends('front2.main')

@section('style')
<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.css" />

<link type="text/css" rel="stylesheet" href="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials-theme-flat.css" />
@endsection
<?php $cover = json_data($site_content,'static_cover');  ?>
@section('content')

<!-- style="background: url('{{getImage(SETTINGS_PATH.$cover)}}'); background-repeat: no-repeat; background-size: cover;" -->

	<!-- BANNER AREA STRAT -->
		<section class="bannerhead-area" >
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="banner-heading">
							<h1> {{$row->name}} </h1>
						</div>
					</div>
				</div>
			</div>
		</section>
		<!-- BANNER AREA END -->


		<section class="latest-blog-area">
			<div class="container">
				<div class="text-center">
					<div class="section-titel" style="margin-top:100px;">
						<h3> {{$row->name}} </h3>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12 col-sm-12  xs-res-mrbtm">
						<div class="">
							
								<img src="{{getImage(STATIC_PAGE_PATH.$row->img)}}" alt="{{$row->name}}" style=" display: block; margin: auto;" />
							
							<div class="blog-content" style="margin: 50px; padding: 20px;">
									
								{!! $row->description !!}
								
							</div>



						</div>
					</div>
				
				</div>
			</div>
		</section>















@endsection

@section('script')

<script type="text/javascript" src="https://cdn.jsdelivr.net/jquery.jssocials/1.4.0/jssocials.min.js"></script>
<script>
    $("#share").jsSocials({
        shares: ["email", "twitter", "facebook", "googleplus", "linkedin", "pinterest", "stumbleupon"]
    });
</script>


@endsection