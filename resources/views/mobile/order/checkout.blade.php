@extends('mobile.main')

@section('content')









	<div class="clearfix"></div>
	<div class="row st-col">
		<h3 class="num_pro">
			<b>  {{ json_data($scm,'Mcheckout_title1') }} </b>
		</h3>
	</div> <!------- end number products --------->

	<div class="clearfix"></div>

	<div class="row">
		<div class="col s12 m12 l12">
			<div class="page-about">

				<div class="col s12 m12 l12 padd-left padd-right">
					<div class="des-page-checkout">
						<form id="orderForm">

							<ul id="errors">
								
							</ul>

							<div class="coverLoading" style=""><img src="{{furl()}}/img/loading.gif"></div>
							@csrf


							


							<div class="input-field col s12 box-form">
								<input id="last_name" type="text" name="name" value="{{clientAuth()->user()->name}}" required style="padding: 5px 10px;">
								<label for="last_name"> ( * )  {{ json_data($scm,'Mcheckout_title3') }} </label>
							</div>

							<div class="input-field col s12 box-form">
								<input id="last_name2" type="tel"  name="mobile" value="{{clientAuth()->user()->mobile1}}" required style="padding: 5px 10px;">
								<label for="last_name2">   ( * ) {{ json_data($scm,'Mcheckout_title4') }}    </label>
							</div>

							


							<div class="input-field col s12 box-form" style="    margin-bottom: 20px;">
								<textarea id="textarea2" value="{{clientAuth()->user()->address}}" class="materialize-textarea" name="address"  style="padding: 5px 10px;"></textarea>
								<label for="textarea2" class="">  ( * )  {{ json_data($scm,'Mcheckout_title5') }}  </label>
							</div>


							<div class="input-field col s12 box-form">
								<select class="browser-default" name="gov_id" required>
									<option value="">  ( * ) {{ json_data($scm,'Mcheckout_title2') }}  </option>
									 @foreach($govs as $gov)
									  <option value="{{$gov->id}}">{{$gov->name}}</option>
									  @endforeach
								</select>
							</div>

							<div class="input-field col s12 box-form">
								<input id="last_name4" type="text" name="city" required style="padding: 5px 10px;">
								<label for="last_name4"> ( * ) @lang('frontSite.city') </label>
							</div>


	

							<div class="row st-col">
								<div class="col s12 m12 l12 no-padding">

									<hr>
									<h3 class="text-center" style="color:#64B802; font-size: 18px;">
									<strong>ِ@lang('frontSite.paymentDleviry')</strong></h3>
									<hr>

									<!-- <div class="total-price color-1 center color-1">
										<a href="{{--route('front.get.cart.show')--}}">  {{-- json_data($scm,'Mcheckout_title6') --}}   </a>
									</div>


									<div class="total-price color-1 center color-4">   {{-- json_data($scm,'Mcheckout_title7') --}}   
									 <strong> EGP {{-- \Cart::getSubTotal() + $setting->price_charge--}}  </strong> </div> -->
									<div class="total-price center color-2">
										<button type="submit" class="color-4 btn-check">   {{ json_data($scm,'Mcheckout_title8') }}   </button>
									</div>
								</div>
							</div>


						</form>

					</div>
				</div>

			</div>
		</div>
	</div>
	








@endsection



@section('script')
<script type="text/javascript">
  
    $('#orderForm').parsley();


    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });

   

    $("#orderForm").submit(function(e) 
    {

        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#orderForm')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.post.order.sendOrder')}}",
           data:formData,
           contentType: false,
           processData: false,
           beforeSend:function()
           {
           	   $(".coverLoading").css("display","block")
           	   $("#orderForm input[type='submit']").css("display","none");
           },
           success:function(data)
           {
             $("#errors").html('');
             $("#errors").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
             $('input').val("");
             $("textarea").val("");
             $(".coverLoading").css("display","none")
           	 $("#price_filter input[type='submit']").css("display","block");

           	 alert(data.success)
           	 
           	 // $.toast({
             //        heading: 'Message',
             //        text: data.success,
             //        position: 'top-left',
             //        stack: false,
             //        icon: 'success',
             //        hideAfter: 3000 
             //    })

             setTimeout(function(){ location.href = "{{route('front.get.home.index')}}"}, 3000);
           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
             $(".coverLoading").css("display","none")
              
            }

        });

	});

</script>



@endsection
