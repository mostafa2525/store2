@extends('mobile.main')

@section('content')


	<div class="clearfix"></div>

	<div class="row st-col">
		<div class="col s12 m12 l12">
			<div class="slide-main">
				<div class="slider">
					<ul class="slides">
						@foreach($slider as $sl)
						<li>
							<a href="{{$sl->link}}">
							<img src="{{getImage(SLIDER_PATH.'small/'.$sl->img)}}"> <!-- random image -->
							</a>
						</li>
						@endforeach

					</ul>
				</div>
			</div>
		</div>
	</div> <!------- end slider --------->



	<div class="clearfix"></div>
	<div class="row st-col">
		<div class="all_item_home">
		<div class="col s12 m12 p-0">
			<h3 class="main_class"> {{ json_data($scm,'Mhome_title2') }} </h3>
		</div>

		@foreach($categories as $cat)
		<div class="col s3 m3 p-0">
			<div class="item_section_home">
				<a href="{{route('front.get.product.category',[$cat->slug])}}" alt="{{$cat->name}} " >
					<img src="{{getImage(CATEGORY_PATH.'small/'.$cat->img)}}" title="{{$cat->name}} ">
					<span> {{$cat->name}}  </span>
				</a>
			</div>
		</div>
		@endforeach

		</div>
	</div> <!------- end all item home --------->

	<div class="clearfix"></div>
	<div class="row st-col">
		<div class="col s12 m12">
			<div class="tit_offers">
				
			</div>
		</div>
	</div>
	<div class="clearfix"></div>


	<div class="row st-col">
		<div class="section_white">
			<?php $i=1; ?>
			@foreach($categories as $cat)
				<div class="row st-col">
					<div class="col s12 m12">
						<div class="tit_main">
							<h3> {{$cat->name}} </h3>
							<a href="{{route('front.get.product.category',[$cat->slug])}}">{{ json_data($scm,'Mhome_title3') }}</a>
						</div>
					</div>
				</div>

				@foreach($cat->productMenu() as $prod)
					<div class="col s6 m3">
					<a href="{{route('front.get.product.show',[$cat->slug,$prod->sub->slug,$prod->slug])}}" style="height: 100%; " >

						<div class="item-silder">
		
							<div class="img-pr">
								
								<img class="responsive-img" src="{{getImage(PRODUCT_PATH.'small/'.$prod->img)}}" />
								@if($prod->offer)
								<span class="precent_num"> {{$prod->offer}} % -</span>
								@endif
							</div>
						

							<div class="des-pr right-align">
								<a href="{{route('front.get.product.show',[$cat->slug,$prod->sub->slug,$prod->slug])}}">
									{{\Str::Words($prod->name,'3','')}}
								</a>
								<label> EGP {{$prod->price}} </label>
							</div>
						</div>
					</a>

					</div>
				@endforeach
				<?php $link = 'link'.$i;?>
				<?php $img = 'img'.$i;?>
				@if($adv->$img)
				<div class="row st-col">
					<div class="baneer_home">
						<a href="{{$adv->link.$i}}">
							<img src="{{getImage(MOBILEAD_PATH.$adv->$img)}}">
						</a>
					</div>
				</div>
				@endif
			<?php $i++; ?>

		@endforeach




	</div>



	<div class="clearfix"></div>
	<div class="row st-col">
		<div class="col s12 m12">
			<div class="tit_main">
				<h3> {{ json_data($scm,'Mhome_title4') }} </h3>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>

	<div class="row st-col">
		<div class="section_white">
			<div class="owl-carousel">

			@foreach($latest as $fpro)
			<div class="col s12 m12">
				<a href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}" style="height: 100%; " >
				<div class="item-silder">
					<div class="img-pr">
						<img class="responsive-img" src="{{getImage(PRODUCT_PATH.'small/'.$fpro->img)}}" />
						@if($fpro->offer)
							<span class="precent_num"> {{$fpro->offer}} % -</span>
						@endif
					</div>
					<div class="des-pr right-align">
						<a href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}"> {{\Str::Words($fpro->name,3,'..')}} </a>
						<label>EGP {{$fpro->price}}</label>
					</div>
				</div>
				</a>
			</div>
			@endforeach
			
		</div>
	</div>

</div>



@endsection