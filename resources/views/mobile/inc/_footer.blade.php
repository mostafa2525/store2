</div>

	<footer>
		<div class="container">
			<div class="row st-col">
				<div class="s12 m12">
					<div class="lang_footer">
						<a href="{{url('/ar')}}">  <img src="{{murl()}}/images/Egy.png"> عربي </a>
						<a href="{{url('/en')}}">  <img src="{{murl()}}/images/en.gif">  English </a>
					</div>
				</div>
				<div class="s12 m12">
					<div class="s12 m12">
						<div class="menu_footer">
							<a href="{{route('front.get.home.index')}}"> {{ json_data($scm,'MsectionLinks_home') }} </a>
							<a href="{{route('front.get.home.about')}}"> {{ json_data($scm,'MsectionLinks_about') }} </a>
							<a href="{{route('front.get.home.help')}}"> {{ json_data($scm,'MsectionLinks_help') }} </a>
							<a href="{{route('front.get.contactus.contact')}}"> {{ json_data($scm,'MsectionLinks_contactUs') }} </a>
						</div>
					</div>
					<div class="s12 m12">
						<p class="center-align copy_right"> <a href="https://eraasoft.com/" target="_blank">  @lang('frontSite.copyRight')  </a>  </p>
					</div>
				</div>
			</div>
		</div>
	</footer>