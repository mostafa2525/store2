  <script type="text/javascript" src="{{murl()}}/js/jquery.min.js"></script>
  <script type="text/javascript" src="{{murl()}}/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="{{murl()}}/js/materialize.js"></script>
  

  <script src="{{murl()}}/js/owl.carousel.js"></script>

  <script>
        $( document ).ready(function(){$(".button-collapse").sideNav();});
  </script>


  <script>
        (function($) {
            $(function() {

                $('.dropdown-button').dropdown({
                        inDuration: 300,
                        outDuration: 225,
                        hover: false, // Activate on hover
                        belowOrigin: true, // Displays dropdown below the button
                        alignment: 'right' // Displays dropdown with edge aligned to the left of button
                    }
                );

            }); // End Document Ready
        })(jQuery); // End of jQuery name space


        $(document).ready(function(){
            $('.slider').slider();
        });

        $(document).ready(function(){
            $(".owl-carousel").owlCarousel();
        });

  </script>
  
  



<script type="text/javascript" src="{{furl()}}/seoera/parsley.js"></script>
@if(Request::segment(1) == 'ar')
<script type="text/javascript" src="{{furl()}}/seoera/i18n/ar.js"></script>
@else
<script type="text/javascript" src="{{furl()}}/seoera/i18n/en.js"></script>
@endif
<script type="text/javascript" src="{{furl()}}/seoera/toster/jquery.toast.min.js"></script>


@if(session('message') !=  null )
<script type="text/javascript">
    $.toast({
                heading: 'Message',
                text: "{{ session('message') }}",
                position: 'top-right',
                stack: false,
                icon: 'info',
                hideAfter: 10000 
            })
</script>
@endif

    @yield('script')

<script type="text/javascript">
  



    // $.ajaxSetup({ headers: { 'csrftoken' : '{{ csrf_token() }}' } });
   

    $("#subscribe-form").submit(function(e) 
    {
        e.preventDefault();

        // return false;
        var formData  = new FormData(jQuery('#subscribe-form')[0]);
        $.ajax({

           type:'POST',
           url:"{{route('front.post.contactus.subscribe')}}",
           data:formData,
           contentType: false,
           processData: false,
           success:function(data)
           {

             $("#errors-footer").html('');
             $("#errors-footer").append("<li class='alert alert-success text-center'>"+data.success+"</li>")
             $('.input-sub').val("");
           },
            error: function(xhr, status, error) 
            {
              $("#errors-footer").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors-footer").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
            }

        });

	  });



    jQuery(window).load(function() 
    {
          $.ajax({

            type:'POST',
            url:"{{route('front.post.home.geoip')}}",
            data:{
            "_token": "{{ csrf_token() }}",
                },

          });

    });








</script>






    </body>

</html>