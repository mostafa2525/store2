   </head>

    <body class="">


	
	<div class="container">
	<div class="row">
		<div class="menu-main">


			<div class="col s2 m1 l2">

				<div id="mobile-collapse">
					<nav>
						<ul id="slide-out" class="side-nav">

							<li><a class="waves-effect" href="{{route('front.get.home.index')}}">{{ json_data($scm,'MsectionLinks_home')}}<i class="fa fa-home" aria-hidden="true"></i>  </a></li>

							@foreach($categories as $cat)
							<li><a class="waves-effect" href="{{route('front.get.product.category',[$cat->slug])}}">{{$cat->name}} <i class="fa fa-caret-square-o-left" aria-hidden="true"></i>  </a></li>
							@endforeach

						</ul>
						<a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons">menu</i></a>
					</nav>
				</div>
			</div>
			<!------- end menu --------->


			<div class="col s4 m3 l4 center padd-left padd-right">
				<div class="logo">
					<a href="{{route('front.get.home.index')}}">
						<img src="{{ getImage(SETTINGS_PATH.$setting->logo) }}" />
					</a>
				</div>
			</div>
			<!------- end logo --------->

			<div class="col s6 m8 l6 center">

				<div class="shopping-cart">
					<!-- Dropdown Trigger -->
					<a class='dropdown-button' href='#' data-activates='dropdown1'><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>

					<!-- Dropdown Structure -->
					<div id='dropdown1' class='dropdown-content shopping-cont'>
						<div id="append-prod"><h1>  {{json_data($scm,'MtopBar_title5')}} </h1></div>

						@if(\Cart::getTotalQuantity())
												
							@foreach(\Cart::getContent() as $cart)

							<div class="item-shopping">
								<img src="{{getImage(PRODUCT_PATH.$cart->attributes->img)}}" />
								<div class="dit-item-shopping">
									<p>
										<a href="{{route('front.get.product.show',[$cart->attributes->catSlug,$cart->attributes->subCatslug,$cart->attributes->slug])}}" style="color: #fff;">
										{{\Str::Words($cart->name,4,'')}}
										</a>
									</p>
									<label>EGP {{$cart->price}}</label>
								</div>
							</div>
							@endforeach
												
						@endif


						<div class="total-cart-home center">
							<div class="col s12 m12 l6">
								<a class="waves-effect waves-light btn color-1" href="{{route('front.get.cart.show')}}"> {{json_data($scm,'MtopBar_title6')}} </a>
							</div>
							
						</div>
						


					</div>
				</div>

				<div class="tit-main user_icon">
					<a class='dropdown-button' href='#' data-activates='dropdown2'><i class="far fa-user"></i></a>
					<!-- Dropdown Structure -->
					<div id='dropdown2' class='dropdown-content user-cont'>
						<ul>
							@if(!clientAuth()->user())
							<li>{{--@lang('frontSite.login') @lang('frontSite.register') @lang('frontSite.myAccount') @lang('frontSite.myOrders') --}}
								<a href="{{route('front.client.get.auth.login')}}">
							   {{json_data($scm,'MtopBar_title1')}}
								</a>
							</li>
							<li><a href="{{route('front.client.get.auth.register')}}">
							  {{json_data($scm,'MtopBar_title2')}} </a></li>

							@else
							<li><a href="{{route('front.client.get.profile.index','info')}}">{{json_data($scm,'MtopBar_title3')}} </a></li>
							<li><a href="{{route('front.client.get.profile.index','my-orders')}}">
							 {{json_data($scm,'MtopBar_title4')}} </a></li>
							<li class="logout-btn"><a href="{{route('front.client.get.auth.logout')}}"> @lang('frontSite.logout') </a></li>
							@endif


						</ul>
					</div>
				</div>

			</div>
			<!------- end icon top header --------->



		</div>
	</div>


		<div class="clearfix"></div>


	<div class="row st-col">
		<div class="col s12 m12 l12 no-padding">
			<div class="search_home">
				<div class="input-field col s12">
					<form method="GET" action="{{route('front.get.product.search')}}"  >
						<input type="search" class="validate box_search" name="search" placeholder="{{ json_data($scm,'Mhome_title1') }}"  style="border: 2px solid #02C54C !important;">
					</form>
				</div>
			</div>
		</div>
	</div> <!------- end search --------->



<div class="container">
