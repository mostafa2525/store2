 <!DOCTYPE html>
  <html>
    <head>
	  <meta charset="utf-8" />

	  <title>{{$setting->site_name}}</title>
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
		<link type="text/css" rel="stylesheet" href="{{murl()}}/css/bootstrap.min.css"  media="screen,projection"/>
		<link type="text/css" rel="stylesheet" href="{{murl()}}/css/materialize.css"  media="screen,projection"/>

	  <link type="text/css" rel="stylesheet" href="{{murl()}}/css/style.css"  media="screen,projection"/>
	  <link type="text/css" rel="stylesheet" href="{{murl()}}/css/font-awesome.css"  media="screen,projection"/>
	  <link type="text/css" rel="stylesheet" href="{{murl()}}/css/font/font.css"  media="screen"/>
	  <link rel="stylesheet" href="{{murl()}}/css/owl.carousel.css">
      <!--Let browser know website is optimized for mobile-->
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

	  <link type="text/css" rel="stylesheet" href="{{murl()}}/css/mobile.css"  media="screen"/>
	  <link rel="stylesheet" href="{{furl()}}/seoera/toster/jquery.toast.min.css">


	  <link rel="stylesheet" href="{{murl()}}/new/css/endMobile.css">
		
		 @if(app()->getLocale() == "ar")
			<link href="https://fonts.googleapis.com/css?family=El+Messiri" rel="stylesheet">
			<style type="text/css">
				div,p,span,li,a,ul,body,html,h1,h2,h3,h4,h5,h6
				{
					font-family: 'El Messiri', sans-serif !important;
				}
			</style>
        @else
        <link rel="stylesheet" href="{{furl()}}/seoera/css/en.css">
        @endif


        <link rel="stylesheet" href="{{furl()}}/seoera/css/ltr.css">
        @yield('style')
        