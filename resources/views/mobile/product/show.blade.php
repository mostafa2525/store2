@extends('mobile.main')


@section('content')




<div class="clearfix"></div>

		<div class="row st-col">
			<div class="col s12 m12 l12">
				<div class="slide-home">
					<!-- <span class="icon_wish"><a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a></span> -->
					<div class="slider">
						<ul class="slides">
							<li>
								<img src="{{getImage(PRODUCT_PATH.$row->img)}}"> <!-- random image -->
							</li>

							@if($images)
								@foreach($images as $img)
								<li>
									<img src="{{getImage(PRODUCT_PATH_IMAGES.$img)}}"> <!-- random image -->
								</li>
								@endforeach
							@endif
							
						
						</ul>
					</div>
				</div>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row st-col">
			<div class="dit_view_pro">
				<label> {{$row->name}} </label>
				<h3>{{$row->small_desc}}</h3>
				<br>
				<h2 class="text-center">EGP {{ $row->price}}</h2>
				<ul class="pro-rating text-center">
					<li class="pro-ratcolor"> <i class="fa fa-star"></i></li>
					<li class="pro-ratcolor"><i class="fa fa-star"></i></li>
					<li class="pro-ratcolor"><i class="fa fa-star"></i></li>
					<li><i class="fa fa-star"></i></li>
					<li><i class="fa fa-star"></i></li>
				</ul>
				

				@if($row->offer)
				<span> {{$row->offe}} %  </span>
				@endif

			</div>
		</div>
		<hr>

		<div class="clearfix"></div>

		<div class="row st-col">
			<div class="select_size">
				<div class="col s12 m12">
					<p style="    font-weight: 800; font-size: 20px;"> {{ json_data($scm,'Mproduct_title1') }} </p>
				</div>
				<div class="col s12 m12">
					<div class="btn-group" data-toggle="buttons">

					
						<?php $i = 1; ?>

						@foreach($row->allSizes as $size)
							@if($i==1)
							<?php $siz=$size->size_id; ?>
							@endif
						<label class="btn btn-default @if($i==1) active @endif size-cart" data-size="{{$size->id}}" >
							<input type="radio"  id="option{{$i}}"  autocomplete="off" 
							@if($i==1) checked @endif  > 
							{{$size->sizeData->title}}
						</label>
						<?php $i++; ?>
						@endforeach
						<input type="hidden" name="data-size" class="data-size" value="{{$siz}}">
					

					</div>
				</div>
			</div>
		</div>

		<div class="row st-col">
			<div class="select_color">
				<div class="col s12 m12">
					<p style="    font-weight: 800; font-size: 20px;"> {{ json_data($scm,'Mproduct_title2') }} </p>
				</div>
				<div class="col s12 m12">
					<div class="btn-group text-center" data-toggle="buttons">

						<?php $i = 1; ?>
						@foreach($row->allColors as $co)
								@if($i==1)
								<?php $col=$co->color_id; ?>
								@endif
							<label class="btn btn-default color_select @if($i==1) active @endif color-cart" data-color="{{$co->id}}" style="background-color: {{$co->colorData->color}};">
								<input type="radio" name="options" id="option2" autocomplete="off" class="color-cart" value="{{$co->color_id}}" > <div class="img_selected" @if($i==1) checked @endif></div>
							</label>
							<?php $i++; ?>
						@endforeach
						<input type="hidden" name="data-size" class="data-color" value="{{$col}}">
					</div>
				</div>
			</div>
		</div>

		<div class="row st-col">
			<div class="col s12 m12">
				<a href="#modal1" class="add_car_page addToCart" data-proId="{{$row->id}}">
					<i class="fa fa-shopping-cart"  aria-hidden="true"></i> {{ json_data($scm,'Mproduct_title3') }}
				</a>
			</div>

		</div>

		<div class="row">
			<div class="col s12 m12 l12">


				<!-- Modal Structure -->
				<div id="modal1" class="modal" style="height: 250px;">
					<div class="modal-content">
						<h4 class="head_pop">   @lang('frontSite.successCart') </h4>
					</div>
					<div class="modal-footer center model_pop">
						<a class="waves-effect waves-light btn btn_pop color-1"  href="{{route('front.get.home.index')}}" style="display: block; width: 100%; margin-bottom: 15px;"> @lang('frontSite.contnueShopping') </a>
						<a class="waves-effect waves-light btn btn_pop color-2" href=" {{route('front.get.cart.show')}}" style="display: block; width: 100%;"> @lang('frontSite.contnueCheckout') </a>
					</div>
				</div>

			</div>
		</div>



		<div class="row st-col">
			<div class="col s12 m12">
				<div class="tit_main">
					<hr>
					<hr>
				</div>
			</div>
		</div>

		<div class="row st-col">
			<div class="col s12 m12">
				<div class="tit_main">
					<h3> {{ json_data($scm,'Mproduct_title4') }}  </h3>
				</div>
			</div>
		</div>

		<div class="clearfix"></div>

		<div class="row st-col">
			<div class="section_white">
				<div class="owl-carousel">

					@foreach($related as $fpro)
					<div class="col s12 m12">
						<a href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}" 
							style="height: 100%;">
						<div class="item-silder">
							<div class="img-pr">
								<img class="responsive-img" src="{{getImage(PRODUCT_PATH.'small/'.$fpro->img)}}" />
								
								@if($fpro->offer)
								<span class="precent_num"> {{$fpro->offer}} % </span>
								@endif
							</div>
							<div class="des-pr right-align">
								<a href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}"> {{\Str::Words($fpro->name,3,'..')}} </a>
								<label> EGP {{$fpro->price}} </label>
							</div>
						</div>
						</a>
					</div>
					@endforeach	
				</div>
			</div>
		</div>

	</div>



@endsection


@section('script')

<script>



	$(document).ready(function(){
            $('.modal').modal();
        });

   


    var size = $(".data-size").val();
    var color = $(".data-color").val();

    $(document).on("click",".size-cart",function() 
    {
    	size = $(this).attr("data-size");
    });
    $(document).on("click",".color-cart",function() 
    {
    	color = $(this).attr("data-color");
    });


    $(document).on("click",".addToCart",function(e) 
    {

        e.preventDefault();
        el = $(this);

        // return false;
        var prodId = $(this).attr("data-proId");
        $.ajax({

           type:'POST',
           url:"{{route('front.post.cart.mobileAdd')}}",
           data:{
            "_token": "{{ csrf_token() }}",
            "prodId": prodId,"size":size,"color":color },
            beforeSend:function()
            {
            	$(".coverloading").css("display","block");
            },
           success:function(data)
           {
              if(data.success)
              {
                $(".coverloading").css("display","none");
                el.removeClass("addToCart");

              }
              else
              {
                 $("#append-prod").append(data);
                 $(".coverloading").css("display","none");
              }


           }
         

        });

    });



</script>





@endsection