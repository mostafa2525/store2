@if($allProducts->count())
	@foreach($allProducts as $fpro)
	<div class="col s12 m12">
		<div class="item-silder item_list">
			<div class="img-pr">
				<img class="responsive-img" src="{{getImage(PRODUCT_PATH.'small/'.$fpro->img)}}" alt="{{$fpro->name}}" />
			</div>
			<div class="des-pr right-align">
				<h4> {{$fpro->category->name}} </h4>
				<a href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}">{{\Str::Words($fpro->name,3,'..')}}</a>
				<label>  EGP {{ $fpro->price}}  </label>
			</div>
		</div>
	</div>

	@endforeach
@else

	<div class="alert alert-danger text-center">@lang('frontSite.dataNotFound')</div>

@endif