



<div class="item-shopping">
	<img src="{{getImage(PRODUCT_PATH.$row->img)}}" />
	<div class="dit-item-shopping">
		<p>
			<a href="{{route('front.get.product.show',[$row->category->slug,$row->sub->slug,$row->slug])}}" style="color: #fff;">
			{{\Str::Words($row->name,4,'')}}
			</a>
		</p>
		<label>EGP @if($row->offer) {{ $row->price - ($row->price * ($row->offer/100))}} @else {{$row->price}} @endif</label>
	</div>
</div>



