@extends('mobile.main')

@section('content')




	@if(\Cart::getTotalQuantity())

			<div class="clearfix"></div>

			<form method="post" action="{{route('front.post.cart.update')}}">
				@csrf
			<div class="row st-col">
				<h3 class="num_pro">
					{{ json_data($scm,'Mcart_title1') }} <b> {{ \Cart::getTotalQuantity() }} </b>  {{ json_data($scm,'Mcart_title2') }}
				</h3>
			</div> <!------- end number products --------->

			<div class="clearfix"></div>
				
			@foreach(\Cart::getContent() as $cart)
			<div class="row st-col">
				<div class="section_white">

					
					<div class="col s12 m12">
						<div class="item-silder item_list">
							<div class="img-pr">
								<img class="responsive-img" src="{{getImage(PRODUCT_PATH.'small/'.$cart->attributes->img)}}" />
							</div>
							<div class="des-pr right-align">
								<h4> {{ json_data($scm,'Mcart_title3') }}   
									<strong> {{ $cart->quantity}} </strong>
								</h4>
								<a href="{{route('front.get.product.show',[$cart->attributes->catSlug,$cart->attributes->subCatslug,$cart->attributes->slug])}}">{{$cart->name}}</a>
								<label class="price-prod"> 

									@if($cart->attributes->offer)
									<span class="new-price"> EGP {{  ($cart->price - ($cart->price * ($cart->attributes->offer/100))) * ($cart->quantity) }}</span> -  
									<span class="old-price"><del>EGP {{ $cart->price * $cart->quantity}} </del></span>
									@else
									<span class="old-price"> EGP {{ $cart->price * $cart->quantity}} </span>
									@endif
								</label>
							</div>
							</div>
					</div>
					
					<div class="col s12 m12">
						<div class="delete_item">
							<a href="{{route('front.get.cart.delete',[$cart->id])}}">  {{ json_data($scm,'Mcart_title4') }} <i class="fa fa-trash" aria-hidden="true"></i>  </a>

					
							<input id="input-number-mod-2" value="{{$cart->quantity}}" min="1" name="qunt[]" class="mod cart-quant" type="number" value="1" data-cartId="{{$cart->id}}" data-price="{{$cart->price}}"  />
							<input type="hidden" name="prodCart[]" value="{{$cart->id}}" >

						</div>

					</div>
				</div>
			</div>

			@endforeach


			<div class="row st-col">
				<div class="col s12 m12 l12 no-padding">
					
						<!-- <button type="submit" class="btn-bl" >
						 {{-- json_data($scm,'Mcart_title5') --}}  
						</button> -->
				

					<div class="total-price  color-4" style="    padding: 20px;"> 
					 	<span class="left">	{{ json_data($scm,'Mcart_title6') }} </span>   
						<strong class="right total-price-cart"> EGP {{  \Cart::getSubTotal() }} </strong>    
					</div>
					<div class="delivery center">    
					  <span class="left">{{ json_data($scm,'Mcart_title7') }} </span>   
					  <span class="right ">{{$setting->price_charge}} EGP </span> 
					</div>

					<div class="total-price  color-4" style="padding: 20px;"> 
					 	<strong class="left">	{{ json_data($scm,'Mcart_title8') }} </strong>   
						<strong class="right total-price-charge">  EGP - {{  \Cart::getSubTotal() + $setting->price_charge }} </strong>    
					</div>
					
						
						<div class="total-price center color-2">
							<a href="{{route('front.get.order.checkout')}}"> {{ json_data($scm,'Mcart_title9') }} </a>
						</div>
				
				</div>
			</div>

		

		</form>



	@else
									
		<h3 class="alert alert-danger text-center">@lang('frontSite.dataNotFound')</h3>

	@endif
		<!-- DISCOUNT SUBTOTAL AREA END -->



@endsection



@section('script')



<script type="text/javascript">
  



    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });


    $(".cart-quant").change(function(e) 
    {
        e.preventDefault();
        var el = $(this);
        var qty = $(this).val();
        var cartId = $(this).attr("data-cartId");
        var price = $(this).attr("data-price");

        el.parents(".section_white").find(".price-prod").html("EGP " + price * qty  )  
        el.parents(".section_white").find(".des-pr h4 strong").html( qty  )  

        $.ajax({

           type:'GET',
           url:"{{route('front.get.cart.edit')}}",
           data:{'qty':qty,'cartId':cartId},
           success:function(data)
           {
           		$(".total-price-cart").html("EGP "+data.toatal);
           		$(".total-price-charge").html("EGP "+data.toatalWithCharge);
           },
            error: function(xhr, status, error) 
            {
            }

        });

	});

</script>



@endsection
