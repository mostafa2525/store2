@extends('mobile.main')





@section('content')


<div class="clearfix"></div>

	<div class="row st-col">
		<h3 class="back_tit"> <i class="fa fa-chevron-right" aria-hidden="true"></i>  {{$row->name}}  </h3>
	</div> <!------- end back_tit --------->

		<div class="clearfix"></div>

	<div class="row st-col">
		<div class="block_sort">
			<div class="col s12 m12">
				<div class="sort_by">
						<select class="browser-default" required  id="sortProduct">
							<option value=""> -- {{ json_data($scm,'Mhome_title1') }}     -- </option>
							<option value="1">  {{ json_data($scm,'Mhome_title2') }}  </option>
							<option value="2">  {{ json_data($scm,'Mhome_title3') }}    </option>
							<option value="3"> {{ json_data($scm,'Mhome_title4') }}     </option>
						</select>
				</div>
			</div>


		</div>

	</div><!------- end sort --------->


	<div class="clearfix"></div>
	<div class="row st-col">
		<h3 class="num_pro">
			تم ايجاد <b>{{$allProdCat}}</b> منتج
		</h3>
	</div> <!------- end number products --------->

	<div class="clearfix"></div>

		<div class="row st-col">
			<div class="section_white">

				@foreach($allProducts as $fpro)
				<div class="col s12 m12">
					<a href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}" style="height: 100%; " >
					<div class="item-silder item_list">
						<div class="img-pr">
							<img class="responsive-img" src="{{getImage(PRODUCT_PATH.'small/'.$fpro->img)}}" alt="{{$fpro->name}}" />
						</div>
						<div class="des-pr right-align">
							<h4> {{$row->name}} </h4>
							<a href="{{route('front.get.product.show',[$fpro->category->slug,$fpro->sub->slug,$fpro->slug])}}">{{\Str::Words($fpro->name,3,'..')}}</a>
							<label>  EGP {{ $fpro->price}}  </label>
						</div>
					</div>
					</a>
				</div>

				@endforeach
				<div class="col s12 m12">
					<div class="single-product">
						{{$allProducts->links()}}
					</div>
				</div>
		

			</div>
		</div>




@endsection






@section('script')



<script type="text/javascript">
  



    $.ajaxSetup({
        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
    });


    $("#sortProduct").change(function(e) 
    {
        e.preventDefault();

        // return false;
        var sortId = $(this).val();

        $.ajax({

           type:'GET',
           url:"{{route('front.post.product.sortProduct')}}",
           data:{'sortId':sortId},
           beforeSend:function()
           {
           	   $(".coverLoading").css("display","block")
           },
           success:function(data)
           {
             $(".section_white").html('');
             $(".section_white").html(data);
           	 $(".coverLoading").css("display","none")

           },
            error: function(xhr, status, error) 
            {
              $("#errors").html('');
              $.each(xhr.responseJSON.errors, function (key, item) 
              {
                $("#errors").append("<li class='alert alert-danger show-errors'>"+item+"</li>")
              });
            }

        });

	});

</script>



@endsection

