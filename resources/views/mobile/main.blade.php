@include('mobile.inc._linksHeader')
@include('mobile.inc._header')


@yield('content')

@include('mobile.inc._footer')
@include('mobile.inc._linksFooter')