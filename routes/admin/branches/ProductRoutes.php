<?php 

//  sub categories routes
//  
Route::group(['prefix'=>'product','namespace'=>'Product'],function()
{
	
	// show all data an data table 
	Route::get('/all','ProductController@index')->name('get.product.index'); 
	Route::get('/all/categories/{id}','ProductController@category')->name('get.product.category'); 
	Route::get('/all/sub-categories/{id}','ProductController@sub')->name('get.product.sub'); 
	Route::post('/sub-categories','ProductController@getSub')->name('post.product.getSub'); 

	// show all data according parent category
	Route::get('/slice/{id}','ProductController@showSlice')->name('get.product.showSlice'); 

	// view form for adding new item 
	Route::get('/add','ProductController@add')->name('get.product.add');  
	// store data of item 
	Route::post('/store','ProductController@store')->name('post.product.store'); 
	// view data of specific item  
	Route::get('/edit/{id}','ProductController@edit')->name('get.product.edit'); 
	// update data of specific item 
	Route::put('/update','ProductController@update')->name('put.product.update'); 
	// delete data of specific item
	Route::get('/delete/{id}','ProductController@delete')->name('get.product.delete'); 
	// delete multi  item
	Route::post('/delete/multi','ProductController@deleteMulti')->name('post.product.deleteMulti'); 



	// sort elements
	Route::post('/sort','ProductController@sort')->name('post.product.sort'); 
	// make this item visibile or not  
	Route::get('/visibility/{id}','ProductController@visibility')->name('get.product.visibility'); 



	// view data of seo
	Route::get('/seo/{id}','ProductController@seo')->name('get.product.seo')->where('id', '[0-9]+');
	// update data of seo
	Route::post('/seo/update','ProductController@updateSeo')->name('post.product.updateSeo');




	// more images  for Product
	Route::get('/add-images/{id}','ImageController@addImages')->name('get.product.addImages')->where('id', '[0-9]+');
	Route::post('/add-images','ImageController@moreImages')->name('post.product.moreImages'); 
	Route::get('/delete-image/{id}','ProductController@deleteImage')->name('get.product.delete.image');
	// Route::get('/delete-image/{id}/{index}','ImageController@deleteImage')->name('get.product.delete.image');






	
	

});