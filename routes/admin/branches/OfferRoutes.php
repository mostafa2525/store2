<?php 

//  Color routes
//  
Route::group(['prefix'=>'offer'],function()
{
	
	// show all data an data table 
	Route::get('/all','OfferController@index')->name('get.offer.index'); 
	
	// update data of specific item 
	Route::put('/update','OfferController@update')->name('put.offer.update'); 

});