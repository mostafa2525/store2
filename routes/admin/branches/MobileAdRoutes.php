<?php 

//  Country routes
//  
Route::group(['prefix'=>'mobileAd'],function()
{
	
	// show all data an data table 
	Route::get('/all','MobileAdController@index')->name('get.mobilead.index'); 

	// update data of specific item 
	Route::put('/update','MobileAdController@update')->name('put.mobilead.update'); 

});