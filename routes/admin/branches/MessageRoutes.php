<?php 

//  Newsletter routes
//  
Route::group(['prefix'=>'message'],function()
{

	// show all data an data table 
	Route::get('/all','MessageController@index')->name('get.message.index'); 

	// delete data of specific item
	Route::get('/delete/{id}','MessageController@delete')->name('get.message.delete'); 

	// delete multi  item
	Route::post('/delete/multi','MessageController@deleteMulti')->name('post.message.deleteMulti'); 

	Route::get('/reply/{id}','MessageController@reply')->name('get.message.reply');

	Route::post('/reply','MessageController@sendReply')->name('post.message.sendReply');

});