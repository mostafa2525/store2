<?php 

//  Newsletter routes
//  
Route::group(['prefix'=>'client', 'namespace'=>'Client'],function()
{
	
	// show all data an data table 
	Route::get('/all','ClientController@index')->name('get.client.index'); 
	Route::get('/show/{id}','ClientController@show')->name('get.client.show'); 

	Route::get('/delete/{id}','ClientController@delete')->name('get.client.delete'); 
	Route::post('/block','ClientController@block')->name('post.client.block');
	Route::post('/trust','ClientController@trust')->name('post.client.trust');

	// Route::get('/orders/{id}','ClientController@orders')->name('get.client.orders'); 

	// delete multi  item
	Route::post('/delete/multi','ClientController@deleteMulti')->name('post.client.deleteMulti'); 

	

});