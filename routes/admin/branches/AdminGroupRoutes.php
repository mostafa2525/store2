<?php 

//  language routes
//  
Route::group(['prefix'=>'adminGroup'],function()
{
	
	// show all data an data table 
	Route::get('/all','AdminGroupController@index')->name('get.adminGroup.index'); 
	// view form for adding new item 
	Route::get('/add','AdminGroupController@add')->name('get.adminGroup.add');  
	// store data of item 
	Route::post('/store','AdminGroupController@store')->name('post.adminGroup.store'); 
	// view data of specific item  
	Route::get('/edit/{id}','AdminGroupController@edit')->name('get.adminGroup.edit'); 
	// update data of specific item 
	Route::put('/update','AdminGroupController@update')->name('put.adminGroup.update'); 
	// delete data of specific item
	Route::get('/delete/{id}','AdminGroupController@delete')->name('get.adminGroup.delete'); 

	Route::post('/delete/multi','AdminGroupController@deleteMulti')->name('post.adminGroup.deleteMulti');

});