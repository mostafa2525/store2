<?php 

//  Settings routes
//  
Route::group(['prefix'=>'settings'],function()
{
	
	// view form for editing base settings
	Route::get('/base','SettingsController@base')->name('get.settings.base');  
	// update settings
	Route::post('/base/update','SettingsController@updateBase')->name('post.settings.updateBase'); 
	// view data of seo
	Route::get('/seo','SettingsController@seo')->name('get.settings.seo'); 
	// update data of seo
	Route::post('/seo/update','SettingsController@updateSeo')->name('post.settings.updateSeo'); 

	Route::get('/site/content','SettingsController@siteContent')->name('get.settings.siteContent'); 



	Route::get('/site/content','SettingsController@siteContent')->name('get.settings.siteContent');  
    Route::post('/site/content/home','SettingsController@homeSiteContent')->name('post.settings.homeSiteContent');



    Route::get('/mobile/content','SettingsController@mobile')->name('get.settings.mobile');  
    Route::post('/mobile/content','SettingsController@siteContentMobile')->name('post.settings.siteContentMobile');
	
	// view data of about us page
	Route::get('/aboutus','SettingsController@aboutus')->name('get.settings.aboutus'); 
	// update data of about us page
	Route::post('/aboutus/update','SettingsController@updateaboutus')->name('post.settings.updateaboutus');



	// help us page
	Route::get('/help','SettingsController@help')->name('get.settings.help'); 
	// update data of help us page
	Route::post('/help/update','SettingsController@updateHelp')->name('post.settings.updateHelp');




	// view data of about us page
	Route::get('/collections','SettingsController@collections')->name('get.settings.collections'); 
	// update data of about us page
	Route::post('/collections/update','SettingsController@updateCollections')->name('post.settings.updateCollections'); 



});