<?php 

//  Size routes
//  
Route::group(['prefix'=>'order', 'namespace'=>'Order'],function()
{
		
	Route::get('/add','OrderController@add')->name('get.order.add'); 
	Route::post('/add','OrderController@store')->name('post.order.store'); 
	Route::post('/content','OrderController@contentStatus')->name('get.order.contentStatus'); 
	Route::post('/to-canceled','OrderController@toCanceled')->name('post.order.toCanceled');

	// search and filter
	Route::post('/search','OrderController@search')->name('post.order.search'); 
	Route::get('/filter','OrderController@filter')->name('get.order.filter'); 
	Route::get('/filter/all','OrderController@filterOrder')->name('get.order.filterOrder'); 


  Route::group(['prefix'=>'pending'],function(){

		Route::get('/all','PendingController@index')->name('get.order.pending.index'); 
		Route::get('/show/{id}','PendingController@show')->name('get.order.pending.show'); 
		Route::post('/to-shipping','PendingController@toShipping')->name('post.order.pending.toShipping'); 
		Route::get('/delete/{id}','PendingController@delete')->name('get.order.pending.delete'); 
		Route::post('/delete/multi','PendingController@deleteMulti')->name('post.order.pending.deleteMulti');
	
	});
	
  Route::group(['prefix'=>'shipping'],function(){

		Route::get('/all','ShippingController@index')->name('get.order.shipping.index');
		Route::get('/show/{id}','ShippingController@show')->name('get.order.shipping.show'); 
		Route::post('/to-accepted','ShippingController@toAccepted')->name('post.order.shipping.toAccepted'); 
		Route::post('/to-refused','ShippingController@toRefused')->name('post.order.shipping.toRefused'); 
		Route::get('/delete/{id}','ShippingController@delete')->name('get.order.shipping.delete'); 
		Route::post('/delete/multi','ShippingController@deleteMulti')->name('post.order.shipping.deleteMulti');

	});
	
  Route::group(['prefix'=>'canceled'],function(){

		Route::get('/all','CanceledController@index')->name('get.order.canceled.index');
		Route::get('/show/{id}','CanceledController@show')->name('get.order.canceled.show'); 
		Route::get('/delete/{id}','CanceledController@delete')->name('get.order.canceled.delete'); 
		Route::post('/delete/multi','CanceledController@deleteMulti')->name('post.order.canceled.deleteMulti');

	});
	
  Route::group(['prefix'=>'accepted'],function(){

		Route::get('/all','AcceptedController@index')->name('get.order.accepted.index');
		Route::get('/show/{id}','AcceptedController@show')->name('get.order.accepted.show'); 
		Route::get('/delete/{id}','AcceptedController@delete')->name('get.order.accepted.delete'); 
		Route::post('/delete/multi','AcceptedController@deleteMulti')->name('post.order.accepted.deleteMulti');

	});
	
  Route::group(['prefix'=>'refused'],function(){

		Route::get('/all','RefusedController@index')->name('get.order.refused.index');
		Route::get('/show/{id}','RefusedController@show')->name('get.order.refused.show'); 
		Route::get('/delete/{id}','RefusedController@delete')->name('get.order.refused.delete'); 
		Route::post('/delete/multi','RefusedController@deleteMulti')->name('post.order.refused.deleteMulti');

  });

});